//
//  forgotpasswordcontroller.h
//  PhysioMonkey
//
//  Created by prateek on 3/29/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
@interface forgotpasswordcontroller : UIViewController <WebDataDelegate>
{
    NSMutableDictionary *Request_data;
    NSString * str_status;
}

//IBOUTLET
@property (nonatomic,retain)IBOutlet UIButton * Back_btn;
@property (nonatomic,retain)IBOutlet UIButton * btn_Resetpassword;
@property (nonatomic,retain)IBOutlet UITextField * txt_email;
@property (nonatomic,strong) WebService *webObj;


//IBACTION
-(IBAction)Resetpasswordclicked:(id)sender;

@end

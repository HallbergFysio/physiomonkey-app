//
//  VerifyNewClient.h
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonMethodClass.h"
#import "WebService.h"

@interface VerifyNewClient : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate>
{
    NSArray * arr_name;
    NSArray * arr_image;
    NSArray * arr_phone;
    NSArray * arr_email;
    NSArray * arr_id;
    NSArray * main_arr;
    NSString * str_checkstatus;
    NSString * str_status;
    NSMutableDictionary * Request_data;
}

//IBOUTLET
@property (nonatomic,retain)IBOutlet UITableView * tbl_verifyclient;
@property(nonatomic,strong)CommonMethodClass*CommonMethodObj;
@property (nonatomic,retain)WebService * webobj;
@property (strong, nonatomic) IBOutlet UISearchBar *SearchBar;
@end

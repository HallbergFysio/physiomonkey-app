//
//  CreateProgramController.m
//  PhysioMonkey
//
//  Created by prateek on 17/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "CreateProgramController.h"

@interface CreateProgramController ()
@end

@implementation CreateProgramController
@synthesize imagepick;

#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    indexPathOfTextField = nil;
    
    arr_rowwcount = [[NSMutableArray alloc]init];
    arrTimeSlot   = [[NSMutableArray alloc] init];
    textview_mstr = [[NSMutableString alloc]init];
    dict_textview = [[NSMutableDictionary alloc]init];
    arr_showbtn = [[NSMutableArray alloc]init];
    arr_imagemutable = [[NSMutableArray alloc]init];
    arr_discriptionmutable = [[NSMutableArray alloc]init];
    arr_2mdemoarr = [[NSMutableArray alloc]init];
    
    
    [arr_showbtn addObject:@"hiddenbutton"];
    [arr_showbtn addObject:@"hiddenbutton"];
    [arr_showbtn addObject:@"hiddenbutton"];
    [arr_showbtn addObject:@"hiddenbutton"];
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    
    _txt_adddiscription.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    self.tbl_createprogram.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _txt_name.layer.borderColor = [[UIColor whiteColor]CGColor];
    _txt_name.layer.borderWidth = 1.0;
    _txt_name.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _txt_selectcatagory.layer.borderColor = [[UIColor whiteColor]CGColor];
    _txt_selectcatagory.layer.borderWidth = 1.0;
    _txt_selectcatagory.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _txt_adddiscription.layer.borderColor = [[UIColor whiteColor]CGColor];
    _txt_adddiscription.layer.borderWidth = 1.0;
    _txt_adddiscription.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    [self webservicecalled];
    
    if ([_str_clasname isEqualToString:@"sublibrary"])
    {
        [_btn_sendbutton setTitle:@"UPDATE" forState:UIControlStateNormal];
        _txt_adddiscription.editable = NO;
        _txt_adddiscription.dataDetectorTypes = UIDataDetectorTypeLink;
        _txt_adddiscription.delegate = self;
        
        createprogramcat_idd = [_dic_sublib valueForKeyPath:@"cat_id"];
        _txt_name.text = [_dic_sublib valueForKeyPath:@"title"];
        _txt_adddiscription.text = [_dic_sublib valueForKeyPath:@"description"];
        NSArray * arr_detail = [_dic_sublib valueForKeyPath:@"details"];
        dicTimeSlot = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"12:00",@"FromTime",nil];
        
        NSArray * edit_file_arr =  [arr_detail valueForKeyPath:@"file"];
        NSArray * edit_id_arr =  [arr_detail valueForKeyPath:@"id"];
        NSArray * edit_text_arr =  [arr_detail valueForKeyPath:@"text"];
        
        if ([edit_file_arr count]==0 && [edit_text_arr count]==0)
        {
            dicTimeSlot = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"12:00",@"FromTime",nil];
            
            [arrTimeSlot addObject:dicTimeSlot];
            [arrTimeSlot addObject:dicTimeSlot];
            [arrTimeSlot addObject:dicTimeSlot];
            [arrTimeSlot addObject:dicTimeSlot];
            
            
            [arr_discriptionmutable addObject:@""];
            [arr_discriptionmutable addObject:@""];
            [arr_discriptionmutable addObject:@""];
            [arr_discriptionmutable addObject:@""];
            
        }
        else if ([edit_file_arr count]<4)
        {
            dicTimeSlot = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"12:00",@"FromTime",nil];
            
            [arrTimeSlot addObject:dicTimeSlot];
            [arrTimeSlot addObject:dicTimeSlot];
            [arrTimeSlot addObject:dicTimeSlot];
            [arrTimeSlot addObject:dicTimeSlot];
            
            for (int i=0; i<[edit_file_arr count]; i++)
            {
                
                dicTimeSlot = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[edit_file_arr objectAtIndex:i],@"FromTime",nil];
                [arrTimeSlot replaceObjectAtIndex:i withObject:dicTimeSlot];
                NSLog(@"arrTimeSlot==%@",arrTimeSlot);
            }
        }
        else if ([edit_text_arr count]<4)
        {
            [arr_discriptionmutable addObject:@""];
            [arr_discriptionmutable addObject:@""];
            [arr_discriptionmutable addObject:@""];
            [arr_discriptionmutable addObject:@""];
            
            for (int i=0; i<[edit_text_arr count]; i++)
            {
                
                [arr_discriptionmutable replaceObjectAtIndex:i withObject:[edit_text_arr objectAtIndex:i]];
                NSLog(@"arrTimeSlot==%@",arr_discriptionmutable);
            }
        }
       
        else
            
        {
            
            for (int i=0; i<[edit_file_arr count]; i++)
            {
                
                ar = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[edit_file_arr objectAtIndex:i],@"FromTime",nil];
                [arrTimeSlot addObject:ar];
                 [arr_showbtn addObject:@"hiddenbutton"];
                NSLog(@"arrTimeSlot==%@",arrTimeSlot);
            }
            
            for (int i=0; i<[edit_text_arr count]; i++)
            {
                [arr_discriptionmutable addObject:[edit_text_arr objectAtIndex:i]];
                NSLog(@"arrTimeSlot==%@",arrTimeSlot);
            }
        }
        
        //arr_detailtext = [arr_detail valueForKeyPath:@"text"];
        if ([edit_text_arr count]<4)
        {
            [arr_discriptionmutable addObject:@""];
            [arr_discriptionmutable addObject:@""];
            [arr_discriptionmutable addObject:@""];
            [arr_discriptionmutable addObject:@""];
            
            for (int i=0; i<[edit_text_arr count]; i++)
            {
                
                [arr_discriptionmutable replaceObjectAtIndex:i withObject:[edit_text_arr objectAtIndex:i]];
                NSLog(@"arrTimeSlot==%@",arr_discriptionmutable);
            }
        }

        str_idsublib = [_dic_sublib valueForKeyPath:@"cat_id"];
    }
   
    else
    {
        [_btn_sendbutton setTitle:@"SEND" forState:UIControlStateNormal];
        dicTimeSlot = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"12:00",@"FromTime",nil];
        
        [arrTimeSlot addObject:dicTimeSlot];
        [arrTimeSlot addObject:dicTimeSlot];
        [arrTimeSlot addObject:dicTimeSlot];
        [arrTimeSlot addObject:dicTimeSlot];
        
        [arr_discriptionmutable addObject:@""];
        [arr_discriptionmutable addObject:@""];
        [arr_discriptionmutable addObject:@""];
        [arr_discriptionmutable addObject:@""];
        
    }
        [self DisplayLogoImageinTitleview];
    self.navigationItem.hidesBackButton=YES;
    [self displaybackbutton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[SVProgressHUD show];
    [self method];
}

#pragma mark -
#pragma mark: Navigation Baar Menu
-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}

-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark -
#pragma mark: HTTPWebserviceMethod
-(void)webservicecalled
{
    //[SVProgressHUD show];
    sendertitle =@"cat";
    if ([_str_clasname isEqualToString:@"mylibrary"])
    {
       [self.webobj GETAPI:nil  methodname:@"v1/categoryoptions"];
    }
    else
    {
        if ([_str_subclasname isEqualToString:@"newlibrary"])
        {
           [self.webobj GETAPI:nil  methodname:@"v1/categoryoptions"];
        }
        else
        {
            Post_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_str_idclientid,@"client_id", nil];
            [self.webobj postapi:Post_data methodname:@"v1/categoryOptionsOnClientScreen"];
   
        }
        
    }
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    if ([sendertitle isEqualToString:@"SEND"])
    {
        
        NSString * st = [dict objectForKey:@"message"];
        
        if ([st isEqualToString:@"Program saved successfully."])
        {
            [self.webobj showAlertView:@"Program Saved" msg:@"Successfully." atag:0 cncle:@"OK" othr:nil delegate:nil];
            [self clearTextfield];
            sendertitle = @"";
        }
        else
        {
            [self.webobj showAlertView:@"Create Program!" msg:@"Failed!" atag:0 cncle:@"OK" othr:nil delegate:nil];
            sendertitle = @"";
        }
    }
    else if ([sendertitle isEqualToString:@"cat"])
    {
        sendertitle = @"";
        arr_main = [dict valueForKeyPath:@"data"];
        arr_name = [arr_main valueForKeyPath:@"name"];
        arr_idd = [arr_main valueForKeyPath:@"id"];
        
        
        myPickerView = [[UIPickerView alloc]initWithFrame:
                        CGRectMake(10, 100, self.view.frame.size.width, 200)];
        
        myPickerView.dataSource = self;
        myPickerView.delegate = self;
        myPickerView.showsSelectionIndicator = YES;
        [myPickerView setBackgroundColor:[UIColor colorWithRed:77.0f/255.0f green:154.0f/255.0f blue:79.0f/255.0f alpha:1.0]];
        [myPickerView setTintColor:[UIColor whiteColor]];
        _txt_selectcatagory.inputView = myPickerView;
        
        UIToolbar *toolbar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
        [toolbar sizeToFit];
        NSMutableArray *barItems = [[NSMutableArray alloc] init];
        UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(onClickingCancelBtn)];
        [btnCancel setTintColor:[UIColor whiteColor]];
        [barItems addObject:btnCancel];
        [toolbar setBarTintColor:[UIColor colorWithRed:69.0f/255.0f green:28.0f/255.0f blue:113.0f/255.0f alpha:1.0]];
        
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        [barItems addObject:flexSpace];
        
        UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(onClickingDoneBtn)];
        [btnDone setTintColor:[UIColor whiteColor]];
        [barItems addObject:btnDone];
        [toolbar setItems:barItems animated:YES];
        [self.txt_selectcatagory setInputAccessoryView:toolbar];
        
        
        if ([_str_clasname isEqualToString:@"sublibrary"])
        {
            NSMutableArray *chatonly_arr = [NSMutableArray new];
            
            for (NSDictionary *dict1 in arr_main)
            {
                NSString *status = [NSString stringWithFormat:@"%@",dict1[@"id"]];
                
                
                if ([status isEqualToString:str_idsublib])
                {
                    if (![chatonly_arr containsObject:dict1])
                    {
                        [chatonly_arr addObject:dict1];
                    }
                }
            }
            
            NSLog(@"chatonly===========%@",chatonly_arr);
            
            if (!([chatonly_arr count]==0))
            {
                NSArray * ptnaarr = [chatonly_arr objectAtIndex:0];
                _txt_selectcatagory.text = [ptnaarr valueForKey:@"name"];
            }
            
        }
    }
}

#pragma mark -
#pragma mark: IBAction

-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)addexercideclicked:(id)sender
{
    NSLog(@"hi");
    str_addcount = @"addexer";
    dicTimeSlot = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"12:00",@"FromTime",nil];
    [arrTimeSlot addObject:dicTimeSlot];
    [arr_showbtn addObject:@"showbutton"];
    NSLog(@"arr_showbtn==%@",arr_showbtn);
    [arr_discriptionmutable addObject:@""];
    NSLog(@"arr_rowwcount2==%@",arrTimeSlot);
    [self.tbl_createprogram reloadData];
}

-(void)addRowForTimeSlot
{
    dicTimeSlot = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"12:00",@"FromTime",nil];
    [arr_rowwcount addObject:dicTimeSlot];
}

-(IBAction)sendbuttonclicked:(UIButton*)sender
{
    sendertitle = sender.titleLabel.text;
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    if ([_txt_name.text isEqualToString:@""])
    {
        [self.webobj showAlertView:@"Alert!" msg:@"Please Enter Program Title." atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([_txt_selectcatagory.text isEqualToString:@""])
    {
        [self.webobj showAlertView:@"Alert!" msg:@"Please Select Category." atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([_txt_adddiscription.text isEqualToString:@""])
    {
        [self.webobj showAlertView:@"Alert!" msg:@"Please Enter Program Discription." atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else
    {
        [SVProgressHUD show];
        
        if ([_str_clasname isEqualToString:@"mylibrary"])
        {
            Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_txt_name.text,@"title",createprogramcat_idd,@"cat_id",_txt_adddiscription.text,@"description", nil];
            _str_clasname = @"";
        }
        else
        {
            Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[obj valueForKey:@"clientid_createprogram"],@"client_id",_txt_name.text,@"title",createprogramcat_idd,@"cat_id",_txt_adddiscription.text,@"description", nil];
        }
        
        
        if (!([arr_discriptionmutable count]==0))
        {
            for (int i=0; i<[arr_discriptionmutable count]; i++)
            {
                NSString * str = [NSString stringWithFormat:@"descArr[%d]",i];
                //                if ([[arr_discriptionmutable objectAtIndex:i] isEqualToString:@""])
                //                {
                [Request_data setObject:[arr_discriptionmutable objectAtIndex:i] forKey:str];
                //}
                
            }
        }
        
        NSLog(@"arrTimeSlot====%@",arrTimeSlot);
        NSLog(@"Request_data====%@",Request_data);
        NSArray * file_a = [arrTimeSlot valueForKeyPath:@"FromTime"];
        [WebService getApiDataWithMultiPartParameters:Request_data strMethod_url:@"v1/createprogramnew" imageDataDic:file_a withCompletion:^(NSDictionary *resposnce)
         {
             NSLog(@"%@",resposnce);
             [SVProgressHUD dismiss];
             
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSString * st = [resposnce objectForKey:@"message"];
                                
                                if ([st isEqualToString:@"Program saved successfully."])
                                {
                                    [self.webobj showAlertView:@"Program saved" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
                                    [self clearTextfield];
                                    sendertitle = @"";
                                }
                                else
                                {
                                    [self.webobj showAlertView:@"Create Program!" msg:@"Failed!" atag:0 cncle:@"OK" othr:nil delegate:nil];
                                    sendertitle = @"";
                                }
                                
                            });
             
         }
                                              failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                [SVProgressHUD dismiss];
                                
                            });}];
    }
}

- (UIImage *)loadThumbNail:(NSURL *)urlVideo
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:urlVideo options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform=TRUE;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    return [[UIImage alloc] initWithCGImage:imgRef];
}

-(void)clearTextfield
{
    _txt_adddiscription.text = @"";
    _txt_selectcatagory.text = @"";
    _txt_name.text = @"";
    
    librarycell * cell  = [self.tbl_createprogram cellForRowAtIndexPath:indexPath34];
    cell.img_createProgram.image = [UIImage imageNamed:@""];
    cell.lbl_createProgram.text = @"";
}

-(void)onClickingDoneBtn
{
    if ([select_value isEqualToString:@"selected"])
    {}
    else
    {
        _txt_selectcatagory.text = [arr_name objectAtIndex:0];
        createprogramcat_idd = [arr_idd objectAtIndex:0];
    }
    
    [myPickerView removeFromSuperview];
    [_txt_selectcatagory resignFirstResponder];
}

-(void)onClickingCancelBtn
{
    [myPickerView removeFromSuperview];
    [_txt_selectcatagory resignFirstResponder];
}

-(void)dismissKeyboard
{
    [_txt_name resignFirstResponder];
    [_txt_adddiscription resignFirstResponder];
    [_txt_selectcatagory resignFirstResponder];
    
    
    librarycell* cell1 = [_tbl_createprogram cellForRowAtIndexPath: [NSIndexPath indexPathForRow:0
        inSection:0]];
     librarycell* cell2 = [_tbl_createprogram cellForRowAtIndexPath: [NSIndexPath indexPathForRow:1 inSection:0]];
     librarycell* cell3 = [_tbl_createprogram cellForRowAtIndexPath: [NSIndexPath indexPathForRow:2 inSection:0]];
     librarycell* cell4 = [_tbl_createprogram cellForRowAtIndexPath: [NSIndexPath indexPathForRow:3 inSection:0]];
    [cell1.lbl_createProgram resignFirstResponder];
    [cell2.lbl_createProgram resignFirstResponder];
    [cell3.lbl_createProgram resignFirstResponder];
    [cell4.lbl_createProgram resignFirstResponder];
    
}
-(void)videoclicked:(UIButton*)sender event:(id)event
{
    mediatype = @"video";
    index_path = [sender tag];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *Cancel_click = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       NSLog(@"You pressed button Cancel_click");
                                   }
                                   ];
    UIAlertAction *TakeVideo_clicked = [UIAlertAction actionWithTitle:@"Take Video"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                        {
                                            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                                            {
                                                [self.webobj showAlertView:@"Error" msg:@"Device has no camera" atag:0 cncle:@"OK" othr:nil delegate:nil];
                                            }
                                            else
                                            {
                                                imagepick = [[UIImagePickerController alloc] init];
                                                imagepick.delegate = self;
                                                imagepick.sourceType =  UIImagePickerControllerSourceTypeCamera;
                                                imagepick.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:imagepick.sourceType];
                                                
                                                imagepick.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
                                                [self presentViewController:imagepick animated:YES completion:NULL];
                                                
                                            }
                                            
                                        }];
    
    UIAlertAction *SelectVideo_clicked = [UIAlertAction actionWithTitle:@"Select Video"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                          {
                                              NSLog(@"You pressed button Selectvideo_clicked");
                                              imagepick = [[UIImagePickerController alloc] init];
                                              imagepick.delegate = self;
                                              imagepick.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                              imagepick.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie,nil];
                                              [self presentViewController:imagepick animated:YES completion:NULL];
                                              
                                          }];
    
    [alert addAction:Cancel_click];
    [alert addAction:TakeVideo_clicked];
    [alert addAction:SelectVideo_clicked];
    
    [self presentViewController:alert animated:NO completion:nil];
}

-(void)cameraclicked:(UIButton *)sender event:(id)event
{
    mediatype = @"image";
    str_index = [NSString stringWithFormat:@"%ld",[sender tag]];
    index_path = [sender tag];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *Cancel_click = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       NSLog(@"You pressed button Cancel_click");
                                   }
                                   ];
    UIAlertAction *Takephoto_clicked = [UIAlertAction actionWithTitle:@"Take photo"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                        {
                                            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                                            {
                                                [self.webobj showAlertView:@"Error" msg:@"Device has no camera" atag:0 cncle:@"OK" othr:nil delegate:nil];
                                            }
                                            else
                                            {
                                                imagepick = [[UIImagePickerController alloc] init];
                                                imagepick.delegate = self;
                                                imagepick.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                [self presentViewController:imagepick animated:YES completion:NULL];
                                                
                                            }
                                            
                                        }];
    
    UIAlertAction *Selectphoto_clicked = [UIAlertAction actionWithTitle:@"Select photo"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                          {
                                              NSLog(@"You pressed button Selectphoto_clicked");
                                              imagepick = [[UIImagePickerController alloc] init];
                                              imagepick.delegate = self;
                                              imagepick.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                              [self presentViewController:imagepick animated:YES completion:NULL];
                                          }];
    
    [alert addAction:Cancel_click];
    [alert addAction:Takephoto_clicked];
    [alert addAction:Selectphoto_clicked];
    
    [self presentViewController:alert animated:NO completion:nil];
}

-(void)deleteclicked:(UIButton *)sender
{
    delete_index = [sender tag];
    [arrTimeSlot removeObjectAtIndex:delete_index];
    [self.tbl_createprogram reloadData];
}

-(void)method
{
    //[SVProgressHUD show];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrTimeSlot count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"librarycell";
    
    librarycell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    
    if (cell==nil)
    {
        cell = [[librarycell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    
    cell.lbl_createProgram.delegate = self;
    
    
    if (!([arr_discriptionmutable count]==0))
    {
        cell.lbl_createProgram.text = [arr_discriptionmutable objectAtIndex:indexPath.row];
    }
    
    NSLog(@"arr_showbtn==%@",arr_showbtn);
    if ([[arr_showbtn objectAtIndex:indexPath.row]isEqualToString:@"hiddenbutton"])
    {
        [cell.btn_delete setHidden:YES];
    }
    else
    {
        [cell.btn_delete setHidden:NO];
    }
    
    
    if ([_str_clasname isEqualToString:@"sublibrary"])
    {
       
        if ( [[[arrTimeSlot objectAtIndex:indexPath.row] valueForKey:@"FromTime"] isKindOfClass:[UIImage class]])
        {
          cell.img_createProgram.image = [[arrTimeSlot objectAtIndex:indexPath.row] valueForKey:@"FromTime"];
        }
        else
        {
             NSString * str_filetype = [[[arrTimeSlot objectAtIndex:indexPath.row] valueForKey:@"FromTime"] pathExtension];
            if ([str_filetype isEqualToString:@"mp4"])
            {
                cell.img_createProgram.image = [UIImage imageNamed:@"video_dafault_img.jpg"];
            }
            else if([[[arrTimeSlot objectAtIndex:indexPath.row]valueForKey:@"FromTime"]isEqualToString:@""])
            {
                cell.img_createProgram.image = [UIImage imageNamed:@"PLUSIMAGE"];
            }
            else
            {
              [cell.img_createProgram sd_setImageWithURL:[NSURL URLWithString:[[arrTimeSlot objectAtIndex:indexPath.row] valueForKey:@"FromTime"]]placeholderImage:[UIImage imageNamed:@"PLUSIMAGE"]];  
            }
            
           
}
        
       
        
        
     
    }
    else
    {
        if ([[[arrTimeSlot objectAtIndex:indexPath.row] valueForKey:@"FromTime"] isKindOfClass:[UIImage class]])
        {
            cell.img_createProgram.image = [[arrTimeSlot objectAtIndex:indexPath.row] valueForKey:@"FromTime"];
        }
        else if ([[[arrTimeSlot objectAtIndex:indexPath.row] valueForKey:@"FromTime"] isKindOfClass:[NSURL class]])
        {
            AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[[arrTimeSlot objectAtIndex:indexPath.row]valueForKey:@"FromTime"] options:nil];
            AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
            generate.appliesPreferredTrackTransform=TRUE;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 60);
            CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
            UIImage *thumbnail =  [UIImage imageWithCGImage:imgRef];
            cell.img_createProgram.image = thumbnail;
        }
        else
        {
            cell.img_createProgram.image = [UIImage imageNamed:@"PLUSIMAGE"];
        }
    }
    
    cell.lbl_createProgram.layer.borderColor = [[UIColor whiteColor]CGColor];
    cell.lbl_createProgram.layer.borderWidth = 1.0;
    
    
    
    cell.btn_delete.tag = indexPath.row;
    [cell.btn_delete addTarget:self action:@selector(deleteclicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btn_video.tag = indexPath.row;
    [cell.btn_video addTarget:self action:@selector(videoclicked:event:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.btn_camera.tag = indexPath.row;
    [cell.btn_camera addTarget:self action:@selector(cameraclicked:event:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.lbl_createProgram.editable = YES;
    cell.lbl_createProgram.dataDetectorTypes = UIDataDetectorTypeLink;
    cell.lbl_createProgram.delegate = self;
    
    cell.lbl_createProgram.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark: image Picker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if ([mediatype isEqualToString:@"video"])
    {
        NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
        
        if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo)
        {
            NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            [self dismissViewControllerAnimated:YES completion:NULL];
            
            dicTimeSlot = [[NSMutableDictionary alloc]initWithObjectsAndKeys:videoUrl,@"FromTime",nil];
            [arrTimeSlot replaceObjectAtIndex:index_path withObject:dicTimeSlot];
            [self.tbl_createprogram reloadData];
        }
    }
    else if ([mediatype isEqualToString:@"image"])
    {
        img_tpe = @"img";
        chosenImage = info[UIImagePickerControllerOriginalImage];
        dicTimeSlot = [[NSMutableDictionary alloc]initWithObjectsAndKeys:chosenImage,@"FromTime",nil];
        [arrTimeSlot replaceObjectAtIndex:index_path withObject:dicTimeSlot];
        NSLog(@"arr_roarrTimeSlot==%@",arrTimeSlot);
        [self.tbl_createprogram reloadData];
        [imagepick dismissViewControllerAnimated:YES completion:NULL];
    }
}

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    navigationController.navigationBar.titleTextAttributes =@{NSForegroundColorAttributeName: [UIColor whiteColor]};
    [imagepick.navigationBar setTintColor:[UIColor whiteColor]];
    imagepick.navigationBar.barTintColor=[UIColor colorWithRed:105.0/255.0 green:198.0/255.0 blue:135.0/255.0 alpha:1];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark-
#pragma mark: Picker View Delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [arr_name count];
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    select_value = @"selected";
    [_txt_selectcatagory setText:[arr_name objectAtIndex:row]];
    createprogramcat_idd = [arr_idd objectAtIndex:row];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [arr_name objectAtIndex:row];
}

#pragma mark-
#pragma mark: Textfield Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark-
#pragma mark: textview Delegate

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange NS_AVAILABLE_IOS(7_0)
{
    return YES;
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound )
    {
        
        return YES;
    }
    if (txtView==_txt_adddiscription)
    {
        [txtView resignFirstResponder];
    }
    else
    {
        if (![txtView.text isEqualToString:@""])
        {
            NSLog(@"text=======%@",arr_discriptionmutable);
        }
        
        [txtView resignFirstResponder];
    }
    
    return NO;
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([_txt_adddiscription.text isEqualToString:@" Program Description"])
    {
        _txt_adddiscription.text = @"";
    }
    
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if (textView==_txt_adddiscription)
    {
        [textView resignFirstResponder];
    }
    else
    {
        NSLog(@"index====%ld",(long)textView.tag);
        if (![textView.text isEqualToString:@""])
        {
            [arr_discriptionmutable replaceObjectAtIndex:textView.tag withObject:textView.text];
            
            //[arr_discriptionmutable setObject:textView.text atIndexedSubscript:textView.tag];
        }
        NSLog(@"arr_discriptionmutable=======%@",arr_discriptionmutable);
    }
    return YES;
}

@end

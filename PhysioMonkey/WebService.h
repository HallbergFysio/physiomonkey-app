//
//  WebService.h
//  Municipality
//
//  Created by Ankur Sarda on 2/25/14.
//  Copyright (c) 2014 CIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import <CoreLocation/CoreLocation.h>
#define PASS @"userPassword"


typedef void (^WebServiceCalled)(NSDictionary*,NSError*, BOOL);

@protocol WebDataDelegate <NSObject>
@required
-(void)returnJsonData:(NSDictionary *)dict;



@end

@interface WebService : NSObject<UIAlertViewDelegate>


-(void)postapi:(NSMutableDictionary *)data methodname:(NSString *)_methdname;
-(void)GetImageFromUrlInImageView:(UIImageView *)ImageView ImageUrl:(NSString *)ImageUrl;
-(void)postDictionary:(NSMutableDictionary *)dict methodname:(NSString *)_methodname view:(UIView*)view1;
-(void)showAlertView:(NSString *)aTitle msg:(NSString *)message atag:(NSInteger)alerttag cncle:(NSString*)cancle othr:(NSString*)other delegate:(id)delgate;
-(void)GETAPI:(NSMutableDictionary *)data methodname:(NSString *)_methdname;


+(void)getApiDataWithMultiPartParameters:(NSMutableDictionary *)dicParameter strMethod_url:(NSString *)strURL imageDataDic:(NSArray *)imgDataDic  withCompletion:(void(^)(NSDictionary *resposnce))success failure:(void(^)(NSError *error))failure;

+(void)UpdateProfileMultiPartParameters:(NSMutableDictionary *)dicParameter strMethod_url:(NSString *)strURL imageDataDic:(NSData *)imgDataDic  withCompletion:(void(^)(NSDictionary *resposnce))success failure:(void(^)(NSError *error))failure;

+(void)ProfessionalupdateProfileWithMultiPartParameters:(NSMutableDictionary *)dicParameter strMethod_url:(NSString *)strURL imageDataDic:(NSArray *)imgDataDic  withCompletion:(void(^)(NSDictionary *resposnce))success failure:(void(^)(NSError *error))failure;

-(void)AddConstraints:(UIButton*)btnname view1:(UIView*)selfview height:(CGFloat)hight weigt:(CGFloat)wight leading:(CGFloat)leeding top:(CGFloat)topdistence;


-(void)imageround:(UIImageView*)img;
-(void)imageround1:(UIImageView*)img;
-(void)imageround2:(UIImageView*)img;
-(void)imageround40:(UIImageView*)img;
-(void)buttonBorderColor:(UIButton*)btn;
-(void)buttonBorderclearColor:(UIButton*)btn;
-(void)TextfieldBorderColor:(UITextField*)txt;


@property(weak,nonatomic) id<WebDataDelegate> delegate;

@end

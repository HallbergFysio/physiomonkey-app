//
//  programcontroller.m
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "programcontroller.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
@interface programcontroller ()

@end

@implementation programcontroller


#pragma mark -
#pragma mark:ViewLoadMethod
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tbl_program.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    
    if ([self.classname isEqualToString:@"recentprogram"])
    {
        pt_id = [_dictclient valueForKeyPath:@"pt_id"];
        _program_id = [_dictclient valueForKeyPath:@"id"];
    }
    else
    {
        pt_id = [_dictclient valueForKeyPath:@"id"];
    }
    
    [self.webobj imageround2:_img_ptimg];
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[SVProgressHUD show];
    [self method];
}
#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)webservicecalled
{
    //[SVProgressHUD show];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_program_id,@"program_id",pt_id,@"pt_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/singleprogram"];
}
-(void)webservicecalledSavePdf
{
    //[SVProgressHUD show];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_program_id,@"program_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/programaspdf"];
}
-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    NSLog(@"dict==%@",dict);
    if ([Str_savepdf isEqualToString:@"pdfsave"])
    {
        [SVProgressHUD dismiss];
        
        NSDate *currentTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@" yyyy-MM-d H:m:s"];
        NSString *resultString = [dateFormatter stringFromDate: currentTime];
        NSLog(@"resultString=%@",resultString);
        NSLog(@"dict==%@",dict);
        
        NSString *data = [dict valueForKeyPath:@"data"];
        NSString *pdf = [data valueForKeyPath:@"program_pdf"];
        
        NSData *dataPdf = [NSData dataWithContentsOfURL:[NSURL URLWithString:pdf]];
        //Get path directory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //Create PDF_Documents directory
        documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Physiomonkey"];
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        
        NSString *filePath = [NSString stringWithFormat:@"%@/%@ (%@)", documentsDirectory, @"Physiomonkey program Pdf" ,  resultString];
        
        [dataPdf writeToFile:filePath atomically:YES];
        NSLog(@"filePath===%@",filePath);
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [SVProgressHUD dismiss];
                           
                           [self.webobj showAlertView:nil msg:@"PDF saved" atag:0 cncle:@"OK" othr:nil delegate:nil];
                       });
    }
    else
    {
        
        arr_main = [dict valueForKeyPath:@"data"];
        arr_pt_detail = [dict valueForKeyPath:@"pt_detail"];
        
        if ([[arr_pt_detail valueForKeyPath:@"name"]isEqual:[NSNull null]])
        {
            self.lbl_name.text = @"";
        }
        else
        {
            self.lbl_name.text = [arr_pt_detail valueForKeyPath:@"name"];
        }
        
        if ([[arr_pt_detail valueForKeyPath:@"phone"]isEqual:[NSNull null]])
        {
            self.lbl_phone.text = @"";
        }
        else
        {
           self.lbl_phone.text = [arr_pt_detail valueForKeyPath:@"phone"];
        }
        
        if ([[arr_pt_detail valueForKeyPath:@"email"]isEqual:[NSNull null]])
        {
           self.lbl_email.text = @"";
        }
        else
        {
          self.lbl_email.text = [arr_pt_detail valueForKeyPath:@"email"];
        }
        
        if ([[arr_pt_detail valueForKeyPath:@"image"]isEqual:[NSNull null]])
        {
            _img_ptimg.image = [UIImage imageNamed:@"profile_img_infonew.png"];
        }
        else
        {
            [ _img_ptimg sd_setImageWithURL:[NSURL URLWithString:[arr_pt_detail valueForKeyPath:@"image"]]
                           placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
         }
        
        if (![[arr_pt_detail valueForKeyPath:@"clinic_logo"] isEqual:[NSNull null]])
        {
            [_img_logoimg sd_setImageWithURL:[NSURL URLWithString:[arr_pt_detail valueForKeyPath:@"clinic_logo"]]
                            placeholderImage:[UIImage imageNamed:@""]];
        }

        arr_name = [arr_main valueForKeyPath:@"name"];
        arr_crteateddate = [arr_main valueForKeyPath:@"created"];
        arr_discription = [arr_main valueForKeyPath:@"description"];
        arr_programdetail = [arr_main valueForKeyPath:@"details"];
        arr_file = [arr_programdetail valueForKeyPath:@"file"];
        arr_text = [arr_programdetail valueForKeyPath:@"text"];

        file_arr = [arr_file objectAtIndex:0];
        NSLog(@"arr==%@",file_arr);
        
        text_arr = [arr_text objectAtIndex:0];
        NSLog(@"arr==%@",text_arr);

        _lbl_programcontrollernamedate.text = [arr_crteateddate objectAtIndex:0];
        _lbl_programcontrollername.text = [arr_name objectAtIndex:0];
        _txt_programcontrollerdiscription.text = [arr_discription objectAtIndex:0];
        _txt_programcontrollerdiscription.editable = NO;
        _txt_programcontrollerdiscription.dataDetectorTypes = UIDataDetectorTypeLink;
        _txt_programcontrollerdiscription.delegate = self;
        [self.tbl_program reloadData];
        
    }}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [text_arr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"programcell";
    MyProgramsCell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[MyProgramsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    
    NSString * str_filetype = [[file_arr objectAtIndex:indexPath.row] pathExtension];
    NSLog(@"extension: %@", [[file_arr objectAtIndex:indexPath.row] pathExtension]);
    
    
    if ([str_filetype isEqualToString:@"mp4"])
    {
        [cell.btn_playbutton setHidden:NO];
        cell.img_myprogramcontroller.image = [UIImage imageNamed:@"video_dafault_img.jpg"];
    }
    else
    {
        [cell.btn_playbutton setHidden:YES];
        cell.img_myprogramcontroller.contentMode = UIViewContentModeScaleAspectFill;
        cell.img_myprogramcontroller.clipsToBounds=YES;
        
        if (![[file_arr objectAtIndex:indexPath.row]isEqual:[NSNull null]])
        {
            [cell.img_myprogramcontroller sd_setImageWithURL:[NSURL URLWithString:[file_arr objectAtIndex:indexPath.row]]placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
        }
    }
    
    cell.txt_myprogramcontroller.editable = NO;
    cell.txt_myprogramcontroller.dataDetectorTypes = UIDataDetectorTypeLink;
    cell.txt_myprogramcontroller.delegate = self;
    [cell.txt_myprogramcontroller setUserInteractionEnabled:YES];
    
    cell.btn_playbutton.tag = indexPath.row;
    [cell.btn_playbutton addTarget:self action:@selector(imgfullscreenclicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.txt_myprogramcontroller.text = [text_arr objectAtIndex:indexPath.row];
    cell.btn_imgfullscreen.tag = indexPath.row;
    [cell.btn_imgfullscreen addTarget:self action:@selector(imgfullscreenclicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange NS_AVAILABLE_IOS(7_0)
{
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark: IBAction
- (UIImage *)loadThumbNail:(NSURL *)urlVideo
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:urlVideo options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform=TRUE;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    return [[UIImage alloc] initWithCGImage:imgRef];
}

-(void)imgfullscreenclicked:(UIButton *)sender
{
    //[SVProgressHUD show];
    NSArray * arr = [arr_programdetail objectAtIndex:0];
    
    NSDictionary * di = arr[sender.tag];
    NSString * img_str = [di valueForKeyPath:@"file"];
    imagefullscreencontroller * image = [self.storyboard instantiateViewControllerWithIdentifier:@"imgfullvc"];
    image.image_str = img_str;
    [self.navigationController pushViewController:image animated:NO];
}

- (IBAction)Btn_savepdf:(UIButton*)sender
{
    ind_ex++;
    Str_savepdf = @"pdfsave";
    [self webservicecalledSavePdf];
}

-(void)method
{
    //[SVProgressHUD show];
}

-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end

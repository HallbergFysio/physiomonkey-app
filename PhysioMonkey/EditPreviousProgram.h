//
//  EditPreviousProgram.h
//  PhysioMonkey
//
//  Created by prateek on 11/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditPreviousProgram : UIViewController <UITableViewDelegate , UITableViewDataSource , WebDataDelegate>
{
    NSMutableDictionary *Request_data;
    
    NSArray * arr_main;
    NSArray * arr_detials;
    NSArray * arr_title;
    NSArray * arr_created;
    NSArray * arr_id;
    
    NSString * pdf_id;
    NSString * common_str;
    NSString * client_idd;
    NSString * str_programidd;
}

//IBOUTLET
@property (nonatomic,retain)IBOutlet UILabel * cat_name;
@property (nonatomic,retain)IBOutlet UILabel * lbl_line;

@property (nonatomic,retain)IBOutlet UITableView * tbl_view;

@property (nonatomic,retain)NSString * str_client_id_previous22;
@property (nonatomic,retain)NSString * str_cat_idd;
@property (nonatomic,retain)NSString * str_cat_name;

@property (nonatomic,retain)WebService *webobj;

@end

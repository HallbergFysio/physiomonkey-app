//
//  MyProgramsCell.h
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProgramsCell : UITableViewCell <UIScrollViewDelegate , UITextViewDelegate>

//IBOUTLET
@property (nonatomic,retain)IBOutlet UILabel * lbl_UserName;
@property (nonatomic,retain)IBOutlet UILabel * lbl_UserContactNumber;
@property (nonatomic,retain)IBOutlet UILabel * lbl_UserEmailID;
@property (nonatomic,retain)IBOutlet UIImageView * img_User;
@property (nonatomic,retain)IBOutlet UIImageView * img_Name;
@property (nonatomic,retain)IBOutlet UIImageView * img_Contactnumber;
@property (nonatomic,retain)IBOutlet UIImageView * img_EmailID;
@property (nonatomic,retain)IBOutlet UIButton * btn_OpenProgram;
@property (nonatomic,retain)IBOutlet UIButton * btn_ViewProfile;





//IBOUTLET PROGRAMCONTROLLER
@property (nonatomic,retain)IBOutlet UIImageView * img_Subprogram;
@property (nonatomic,retain)IBOutlet UITextView * txt_subprogramname;

//IBOUTLET OPENMYPROGRAM
@property (nonatomic,retain)IBOutlet UIImageView * img_Openmyprogram;
@property (nonatomic,retain)IBOutlet UITextView * txt_openmyprogramname;

//IBOUTLET OPENMYPROGRAMcontroller

@property (nonatomic,retain)IBOutlet UILabel * lbl_myprogram_title;
@property (nonatomic,retain)IBOutlet UITextView * txt_myprogram_discription;
@property (nonatomic,retain)IBOutlet UILabel * lbl_myprogramcreateddate;
@property (nonatomic,retain)IBOutlet UIButton * btn_myprogramopen;
@property (nonatomic,retain) IBOutlet UIScrollView * scroll_view;
@property (nonatomic,retain)IBOutlet UIImageView * img;
//IBOUTLET OPENMYPROGRAMcontroller


//IBOUTLET programcontroller
@property (nonatomic,retain)IBOutlet UIButton * btn_imgfullscreen;
@property (nonatomic,retain)IBOutlet UIButton * btn_playbutton;
@property (nonatomic,retain)IBOutlet UIImageView * img_myprogramcontroller;
@property (nonatomic,retain)IBOutlet UITextView * txt_myprogramcontroller;

@end

//
//  AddProfessionalController.h
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonMethodClass.h"
@interface AddProfessionalController : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate >
{
   
    NSArray * arr_name;
    NSArray * arr_image;
    NSArray * arr_clinicname;
    NSArray * arr_cliniclogo;
    NSArray * arr_Requestsent;
    NSArray * arr_id;
    NSArray * main_arr;
    NSArray * filtered;
    
    NSString * str_checkstatus;
    NSString * str_substatus;
    NSString * str_pageload;
    
    NSMutableDictionary * Request_data;
    BOOL isFilter;
}

//IBOUTLET
@property(nonatomic,retain)IBOutlet UITableView * tbl_addProfessional;
@property(nonatomic,strong)CommonMethodClass*CommonMethodObj;
@property(nonatomic,strong)WebService *webObj;
@property(strong, nonatomic) IBOutlet UISearchBar *SearchBar;
@end

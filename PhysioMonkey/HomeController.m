//
//  HomeController.m
//  PhysioMonkey
//
//  Created by prateek on 03/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "HomeController.h"
#import "SWRevealViewController.h"
#import "CCBarView.h"
#import "CCButtonMenu.h"

@interface HomeController ()

@end

@implementation HomeController

#pragma mark -
#pragma mark : ViewLoadMethod
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self CreateConstraints];
    [self DisplayLogoImageinTitleview];
    
    SWRevealViewController *revealController = [self revealViewController];
    UITapGestureRecognizer *tap = [revealController tapGestureRecognizer];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

#pragma mark -
#pragma mark : Navigation Bar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
    
    UIButton * btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20, 25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
}

#pragma mark -
#pragma mark : IBAction


-(void)addButtonAction:(UIButton*)sender
{
    [menuSRVC revealToggle:sender];
}

-(IBAction)profilebuttonclicked:(id)sender
{
    [self performSegueWithIdentifier:@"clientprofile" sender:self];
}

-(IBAction)Logoutbuttonclicked:(id)sender
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"logedIn"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"role"];
    ViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"main_vc"];
    [self.navigationController pushViewController:vc animated:NO];
}

-(IBAction)AddProfessionalButtonclicked:(id)sender
{
  [self performSegueWithIdentifier:@"clientaddprofessional" sender:self];
}
-(IBAction)MYProgramsButtonclicked:(id)sender
{
    [self performSegueWithIdentifier:@"clientmyprogram" sender:self];
}
-(IBAction)RecentProgramButtonclicked:(id)sender
{
    [self performSegueWithIdentifier:@"clientrecentprogram" sender:self];
}
-(IBAction)MyProfessionalButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"clientmyprofessional" sender:self];
}
-(IBAction)offerButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"offer" sender:self];
}
-(IBAction)callenderbuttonclicked:(id)sender
{
    [self performSegueWithIdentifier:@"clientcalender" sender:self];
}
-(void)CreateConstraints
{
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    NSLog(@"height==%f",height);
    if (height==736.000000)
    {
        
        NSLayoutConstraint * constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_myprofessional attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:155.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_myprofessional attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:222.0f];
        
        [self.view addConstraint:constraint_myprofessional];
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_myprofessional attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:105.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_myprofessional attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:115.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        
        NSLayoutConstraint * constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_callandar attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:155.0f];
        [self.view addConstraint:constraint_callandar];
        
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_callandar attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:472.0f];
        
        [self.view addConstraint:constraint_callandar];
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_callandar attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:105.0f];
        [self.view addConstraint:constraint_callandar];
        
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_callandar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:115.0f];
        [self.view addConstraint:constraint_callandar];
    }
    
    
    if (height==667.000000)
    {
        NSLayoutConstraint * constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_myprofessional attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:135.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_myprofessional attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:207.0f];
        
        [self.view addConstraint:constraint_myprofessional];
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_myprofessional attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:105.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_myprofessional attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:115.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        
        NSLayoutConstraint * constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_callandar attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:135.0f];
        [self.view addConstraint:constraint_callandar];
        
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_callandar attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:420.0f];
        
        [self.view addConstraint:constraint_callandar];
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_callandar attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:105.0f];
        [self.view addConstraint:constraint_callandar];
        
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_callandar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:115.0f];
        [self.view addConstraint:constraint_callandar];
    }
    
}
@end

//
//  previousprogramcontroller.h
//  PhysioMonkey
//
//  Created by prateek on 17/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
@interface previousprogramcontroller : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate>
{
    NSArray * arr_main;
    NSArray * arr_id;
    NSArray * arr_name;
    NSArray * arr_image;
    NSMutableDictionary *Request_data;
    NSMutableDictionary *Post_data;

}

//IBOUTLET
@property(nonatomic,retain)IBOutlet UITableView * tbl_previousprogram;
@property(nonatomic,retain)NSString * str_client_id_previous;
@property (nonatomic,retain)WebService *webobj;
@end

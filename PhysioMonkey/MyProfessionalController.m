//
//  MyProfessionalController.m
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "MyProfessionalController.h"

@interface MyProfessionalController ()

@end

@implementation MyProfessionalController




#pragma mark -
#pragma mark:ViewLoadMethod
- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.navigationController setNavigationBarHidden:YES animated:NO];
    self.tbl_MyProfessional.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.CommonMethodObj =[[CommonMethodClass alloc]init];
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SVProgressHUD show];
    _search_bar.text=@"";
    [self webservicecalled];
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod

-(void)webservicecalled
{
    [self.webObj postapi:nil methodname:@"v1/myprofessional"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    NSString * str_msg = [dict valueForKeyPath:@"message"];
    
    
    if ([str_msg isEqualToString:@"Record not found"])
    {
        [self.webObj showAlertView:nil msg:@"No new clients at the moment" atag:0 cncle:@"OK" othr:nil delegate:nil];
        [self.tbl_MyProfessional setHidden:YES];
    }
    else
    {
        main_arr = [dict valueForKeyPath:@"data"];
        arr_name = [main_arr valueForKeyPath:@"name"];
        arr_image = [main_arr valueForKeyPath:@"image"];
        arr_phone = [main_arr valueForKeyPath:@"phone"];
        arr_email = [main_arr valueForKeyPath:@"email"];
        arr_id = [main_arr valueForKeyPath:@"id"];
        [self.tbl_MyProfessional reloadData];
    }
    
}

#pragma mark -  search bar method

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSString *filterstr = [NSString stringWithFormat:@"%@",_search_bar.text] ;
    NSString *substring = [NSString stringWithString:filterstr];
    NSLog(@"substring %@",substring);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@",substring];
    filtered =[[main_arr filteredArrayUsingPredicate:predicate] copy];
    NSLog(@"filtered %@",filtered);
    if (filtered.count == 0)
    {
        isFilter=NO;
        [self.tbl_MyProfessional reloadData];
    }
    else
    {
        NSLog(@"filtered %@",filtered);
        isFilter=YES;
        [self.tbl_MyProfessional reloadData];
    }
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_search_bar resignFirstResponder];
}
#pragma mark - IBAction

-(void)viewprofileclicked:(UIButton *)sender
{
    ViewProfileController * vpobj = [self.storyboard instantiateViewControllerWithIdentifier:@"viewprofile"];
    if (isFilter)
    {
        vpobj.arr_viewprofile = filtered[sender.tag];
    }
    else
    {
        vpobj.arr_viewprofile = main_arr[sender.tag];
    }
    
    [self.navigationController pushViewController:vpobj animated:NO];
}

-(void)dismissKeyboard
{
    [_search_bar resignFirstResponder];
}

-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"clientmyprofhome" sender:self];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isFilter)
    {
        return [filtered count];
    }
    else
    {
        return [arr_id count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [SVProgressHUD dismiss];
    NSString *strIdentifier = @"RecentCell";
    MyProfessionalCell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[MyProfessionalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    
    if (isFilter)
    {
        Filterarr_name =  [filtered valueForKeyPath:@"name"];
        Filterarr_image = [filtered valueForKeyPath:@"image"];
        Filterarr_phone = [filtered valueForKeyPath:@"phone"];
        Filterarr_email = [filtered valueForKeyPath:@"email"];
        Filterarr_id = [filtered valueForKeyPath:@"id"];
        
        
        cell.lbl_UserName.text = [Filterarr_name objectAtIndex:indexPath.row];
        if ([[Filterarr_phone objectAtIndex:indexPath.row]isEqualToString:@""])
        {
            cell.lbl_UserContactNumber.text = @"0000000000";
        }
        else
        {
            cell.lbl_UserContactNumber.text = [Filterarr_phone objectAtIndex:indexPath.row];
        }
        
        cell.lbl_UserEmailID.text = [Filterarr_email objectAtIndex:indexPath.row];
        
        [ cell.img_User sd_setImageWithURL:[NSURL URLWithString:[Filterarr_image objectAtIndex:indexPath.row]]
                          placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
        
        [self.CommonMethodObj imageround40:cell.img_User];
        
        cell.btn_ViewProfile.tag = indexPath.row;
        [cell.btn_ViewProfile addTarget:self action:@selector(viewprofileclicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.lbl_UserName.text = [arr_name objectAtIndex:indexPath.row];
        if ([[arr_phone objectAtIndex:indexPath.row]isEqualToString:@""])
        {
            cell.lbl_UserContactNumber.text = @"";
        }
        else
        {
            cell.lbl_UserContactNumber.text = [arr_phone objectAtIndex:indexPath.row];
        }
        
        cell.lbl_UserEmailID.text = [arr_email objectAtIndex:indexPath.row];
        
        [ cell.img_User sd_setImageWithURL:[NSURL URLWithString:[arr_image objectAtIndex:indexPath.row]]
                          placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
        
        [self.webObj imageround2:cell.img_User];
        
        
        
        cell.btn_ViewProfile.tag = indexPath.row;
        [cell.btn_ViewProfile addTarget:self action:@selector(viewprofileclicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

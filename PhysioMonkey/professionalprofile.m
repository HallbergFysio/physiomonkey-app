//
//  professionalprofile.m
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "professionalprofile.h"
#import "AppDelegate.h"


@interface professionalprofile ()

@end

@implementation professionalprofile
@synthesize imagepick;

#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
    [self webservicecalled];
    [self.webObj imageround1:_img_userimage];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    _txt_Aboutme.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (IS_IPHONE_4_OR_LESS)
    {
        self.scroll_view.contentSize = CGSizeMake(0,480);
    }
    if (IS_IPHONE_5)
    {
        self.scroll_view.contentSize = CGSizeMake(0,480);
    }
    
    if (IS_IPHONE_6)
    {
        self.scroll_view.contentSize = CGSizeMake(0,480);
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark -
#pragma mark: Navigation Baar Menu
-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark -
#pragma mark: UIButton Action

-(void)dismissKeyboard
{
    [_txt_Name resignFirstResponder];
    [_txt_DOB resignFirstResponder];
    [_txt_Email resignFirstResponder];
    [_txt_Phone resignFirstResponder];
    [_txt_Address resignFirstResponder];
    [_txt_ClinicName resignFirstResponder];
    [_txt_Website resignFirstResponder];
    [_txt_Aboutme resignFirstResponder];
    [_txt_Facebookurl resignFirstResponder];
    [_txt_Twitterurl resignFirstResponder];
    [_txt_LinkedinUrl resignFirstResponder];
}
-(IBAction)editbuttonclicked:(UIButton*)sender
{
    str_common = @"edit";
    if ([sender.titleLabel.text isEqualToString:@"Save"])
    {
        
        Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_txt_Name.text,@"name",_txt_DOB.text,@"dob",_txt_Phone.text,@"phone",_txt_Address.text,@"address",_txt_ClinicName.text,@"clinic_name",_txt_Website.text,@"website",_txt_LinkedinUrl.text,@"linkedin_name",_txt_Twitterurl.text,@"twitter_name",_txt_Facebookurl.text,@"facebook_name",_txt_Aboutme.text,@"about_me",
                        nil];
        
       
        NSData *imageDatalogo = UIImageJPEGRepresentation(_img_logoimage.image,1.0);
        NSData *imageData = UIImageJPEGRepresentation(_img_userimage.image,1.0);
        
        NSArray* arr_imagedata = [NSArray arrayWithObjects: imageData, imageDatalogo,nil];
        
        
        [WebService ProfessionalupdateProfileWithMultiPartParameters:Request_data strMethod_url:@"v1/profileupdate" imageDataDic:arr_imagedata withCompletion:^(NSDictionary *resposnce)
         {
             NSLog(@"%@",resposnce);
             
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSString * str = [resposnce valueForKeyPath:@"message"];
                                
                                if ([str isEqualToString:@"User updated successfully."])
                                {
                                    [self.webObj showAlertView:@"Profile updated!" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
                                    [self webservicecalled];
                                    [_btn_Edit setTitle:@"Edit" forState:UIControlStateNormal];
                                    [self UserInteraction:NO];
                                }
                                else if ([str isEqualToString:@"Required field(s) about_me is missing or empty"])
                                {
                                   [self.webObj showAlertView:@"About_me is missing or empty" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
                                }
                                else if ([str isEqualToString:@"Required field(s) phone is missing or empty"])
                                {
                                    [self.webObj showAlertView:@"Phone number is missing or empty" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
                                }
                                
                            });
             
         }
                    failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                
                            });}];
        
    }
    else
    {
        [_txt_DOB becomeFirstResponder];
        [self UserInteraction:YES];
        [_btn_Edit setTitle:@"Save" forState:UIControlStateNormal];
    }
    
}
-(IBAction)facebookBtnclicked:(id)sender
{
   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str_FacebookUrl] options:@{} completionHandler:nil];
}

-(IBAction)twitterBtnclicked:(id)sender
{
   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str_TwitterUrl] options:@{} completionHandler:nil];
}

-(IBAction)LinkedinBtnclicked:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str_LinkedinUrl] options:@{} completionHandler:nil];
}

-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"profprofilehome" sender:self];
}

-(void)ImageButtonclicked:(UIButton*)sender
{
   
    if ([sender tag]==1)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet]; // 1
        UIAlertAction *Cancel_click = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                       {
                                           NSLog(@"You pressed button Cancel_click");
                                       }
                                       ];
        UIAlertAction *Takephoto_clicked = [UIAlertAction actionWithTitle:@"Take photo"
                                                                    style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                            {
                                                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                                                {
                                                    [self.webObj showAlertView:@"Error" msg:@"Device has no camera" atag:0 cncle:@"OK" othr:nil delegate:nil];
                                                }
                                                else
                                                {
                                                    imagepick = [[UIImagePickerController alloc] init];
                                                    imagepick.delegate = self;
                                                     imagepick.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                    [self presentViewController:imagepick animated:NO completion:NULL];
                                                    
                                                }
                                            }];
        
        UIAlertAction *Selectphoto_clicked = [UIAlertAction actionWithTitle:@"Select photo"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                              {
                                                  NSLog(@"You pressed button Selectphoto_clicked");
                                                  imagepick = [[UIImagePickerController alloc] init];
                                                  imagepick.delegate = self;
                                                imagepick.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                  [self presentViewController:imagepick animated:NO completion:NULL];
                                              }];
        
        [alert addAction:Cancel_click];
        [alert addAction:Takephoto_clicked];
        [alert addAction:Selectphoto_clicked];
        
        [self presentViewController:alert animated:NO completion:nil];
    }
    
    
    else if ([sender tag]==2)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet]; // 1
        UIAlertAction *Cancel_click = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                       {
                                           NSLog(@"You pressed button Cancel_click");
                                       }
                                       ];
        UIAlertAction *Takephoto_clicked = [UIAlertAction actionWithTitle:@"Take photo"
                                                                    style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                            {
                                                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                                                {
                                                    [self.webObj showAlertView:@"Error" msg:@"Device has no camera" atag:0 cncle:@"OK" othr:nil delegate:nil];
                                                }
                                                else
                                                {
                                                    _logoimagepick = [[UIImagePickerController alloc] init];
                                                    _logoimagepick.delegate = self;
                                                    _logoimagepick.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                    [self presentViewController:_logoimagepick animated:NO completion:NULL];
                                                    
                                                }
                                                
                                            }];
        
        UIAlertAction *Selectphoto_clicked = [UIAlertAction actionWithTitle:@"Select photo"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                              {
                                                  NSLog(@"You pressed button Selectphoto_clicked");
                                                  _logoimagepick = [[UIImagePickerController alloc] init];
                                                  _logoimagepick.delegate = self;
                                                  _logoimagepick.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                  [self presentViewController:_logoimagepick animated:NO completion:NULL];
                                              }];
        
        [alert addAction:Cancel_click];
        [alert addAction:Takephoto_clicked];
        [alert addAction:Selectphoto_clicked];
        
        [self presentViewController:alert animated:NO completion:nil];
    }
}
-(void)UserInteraction:(BOOL)bol
{
    [_txt_Name setUserInteractionEnabled:bol];
    [_txt_DOB setUserInteractionEnabled:bol];
    [_txt_Email setUserInteractionEnabled:bol];
    [_txt_Phone setUserInteractionEnabled:bol];
    [_txt_Address setUserInteractionEnabled:bol];
    [_txt_ClinicName setUserInteractionEnabled:bol];
    [_txt_Website setUserInteractionEnabled:bol];
    [_txt_Aboutme setUserInteractionEnabled:bol];
    [_txt_Facebookurl setUserInteractionEnabled:bol];
    [_txt_Twitterurl setUserInteractionEnabled:bol];
    [_txt_LinkedinUrl setUserInteractionEnabled:bol];
    [_btn_userimage setUserInteractionEnabled:bol];
    [_btn_logouserimage setUserInteractionEnabled:bol];
}


#pragma mark -
#pragma mark: Textfield Delegate Method

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark:Textfield Delegate Method

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [SVProgressHUD dismiss];
    chosenImage = info[UIImagePickerControllerOriginalImage];
    if(picker == _logoimagepick)
    {
        self.img_logoimage.image = chosenImage;
        [_logoimagepick dismissViewControllerAnimated:YES completion:NULL];
    }
    if(picker == imagepick)
    {
        self.img_userimage.image = chosenImage;
        [imagepick dismissViewControllerAnimated:YES completion:NULL];
    }
}

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    navigationController.navigationBar.titleTextAttributes =@{NSForegroundColorAttributeName: [UIColor whiteColor]};
    [imagepick.navigationBar setTintColor:[UIColor whiteColor]];
    imagepick.navigationBar.barTintColor=[UIColor colorWithRed:105.0/255.0 green:198.0/255.0 blue:135.0/255.0 alpha:1];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
#pragma mark -
#pragma mark:HTTPWebserviceMethod

-(void)webservicecalled
{
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[obj valueForKey:@"LoginUserID"],@"userid", nil];
    [self.webObj postDictionary:Request_data methodname:@"v1/userdetailbyid" view:self.view];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    
    NSArray * main_arr = [dict valueForKeyPath:@"data"];
    str_Phone = [main_arr valueForKeyPath:@"phone"];
    str_Address = [main_arr valueForKeyPath:@"address"];
    str_Email = [main_arr valueForKeyPath:@"email"];
    str_ClinicName = [main_arr valueForKeyPath:@"clinic_name"];
    str_DOB = [main_arr valueForKeyPath:@"dob"];
    str_WebsiteUrl = [main_arr valueForKeyPath:@"website"];
    str_AboutMe = [main_arr valueForKeyPath:@"about_me"];
    str_LinkedinUrl = [main_arr valueForKeyPath:@"linkedin"];
    str_TwitterUrl = [main_arr valueForKeyPath:@"twitter"];
    str_FacebookUrl = [main_arr valueForKeyPath:@"facebook"];
    str_image = [main_arr valueForKeyPath:@"image"];
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    [obj setObject:str_image forKey:@"LoginUserImage"];
    [obj synchronize];
    str_cliniclogo = [main_arr valueForKeyPath:@"clinic_logo"];
    
    self.txt_Name.text = [NSString stringWithFormat:@"%@",[main_arr valueForKeyPath:@"name"]];
    
    [_img_userimage sd_setImageWithURL:[NSURL URLWithString:str_image]
                      placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
    
    [_img_logoimage sd_setImageWithURL:[NSURL URLWithString:str_cliniclogo]
                      placeholderImage:[UIImage imageNamed:@"cliniclogo"]];
    
    if ([str_Phone isEqualToString:@""])
    {
        self.txt_Phone.text = @"";
    }
    else
    {
        self.txt_Phone.text = str_Phone;
    }
    if ([str_DOB isEqualToString:@""])
    {
        self.txt_DOB.text = @"";
    }
    else
    {
        self.txt_DOB.text = str_DOB;
    }
    if ([str_Email isEqualToString:@""])
    {
        self.txt_Email.text = @"";
    }
    else
    {
        self.txt_Email.text = str_Email;
    }
    if ([str_ClinicName isEqualToString:@""])
    {
        self.txt_ClinicName.text = @"";
    }
    else
    {
        self.txt_ClinicName.text = str_ClinicName;
    }
    if ([str_Address isEqualToString:@""])
    {
        self.txt_Address.text = @"";
    }
    else
    {
        self.txt_Address.text = str_Address;
    }
    if ([str_WebsiteUrl isEqualToString:@""])
    {
        self.txt_Website.text = @"";
    }
    else
    {
        self.txt_Website.text = str_WebsiteUrl;
    }
    if ([str_FacebookUrl isEqualToString:@""])
    {
        self.txt_Facebookurl.text = @"";
    }
    else
    {
        self.txt_Facebookurl.text = str_FacebookUrl;
    }
    if ([str_LinkedinUrl isEqualToString:@""])
    {
        self.txt_LinkedinUrl.text = @"";
    }
    else
    {
        self.txt_LinkedinUrl.text = str_LinkedinUrl;
    }
    if ([str_TwitterUrl isEqualToString:@""])
    {
        self.txt_Twitterurl.text = @"";
    }
    else
    {
        self.txt_Twitterurl.text = str_TwitterUrl;
    }
    if ([str_AboutMe isEqualToString:@""])
    {
        self.txt_Aboutme.text = @"";
    }
    else
    {
        self.txt_Aboutme.text = str_AboutMe;
    }
}

-(void)imageuploadtoserverwebservice
{
    [SVProgressHUD show];
    NSData *imageData = UIImageJPEGRepresentation(_img_userimage.image,1.0);
    NSData *imageDatalogo = UIImageJPEGRepresentation(_img_logoimage.image,1.0);//change Image to NSData
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"hhMMyyyyss"];
    
    NSString *imgName = [NSString stringWithFormat:@"%@.jpg",[df stringFromDate:[NSDate date]]];
    
    NSString *urlString = @"http://professionalphysio.com/v1/profileupdate";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@",[obj valueForKey:@"LoginUserEmail"], [obj valueForKey:@"LoginUserpassword"]];
    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authValue = [authData base64Encoding];
    
    [request setValue:[NSString stringWithFormat:@"Basic %@",authValue] forHTTPHeaderField:@"Authorization"];
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding: NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"profile_pic\"; filename=\"%@\"\r\n",imgName]dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    //////////////////////
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding: NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"clinic_logo\"; filename=\"%@\"\r\n",imgName]dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageDatalogo];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    ///////////
    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"name\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_Name.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    ///////////////
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"dob\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_DOB.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    ///////////////
    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"phone\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_Phone.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    ///////////////
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"address\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_Address.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    ///////////////
    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"clinic_name\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_ClinicName.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    ///////////////
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"website\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_Website.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    ///////////////
    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"linkedin_name\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_LinkedinUrl.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    ///////////////
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"twitter_name\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_Twitterurl.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    ///////////////
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"facebook_name\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_Facebookurl.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    ///////////////
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"about_me\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[_txt_Aboutme.text dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    ///////////////
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[NSData dataWithData:imageDatalogo]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSArray *ArrayObject = [NSJSONSerialization JSONObjectWithData:returnData options: NSJSONReadingMutableContainers error:nil];
    NSString * str = [ArrayObject valueForKeyPath:@"message"];
    [SVProgressHUD dismiss];
    if ([str isEqualToString:@"User updated successfully."])
    {
        [self.webObj showAlertView:@"Update Profile!" msg:@"Successfull!" atag:0 cncle:@"OK" othr:nil delegate:nil];
        [self webservicecalled];
        [_btn_Edit setTitle:@"Edit" forState:UIControlStateNormal];
        [self UserInteraction:NO];
    }
}

@end

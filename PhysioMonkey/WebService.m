
//
//  WebService.m
//  Municipality
//
//  Created by Ankur Sarda on 2/25/14.
//  Copyright (c) 2014 CIS. All rights reserved.
//

#import "WebService.h"
#import "AppDelegate.h"
#import "AFHTTPRequestOperationManager.h"
#import <AFHTTPSessionManager.h>
#import "Reachability.h"

#define BASE_URL @"http://professionalphysio.com/"
#define MAIN_URL @"http://professionalphysio.com/"
#define IMAGE_URL @"http://gigaparse.com/clients/alwayz/API/UserImages/"
@implementation WebService


#pragma mark -
#pragma mark: Post Method without Header
-(void)postDictionary:(NSMutableDictionary *)dict methodname:(NSString *)_methodname view:(UIView*)view1
{
    
    Reachability *internetReachable = [Reachability reachabilityForInternetConnection];
    if ([internetReachable isReachable])
    {
        
        @try
        {
            NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
            [SVProgressHUD show];
            NSString *string =  [NSString stringWithFormat:@"%@%@",MAIN_URL,_methodname];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            [manager.requestSerializer setTimeoutInterval:60];
            [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:[obj valueForKey:@"LoginUserEmail"] password:[obj valueForKey:@"LoginUserpassword"]];
            
            [manager POST:string parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 [SVProgressHUD dismiss];
                 
                 [self.delegate returnJsonData:responseObject];
             }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                
                 [SVProgressHUD dismiss];
                 
                 {
                     if (error.code == NSURLErrorTimedOut)
                     {
                         [self showAlertView:@"Network Error!" msg:@"Please try Again!" atag:0 cncle:@"OK" othr:nil delegate:nil];
                     }
                     else
                     {
                         [self showAlertView:@"Network Error!" msg:@"Please try Again!" atag:0 cncle:@"OK" othr:nil delegate:nil];
                         
                     }
                 }
                 
             }
             ];
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception %@",[exception description]);
            NSLog(@"Exception %@",[exception callStackSymbols]);
        }
        @finally
        {}
    }
    else
    {
        [SVProgressHUD dismiss];
        [self showAlertView:@"Network Error!" msg:@"Please check your internet connection!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    
}
#pragma mark -
#pragma mark: GET Method with Header
-(void)GETAPI:(NSMutableDictionary *)data methodname:(NSString *)_methdname
{
    Reachability *internetReachable = [Reachability reachabilityForInternetConnection];
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    if ([internetReachable isReachable])
    {
        
        @try
        {
            [SVProgressHUD show];
            NSString *string =  [NSString stringWithFormat:@"%@%@",MAIN_URL,_methdname];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
             [manager.requestSerializer setTimeoutInterval:60];
            [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:[obj valueForKey:@"LoginUserEmail"] password:[obj valueForKey:@"LoginUserpassword"]];
           
            [manager GET:string parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 //NSLog(@"apioutputJSON: %@", responseObject);
                 [SVProgressHUD dismiss];
                 [self.delegate returnJsonData:responseObject];
             }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 NSLog(@"error====%@",error);
                 [SVProgressHUD dismiss];
                 
                 {
                     if (error.code == NSURLErrorTimedOut)
                     {
                         [self showAlertView:@"Network Error!" msg:@"Please try Again!" atag:0 cncle:@"OK" othr:nil delegate:nil];
                     }
                     else
                     {
                         [self showAlertView:@"Network Error!" msg:@"Please try Again!" atag:0 cncle:@"OK" othr:nil delegate:nil];
                     }
                 }
                 
             }
             ];
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception %@",[exception description]);
            NSLog(@"Exception %@",[exception callStackSymbols]);
        }
        @finally
        {}
    }
    else
    {
        [SVProgressHUD dismiss];
        [self showAlertView:@"Network Error!" msg:@"Please check your internet connection!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    
}
#pragma mark -
#pragma mark: POST Method with Header
-(void)postapi:(NSMutableDictionary *)data methodname:(NSString *)_methdname
{
    
    Reachability *internetReachable = [Reachability reachabilityForInternetConnection];
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    if ([internetReachable isReachable])
    {
        
        @try
        {
            [SVProgressHUD show];
            NSString *string =  [NSString stringWithFormat:@"%@%@",MAIN_URL,_methdname];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            [manager.requestSerializer setTimeoutInterval:60];
            [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:[obj valueForKey:@"LoginUserEmail"] password:[obj valueForKey:@"LoginUserpassword"]];
            
            [manager POST:string parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 //NSLog(@"apioutputJSON: %@", responseObject);
                 [SVProgressHUD dismiss];
                 [self.delegate returnJsonData:responseObject];
             }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 NSLog(@"error====%@",error);
                 [SVProgressHUD dismiss];
                 
                 {
                     if (error.code == NSURLErrorTimedOut)
                     {
                         [self showAlertView:@"Network Error!" msg:@"Please try Again!" atag:0 cncle:@"OK" othr:nil delegate:nil];
                     }
                     else
                     {
                         [self showAlertView:@"Network Error!" msg:@"Please try Again!" atag:0 cncle:@"OK" othr:nil delegate:nil];
                         
                     }
                 }
                 
             }
             ];
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception %@",[exception description]);
            NSLog(@"Exception %@",[exception callStackSymbols]);
        }
        @finally
        {}
    }
    else
    {
        [SVProgressHUD dismiss];
        [self showAlertView:@"Network Error!" msg:@"Please check your internet connection!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
}
#pragma mark -
#pragma mark: GET Method For Download Image from server
-(void)GetImageFromUrlInImageView:(UIImageView *)ImageView ImageUrl:(NSString *)ImageUrl
{
    
    NSURL *linkURL = [NSURL URLWithString:ImageUrl];
    NSString *imageName = [linkURL lastPathComponent];
    NSString *imgUrl = [NSString stringWithFormat:@"%@%@",IMAGE_URL,imageName];
    
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center =CGPointMake(ImageView.bounds.size.width/2 , ImageView.bounds.size.height/2);
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    [ImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl]
                 placeholderImage:[UIImage imageNamed:@"user.png"]
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            NSLog(@"this is Block");
                            [activityIndicator stopAnimating];
                            
                        }];
    [ImageView addSubview:activityIndicator];
    
}

+(void)getApiDataWithMultiPartParameters:(NSMutableDictionary *)dicParameter strMethod_url:(NSString *)strURL imageDataDic:(NSArray *)imgDataDic  withCompletion:(void(^)(NSDictionary *resposnce))success failure:(void(^)(NSError *error))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:INFINITY];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:60];
    NSMutableIndexSet* codes = [NSMutableIndexSet indexSetWithIndexesInRange: NSMakeRange (200, 100) ] ;
    [codes addIndex: 404];
    [codes addIndex: 400];
    [codes addIndex: 500];
    [codes addIndexesInRange:NSMakeRange(400, 100)];
    
    manager.responseSerializer.acceptableStatusCodes = codes;
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:[obj valueForKey:@"LoginUserEmail"] password:[obj valueForKey:@"LoginUserpassword"]];
    
    NSString *path = [NSString stringWithFormat:@"%@%@",MAIN_URL,strURL];
    [manager POST:path parameters:dicParameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
     {
         if (!([imgDataDic count]==0))
         {
             for (int i=0; i<[imgDataDic count]; i++)
             {
                 NSString * str = [NSString stringWithFormat:@"fileArr[%d]",i];
                 
                 if ([[imgDataDic objectAtIndex:i]isKindOfClass:[UIImage class]])
                 {
                     UIImage *img = [imgDataDic objectAtIndex:i];
                     NSData *imgData = UIImageJPEGRepresentation(img,0);
                     NSString *strPhotoName = [NSString stringWithFormat:@"photo.jpg" ];
                     [formData appendPartWithFileData:imgData name:str fileName:strPhotoName mimeType:@"image/jpeg"];
                 }
                 else if ([[imgDataDic objectAtIndex:i]isKindOfClass:[NSURL class]])
                 {
                     
                     NSData *imgData =[NSData dataWithContentsOfFile:[imgDataDic objectAtIndex:i]];
                     NSString *strPhotoName = [NSString stringWithFormat:@"video.mp4" ];
                     [formData appendPartWithFileData:imgData name:str fileName:strPhotoName mimeType:@"video/mp4"];
                 }
                 else if ([[imgDataDic objectAtIndex:i]isEqualToString:@"12:00"])
                 {
                     UIImage *img = [UIImage imageNamed:@"PLUSIMAGE"];
                     NSData *imgData = UIImageJPEGRepresentation(img,0);
                     
                     [formData appendPartWithFileData:imgData name:str fileName:@"photo.jpg" mimeType:@"image/jpeg"];
                 }
                 
                 else
                 {}
                 
                 
             }
         }
         
     }
     
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         success(responseObject);
         NSLog(@"%@",responseObject);
     }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"%@",error);
         failure(error);
     }];
}

+(void)UpdateProfileMultiPartParameters:(NSMutableDictionary *)dicParameter strMethod_url:(NSString *)strURL imageDataDic:(NSData *)imgDataDic  withCompletion:(void(^)(NSDictionary *resposnce))success failure:(void(^)(NSError *error))failure
{
    [SVProgressHUD show];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:INFINITY];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:60];
    NSMutableIndexSet* codes = [NSMutableIndexSet indexSetWithIndexesInRange: NSMakeRange (200, 100) ] ;
    [codes addIndex: 404];
    [codes addIndex: 400];
    [codes addIndex: 500];
    [codes addIndexesInRange:NSMakeRange(400, 100)];
    
    manager.responseSerializer.acceptableStatusCodes = codes;
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:[obj valueForKey:@"LoginUserEmail"] password:[obj valueForKey:@"LoginUserpassword"]];
    
    NSString *path = [NSString stringWithFormat:@"%@%@",MAIN_URL,strURL];
    [manager POST:path parameters:dicParameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
     {
         
         NSString *strPhotoName = [NSString stringWithFormat:@"photo.jpg" ];
         [formData appendPartWithFileData:imgDataDic name:@"profile_pic" fileName:strPhotoName mimeType:@"image/jpeg"];
       }
     
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         success(responseObject);
         [SVProgressHUD dismiss];
         NSLog(@"%@",responseObject);
     }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         [SVProgressHUD dismiss];
         NSLog(@"%@",error);
         failure(error);
     }];
}

+(void)ProfessionalupdateProfileWithMultiPartParameters:(NSMutableDictionary *)dicParameter strMethod_url:(NSString *)strURL imageDataDic:(NSArray *)imgDataDic  withCompletion:(void(^)(NSDictionary *resposnce))success failure:(void(^)(NSError *error))failure
{
    [SVProgressHUD show];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:INFINITY];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:60];
    NSMutableIndexSet* codes = [NSMutableIndexSet indexSetWithIndexesInRange: NSMakeRange (200, 100) ] ;
    [codes addIndex: 404];
    [codes addIndex: 400];
    [codes addIndex: 500];
    [codes addIndexesInRange:NSMakeRange(400, 100)];
    
    manager.responseSerializer.acceptableStatusCodes = codes;
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:[obj valueForKey:@"LoginUserEmail"] password:[obj valueForKey:@"LoginUserpassword"]];
    
    NSString *path = [NSString stringWithFormat:@"%@%@",MAIN_URL,strURL];
    [manager POST:path parameters:dicParameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
     {
         
         [formData appendPartWithFileData:[imgDataDic objectAtIndex:0] name:@"profile_pic" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
         [formData appendPartWithFileData:[imgDataDic objectAtIndex:1] name:@"clinic_logo" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
     }
     
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         success(responseObject);
         [SVProgressHUD dismiss];
         NSLog(@"%@",responseObject);
     }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"%@",error);
         [SVProgressHUD dismiss];
         failure(error);
     }];
}


#pragma mark -
#pragma mark: UIAlert Delegate Method

-(void)showAlertView:(NSString *)aTitle msg:(NSString *)message atag:(NSInteger)alerttag cncle:(NSString*)cancle othr:(NSString*)other delegate:(id)delgate
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:aTitle message:message delegate:delgate cancelButtonTitle:cancle otherButtonTitles:other, nil];
    if (alerttag==1)
    {
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField *textfield=[alert textFieldAtIndex:0];
        textfield.placeholder=@"Enter Email Address";
    }
    
    [alert setTag:alerttag];
    [alert show];
}

#pragma mark -
#pragma mark: Common Method
-(void)imageround:(UIImageView*)img
{
    img.layer.cornerRadius = 55;
    img.layer.masksToBounds = YES;
    img.layer.borderWidth = 1.5f;
    img.clipsToBounds = YES;
    img.layer.borderColor = [[UIColor clearColor] CGColor];
}

-(void)imageround1:(UIImageView*)img
{
    img.layer.cornerRadius = 45;
    img.layer.masksToBounds = YES;
    img.layer.borderWidth = 1.5f;
    img.clipsToBounds = YES;
    img.layer.borderColor = [[UIColor clearColor] CGColor];
}
-(void)imageround2:(UIImageView*)img
{
    img.layer.cornerRadius = 35;
    img.layer.masksToBounds = YES;
    img.layer.borderWidth = 1.5f;
    img.clipsToBounds = YES;
    img.layer.borderColor = [[UIColor clearColor] CGColor];
}

-(void)imageround40:(UIImageView*)img
{
    img.layer.cornerRadius = 40;
    img.layer.masksToBounds = YES;
    img.layer.borderWidth = 1.5f;
    img.clipsToBounds = YES;
    img.layer.borderColor = [[UIColor clearColor] CGColor];
}

-(void)buttonBorderColor:(UIButton*)btn
{
    btn.layer.borderColor = [[UIColor whiteColor]CGColor];
    btn.layer.borderWidth = 1.0;
}
-(void)buttonBorderclearColor:(UIButton*)btn
{
    btn.layer.cornerRadius=2;
    btn.layer.borderWidth = 1.0f;
    btn.clipsToBounds=YES;
    btn.layer.borderColor = [[UIColor clearColor]CGColor];
}

-(void)TextfieldBorderColor:(UITextField*)txt
{
    txt.layer.borderColor = [[UIColor whiteColor]CGColor];
    txt.layer.borderWidth = 1.0;
    txt.layer.cornerRadius=2;
    txt.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
}
@end

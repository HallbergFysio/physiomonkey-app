//
//  ProfessionalLeft.h
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfessionalLeft : UIViewController <UITableViewDataSource,UITableViewDelegate,WebDataDelegate>
{
    NSArray * arr_leftmenuItem_title;
    NSArray * arr_leftmenuItem_img;
}

//IBOUTLET
@property (nonatomic,retain)IBOutlet UITableView * table_view;
@property (nonatomic,retain)IBOutlet UILabel * lbl_username;
@property (nonatomic,retain)IBOutlet UIImageView * img_userimage;
@property (nonatomic,retain)WebService * webobj;
@end

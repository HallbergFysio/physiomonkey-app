//
//  PreviousCommentController.h
//  PhysioMonkey
//
//  Created by prateek on 03/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviousCommentController : UIViewController <UITableViewDelegate , UITableViewDataSource,WebDataDelegate>
{
    NSArray * arr_TransactionNo;
    NSArray * arr_type;
    NSArray * arr_title;
    NSArray * arr_start;
    NSArray * arr_end;
    NSArray * arr_id;
    NSArray * arr_ptid;
    NSArray * arr_typeid;
    NSArray * arr_main;
}
//IBOUTLET

@property (nonatomic,retain)IBOutlet UIButton * btn_viewcalender;
@property (nonatomic,retain)IBOutlet UIButton * btn_newcomment;

@property (nonatomic,retain)IBOutlet UITableView * tbl_view;
@property (nonatomic,retain)WebService * webobj;

//IBACTION

-(IBAction)viewcalenderclicked:(id)sender;
-(IBAction)newcommentbuttonclicked:(id)sender;
@end

//
//  LeftController.m
//  PhysioMonkey
//
//  Created by prateek on 03/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "LeftController.h"
#import "Leftviewcell.h"
#import "ViewController.h"
#import "offerscontroller.h"
#import "RecentProgramController.h"
#import "MyProgramsViewController.h"
#import "AddProfessionalController.h"
#import "MyProfileviewcontroller.h"
#import "MyProfessionalController.h"
#import "calenderviewcontroller.h"
@interface LeftController ()

@end

@implementation LeftController
@synthesize table_view;

#pragma mark - ViewLoadMethod

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    
    [self.webobj imageround1:_img_userimage];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.table_view.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    arr_leftmenuItem_title= @[@"Home",@"My Profile", @"Add Professional", @"My Professionals", @"My Programs" , @"Recent Programs" , @"Calender" ,@"Offers" , @"Logout"];
    
    arr_leftmenuItem_img= @[@"home2.png",@"1_icon.png",@"ad.png",@"3_icon.png",@"icon.png",@"4_icon.png", @"cal.png", @"6_icon.png", @"7_icon.png"];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    _lbl_username.text = [obj valueForKey:@"LoginUserName"];
    [_img_userimage sd_setImageWithURL:[NSURL URLWithString: [obj valueForKey:@"LoginUserImage"]]
                      placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_leftmenuItem_img count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"left_cell";
    Leftviewcell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[Leftviewcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    cell.lbl_Leftcell.text = [arr_leftmenuItem_title objectAtIndex:indexPath.row];
    cell.img_Leftcell.image = [UIImage imageNamed:[arr_leftmenuItem_img objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   if (indexPath.row==8)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"logedIn"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"role"];
        UINavigationController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainnew"];
        [self presentViewController:vc animated:NO completion:nil];
        NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
        [obj removeObjectForKey:@"clientprovalue"];
        [obj synchronize];
    }
    
    if (indexPath.row==0)
    {
        
        [self performSegueWithIdentifier:@"clienthome" sender:self];
    }
    if (indexPath.row==1)
    {
       
        [self performSegueWithIdentifier:@"clientprofile" sender:self];
    }
    
    if (indexPath.row==2)
    {
        [self performSegueWithIdentifier:@"clientaddprofessional" sender:self];
    }
    
    if (indexPath.row==3)
    {
        [self performSegueWithIdentifier:@"clientmyprofessional" sender:self];
    }
    if (indexPath.row==4)
    {
        [self performSegueWithIdentifier:@"clientmyprogram" sender:self];
    }
    
    if (indexPath.row==5)
    {
        [self performSegueWithIdentifier:@"clientrecentprogram" sender:self];
    }
    
    if (indexPath.row==6)
    {
        [self performSegueWithIdentifier:@"clientcalender" sender:self];    }
    
    if (indexPath.row==7)
    {
        [self performSegueWithIdentifier:@"offer" sender:self];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end

//
//  ptcalandar.h
//  PhysioMonkey
//
//  Created by prateek on 4/6/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendarDelegate.h"
#import "JTCalendarDelegateManager.h"
#import "JTCalendar.h"

@interface ptcalandar : UIViewController <JTCalendarDay,JTCalendarDelegate,WebDataDelegate,UIGestureRecognizerDelegate>
{
    NSDate *_todayDate;
    CGPoint point;
    NSDate *_minDate;
    NSDate *_maxDate;
    BOOL _selectionMode;
    NSMutableArray *_datesSelected;
    NSMutableDictionary *_eventsByDate;
    NSDate *_dateSelected;
    NSMutableArray *arrSelectedDates;
    NSArray * arr_color;
    NSArray * arr_start;
    NSArray * arr_data;
    NSString * str_startdate;
    NSString * str_title;
    NSString *str_color;
    NSString * str_enddate;
    NSDictionary * filter_dict;
    NSMutableDictionary *Request_data;
}
@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (strong, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;

@property (strong, nonatomic) JTCalendarDelegateManager *calendardelegateManager;
@property (strong, nonatomic) JTCalendarDayView *dayView;
@property (nonatomic,retain)WebService * webobj;


//IBOUTLET

@property (nonatomic,retain)IBOutlet UILabel * lbl_year;
@property (nonatomic,retain)IBOutlet UILabel * lbl_month;
@property (nonatomic,retain)NSString * str_cal_satus;
@property (nonatomic,retain)NSString * str_recentclientid;

//IBACTION
-(IBAction)Addbuttonclicked:(id)sender;
-(IBAction)previousbuttonclicked:(id)sender;
-(IBAction)nextmonthbuttonclicked:(id)sender;
-(IBAction)previousmonthbuttonclicked:(id)sender;
-(IBAction)nextyearbuttonclicked:(id)sender;
-(IBAction)previousyearbuttonclicked:(id)sender;


@end

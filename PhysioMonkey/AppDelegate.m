//
//  AppDelegate.m
//  PhysioMonkey
//
//  Created by prateek on 02/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "AppDelegate.h"
#import <PayPalPayment.h>
#import <PayPalMobile.h>
#import <IQKeyboardManager.h>

@interface AppDelegate ()
@end
@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"production_client_id",
                                    PayPalEnvironmentSandbox :     @"AZONebE_Im5nr1vx7p5gKP6TM93BudGFbQDZzq24mKNViiaw00KyeiB_W29g6LU1CXgWIebh-Nf-AhC5"}];
    
    NSLog(@"[[[NSUserDefaults standardUserDefaults]====%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"role"]);
    
     [[IQKeyboardManager sharedManager] setEnable:YES];
    
    
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"logedIn"] isEqualToString:@"login"])
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"role"] isEqualToString:@"3"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SWRevealViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"clientmode"];
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:loginViewController animated:NO completion:NULL];
            
        }
        else
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SWRevealViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"ptmode"];
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:loginViewController animated:NO completion:NULL];
            
        }
        
    }
    else
    {
        UIStoryboard *storyB = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *obj = [storyB instantiateViewControllerWithIdentifier:@"main_vc"];
        [self.window.rootViewController presentViewController:obj animated:NO completion:NULL];
    }
    
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

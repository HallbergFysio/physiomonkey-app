//
//  OpenProgramController.m
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "OpenProgramController.h"

@interface OpenProgramController ()

@end

@implementation OpenProgramController

#pragma mark -
#pragma mark:ViewLoadMethod
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tbl_opProgram.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[SVProgressHUD show];
    [self method];
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark - IBAction
-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)method
{
    //[SVProgressHUD show];
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod

-(void)webservicecalled
{
    //[SVProgressHUD show];
    [self.webobj GETAPI:nil methodname:@"v1/categoryoptions"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    arr_main = [dict valueForKeyPath:@"data"];
    arr_name = [arr_main valueForKeyPath:@"name"];
    arr_image = [arr_main valueForKeyPath:@"image"];
    arr_id = [arr_main valueForKeyPath:@"id"];
    [self.tbl_opProgram reloadData];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_id count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"librarycell";
    MyProfessionalCell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[MyProfessionalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    cell.lbl_OpenProgram.text = [arr_name objectAtIndex:indexPath.row];
    
   
   cell.img_OpenProgram.contentMode = UIViewContentModeScaleAspectFill;
   cell.img_OpenProgram.clipsToBounds=YES;
    
    [ cell.img_OpenProgram sd_setImageWithURL:[NSURL URLWithString:[arr_image objectAtIndex:indexPath.row]]
                            placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OpenMyProgramscontroller * open_obj = [self.storyboard instantiateViewControllerWithIdentifier:@"openmyprogram"];
    open_obj.dict_myprogram = arr_main[indexPath.row];
    open_obj.dict_ptdetail = _arr_dict;
    open_obj.str_catname = [arr_name objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:open_obj animated:NO];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

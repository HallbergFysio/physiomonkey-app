//
//  CreateProgramController.h
//  PhysioMonkey
//
//  Created by prateek on 17/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface CreateProgramController : UIViewController <UITableViewDelegate , UITableViewDataSource , UIActionSheetDelegate , WebDataDelegate , UIImagePickerControllerDelegate , UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate ,UITextViewDelegate>
{
    
    NSArray * arr_main;
    NSArray * arr_name;
    NSArray * arr_idd;
    
    NSString * str_addcount;
    NSString * choose_str;
    NSString * str_idsublib;
    NSString * sendertitle;
    NSString * mediatype;
    NSString * img_tpe;
    NSString * str_index;
    NSString * select_value;
    NSString * createprogramcat_idd;
    
    NSMutableArray * arr_rowwcount;
    NSMutableArray * arr_showbtn;
    NSMutableArray * arr_imagemutable;
    NSMutableArray * arr_discriptionmutable;
    NSMutableArray *arrTimeSlot;
    NSMutableArray * arr_detailfile;
    NSMutableArray * arr_2mdemoarr;
    NSArray * arr_detailtext;
    
    
    NSMutableDictionary *dicTimeSlot;
    NSMutableDictionary *Request_data;
    NSMutableDictionary *Post_data;
    NSMutableDictionary * ar;
    NSMutableDictionary * dict_textview;
    
    UIImage *chosenImage;
    NSInteger index_path;
    NSInteger delete_index;
    NSUserDefaults * row_count;
    NSIndexPath * indexPathOfTextField;
    NSMutableString * textview_mstr;
    NSIndexPath *indexPath34;
    UIPickerView * myPickerView;
    
}
@property (nonatomic,retain)IBOutlet UIButton * btn_addexercise;
@property (nonatomic,retain)IBOutlet UIButton * btn_sendbutton;

@property (nonatomic,retain)IBOutlet UITextField * txt_name;
@property (nonatomic,retain)IBOutlet UITextField * txt_selectcatagory;

@property (nonatomic,retain)IBOutlet UITextView * txt_adddiscription;
@property (nonatomic,retain)IBOutlet UITableView * tbl_createprogram;

@property(nonatomic,strong) UIImagePickerController* imagepick;
@property (nonatomic,retain)WebService * webobj;
@property (nonatomic,retain)NSString * str_clasname;
@property (nonatomic,retain)NSString * str_subclasname;
@property (nonatomic,retain)NSString * str_idclientid;
@property (nonatomic,retain)NSDictionary * dic_sublib;

-(IBAction)sendbuttonclicked:(id)sender;
-(IBAction)addexercideclicked:(id)sender;

@end

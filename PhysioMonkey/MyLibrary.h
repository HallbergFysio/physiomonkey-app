//
//  MyLibrary.h
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyLibrary : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate>
{
    NSArray * arr_main;
    NSArray * arr_id;
    NSArray * arr_name;
    NSArray * arr_image;
}

//IBOUTLET
@property(nonatomic,retain)IBOutlet UITableView * tbl_MYLIBRARY;
@property (nonatomic,retain)IBOutlet UIButton * btn_future;
@property (nonatomic,retain)WebService *webobj;

@end

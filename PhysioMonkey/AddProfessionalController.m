//
//  AddProfessionalController.m
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "AddProfessionalController.h"
#import "AddProfessionalCell.h"

@interface AddProfessionalController ()

@end

@implementation AddProfessionalController

#pragma mark - ViewLoadMethod
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.CommonMethodObj =[[CommonMethodClass alloc]init];
    
    self.tbl_addProfessional.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
    
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _SearchBar.text = @"";
    [SVProgressHUD show];
}

#pragma mark -  Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}

-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark -  IBAction


-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"clientaddprofhome" sender:self];
}

-(void)dismissKeyboard
{
    [_SearchBar resignFirstResponder];
}
-(void)RequestSend_clicked:(UIButton *)sender
{
    [SVProgressHUD show];
    NSLog(@"RequestSend_clicked");
    str_checkstatus = @"RequestSend_clicked";
    str_pageload = @"request";
    NSDictionary *commentData = filtered[sender.tag];
    NSLog(@"all data %@",commentData);
    NSString * str_pt_id = [commentData valueForKeyPath:@"id"];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_pt_id,@"pt_id", nil];
    [self.webObj postapi:Request_data methodname:@"v1/requestsendtopt"];
}

-(void)Cancel_clicked:(UIButton *)sender
{
    NSLog(@"Cancel_clicked");
    [SVProgressHUD show];
    str_checkstatus = @"cancel_clicked";
    str_pageload = @"cancel";
    NSDictionary *commentData = filtered[sender.tag];
    NSLog(@"all data %@",commentData);
    NSString * str_pt_id = [commentData valueForKeyPath:@"id"];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_pt_id,@"pt_id", nil];
    [self.webObj postapi:Request_data methodname:@"v1/cancelreqbyclient"];
    
}

-(void)Requestsent_clicked:(UIButton *)sender
{
    NSLog(@"Requestsent_clicked");
}


#pragma mark -
#pragma mark:HTTPWebserviceMethod

-(void)webservicecalled
{
    [SVProgressHUD show];
    [self.webObj postapi:nil methodname:@"v1/ptlist"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    NSString * str_msg = [dict valueForKeyPath:@"message"];
    if ([str_checkstatus isEqualToString:@"RequestSend_clicked"])
    {
        if ([str_msg isEqualToString:@"Request has been sent."])
        {
            str_checkstatus = @"";
            [self webservicecalled];
            [self.navigationController popViewControllerAnimated:NO];
        }
        else
        {
            str_checkstatus = @"";
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
    else if ([str_checkstatus isEqualToString:@"cancel_clicked"])
    {
        if ([str_msg isEqualToString:@"Request canceled successful"])
        {
            str_checkstatus = @"";
            [self webservicecalled];
            
            [self.navigationController popViewControllerAnimated:NO];
        }
        else
        {
            str_checkstatus = @"";
            [self webservicecalled];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
    else if ([str_msg isEqualToString:@"Request already sent."])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Request already sent!" atag:0 cncle:@"OK" othr:nil delegate:nil];
        str_checkstatus = @"";
        [self webservicecalled];
    }
    
    else
    {
        main_arr = [dict valueForKeyPath:@"data"];
        arr_name = [main_arr valueForKeyPath:@"name"];
        arr_image = [main_arr valueForKeyPath:@"image"];
        arr_clinicname = [main_arr valueForKeyPath:@"clinic_name"];
        arr_cliniclogo = [main_arr valueForKeyPath:@"clinic_logo"];
        arr_Requestsent = [main_arr valueForKeyPath:@"request_sent"];
        arr_id = [main_arr valueForKeyPath:@"id"];
        
        
        if ([str_pageload isEqualToString:@"cancel"])
        {
            isFilter = YES;
            filtered = [main_arr copy] ;
        }
        if ([str_pageload isEqualToString:@"request"])
        {
            isFilter = YES;
            filtered = [main_arr copy] ;
        }
        [self.tbl_addProfessional reloadData];
    }
    
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [filtered count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"ADDCell";
    AddProfessionalCell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[AddProfessionalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    
    if(isFilter)
    {
        NSArray *arr_name1 = [filtered valueForKeyPath:@"name"];
        NSArray *arr_clinic_name1 = [filtered valueForKeyPath:@"clinic_name"];
        NSArray *arr_image1 = [filtered valueForKeyPath:@"image"];
        NSArray *arr_clinic_logo1 = [filtered valueForKeyPath:@"clinic_logo"];
        NSArray *arr_Requestsent1 = [filtered valueForKeyPath:@"request_sent"];
        //NSArray *arr_id1 = [filtered valueForKeyPath:@"id"];
        
        cell.lbl_username.text = [arr_name1 objectAtIndex:indexPath.row];
        
        if ([[arr_clinic_name1 objectAtIndex:indexPath.row]isEqualToString:@""])
        {
            cell.lbl_userhospitalname.text = @"";
        }
        else
        {
            cell.lbl_userhospitalname.text = [arr_clinic_name1 objectAtIndex:indexPath.row];
        }
        [ cell.img_user sd_setImageWithURL:[NSURL URLWithString:[arr_image1 objectAtIndex:indexPath.row]]
                          placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
        
        [ cell.img_cliniclogo sd_setImageWithURL:[NSURL URLWithString:[arr_clinic_logo1 objectAtIndex:indexPath.row]]placeholderImage:[UIImage imageNamed:@""]];
        
        if ([[arr_Requestsent1 objectAtIndex:indexPath.row]isEqualToString:@"1"])
        {
            [cell.btn_RequestSend setHidden:YES];
            [cell.btn_Requestsent setHidden:NO];
            [cell.btn_Cancel setHidden:NO];
        }
        else
        {
            [cell.btn_RequestSend setHidden:NO];
            [cell.btn_Requestsent setHidden:YES];
            [cell.btn_Cancel setHidden:YES];
        }
        
        [self.webObj imageround1:cell.img_user];
        [self.CommonMethodObj imageRounded:cell.btn_Requestsent];
        [self.CommonMethodObj imageRounded:cell.btn_RequestSend];
        [self.CommonMethodObj imageRounded: cell.btn_Cancel];
        
        cell.btn_RequestSend.tag = indexPath.row;
        [cell.btn_RequestSend addTarget:self action:@selector(RequestSend_clicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btn_Cancel.tag = indexPath.row;
        [cell.btn_Cancel addTarget:self action:@selector(Cancel_clicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btn_Requestsent.tag = indexPath.row;
        [cell.btn_Requestsent addTarget:self action:@selector(Requestsent_clicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -  search bar method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSString *filterstr = [NSString stringWithFormat:@"%@",_SearchBar.text] ;
    NSString *substring = [NSString stringWithString:filterstr];
    NSLog(@"substring %@",substring);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@",substring];
    filtered =[[main_arr filteredArrayUsingPredicate:predicate] copy];
    
    if(searchText.length==0)
    {
        isFilter=NO;
        [self.tbl_addProfessional setHidden:YES];
    }
    else
    {
        isFilter=YES;
        NSLog(@"filtered %@",filtered);
        [self.tbl_addProfessional setHidden:NO];
        [self.tbl_addProfessional reloadData];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
@end

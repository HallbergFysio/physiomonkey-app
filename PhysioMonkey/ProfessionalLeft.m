//
//  ProfessionalLeft.m
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ProfessionalLeft.h"

@interface ProfessionalLeft ()

@end

@implementation ProfessionalLeft


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.table_view.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    arr_leftmenuItem_title= @[@"Home",@"My Profile", @"Verify New Client", @"My Library", @"Clients" , @"Recent Clients" , @"Subscription" ,@"Offers" , @"Logout"];
    
    arr_leftmenuItem_img= @[@"home2.png",@"1_icon.png",@"ad.png",@"lib.png",@"icon.png",@"4_icon.png", @"subscrip", @"6_icon.png", @"7_icon.png"];
    
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    [self.webobj imageround40:_img_userimage];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    
    _lbl_username.text = [obj valueForKey:@"LoginUserName"];
    
    [_img_userimage sd_setImageWithURL:[NSURL URLWithString: [obj valueForKey:@"LoginUserImage"]]
                      placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
    
}

#pragma mark -
#pragma mark: UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_leftmenuItem_img count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"left_cell";
    Leftviewcell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[Leftviewcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    cell.lbl_PROLeftcell.text = [arr_leftmenuItem_title objectAtIndex:indexPath.row];
    cell.img_PROLeftcell.image = [UIImage imageNamed:[arr_leftmenuItem_img objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        [self performSegueWithIdentifier:@"pt_home" sender:self];
    }
    if (indexPath.row==1)
    {
      [self performSegueWithIdentifier:@"pt_profile" sender:self];
    }
    if (indexPath.row==2)
    {
       [self performSegueWithIdentifier:@"pt_verifyclient" sender:self];
    }
    if (indexPath.row==3)
    {
        [self performSegueWithIdentifier:@"pt_library" sender:self];
    }
    
    if (indexPath.row==4)
    {
        [self performSegueWithIdentifier:@"pt_client" sender:self];
    }
    if (indexPath.row==5)
    {
       [self performSegueWithIdentifier:@"pt_resentclient" sender:self];
    }
    if (indexPath.row==6)
    {
      [self performSegueWithIdentifier:@"pt_sub" sender:self];
    }
    if (indexPath.row==7)
    {
        [self performSegueWithIdentifier:@"profhomeoffer2" sender:self];
    }
    if (indexPath.row==8)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"logedIn"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"role"];
        UINavigationController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainnew"];
        [self presentViewController:vc animated:NO completion:nil];
        NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
        [obj removeObjectForKey:@"clientprovalue"];
        [obj synchronize];
    }
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end

//
//  offerscontroller.h
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface offerscontroller : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate,UITextViewDelegate>
{
    NSArray * arr_name;
    NSArray * arr_image;
    NSArray * arr_link;
    NSMutableDictionary * Request_data;
}

//IBOUTLET
@property(nonatomic,retain)IBOutlet UITableView * tbl_offer;
@property (nonatomic,strong) WebService *webObj;


@end

//
//  calenderviewcontroller.m
//  PhysioMonkey
//
//  Created by prateek on 03/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "calenderviewcontroller.h"
@interface calenderviewcontroller ()


@end
@implementation calenderviewcontroller


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
   
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    
    UITapGestureRecognizer *oneFingerTwoTaps =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:nil];
    oneFingerTwoTaps.delegate=self;
    [oneFingerTwoTaps setNumberOfTapsRequired:1];
    [oneFingerTwoTaps setNumberOfTouchesRequired:1];
    [[self calendarContentView] addGestureRecognizer:oneFingerTwoTaps];
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    NSDate * da = [NSDate date];
    [_calendarManager setDate:da];
    _datesSelected = [NSMutableArray new];
    _selectionMode = NO;
  
}

-(void)viewWillAppear:(BOOL)animated
{
     [super viewWillAppear:animated];
     [self webservicecalled];
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    point= [touch previousLocationInView:self.view];
    //NSLog(@"Point - %f, %f",point.x,point.y);
    //NSLog(@"Touch");
    return NO;
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)webservicecalled
{
    [self.webobj GETAPI:nil methodname:@"v1/previouscomments"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    arr_data = [dict valueForKeyPath:@"data"];
    arr_color = [arr_data valueForKeyPath:@"color"];
    arr_start = [arr_data valueForKeyPath:@"start"];
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:[NSDate date]];
    
}

#pragma mark -
#pragma mark: UIButton Action

-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"clientcalandarhome" sender:self];
}

-(IBAction)Addbuttonclicked:(id)sender
{
    AddCommentController * ADDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCommentController"];
    [self.navigationController pushViewController:ADDVC animated:NO];
}

-(IBAction)previousbuttonclicked:(id)sender
{
    PreviousCommentController * ADDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PreviousCommentController"];
    [self.navigationController pushViewController:ADDVC animated:NO];
}

-(IBAction)nextmonthbuttonclicked:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"yearModeEnabled" forKey:@"false"];
    [_calendarManager.contentView loadNextPageWithAnimation];
    _calendarManager.settings.yearModeEnabled = false;
}

-(IBAction)previousmonthbuttonclicked:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"yearModeEnabled" forKey:@"false"];
    
    _calendarManager.settings.yearModeEnabled = false;
    
    [_calendarManager.contentView loadPreviousPageWithAnimation];
    
    _calendarManager.settings.yearModeEnabled = false;
    
}
-(IBAction)nextyearbuttonclicked:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"yearModeEnabled" forKey:@"true"];
    
    _calendarManager.settings.yearModeEnabled = true;
    
    [_calendarManager.contentView loadNextPageWithAnimation];
    
}

-(IBAction)previousyearbuttonclicked:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"yearModeEnabled" forKey:@"true"];
    
    _calendarManager.settings.yearModeEnabled = true;
    
    [_calendarManager.contentView loadPreviousPageWithAnimation];
}



#pragma mark - Buttons callback

- (IBAction)didSelectionModeTouch
{
    _selectionMode = !_selectionMode;
    
    if(_selectionMode)
    {
        [_datesSelected removeAllObjects];
        [_calendarManager reload];
    }
}


#pragma mark - CalendarManager delegate
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
   
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date])
    {
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor purpleColor];
        dayView.circleView.layer.borderColor = [UIColor clearColor].CGColor;
        dayView.circleView.layer.borderWidth = 1.f;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date])
    {
        
        dayView.circleView.hidden = NO;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor grayColor];
        dayView.circleView.backgroundColor = [UIColor clearColor];
        dayView.circleView.layer.borderColor = [[UIColor colorWithRed: 153.0/255.0 green:187.0/255.0 blue:81./255.0 alpha:1.0]CGColor];
        dayView.circleView.layer.borderWidth = 1.f;
        
        
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date])
    {
        
        NSDateFormatter *dateFormatter1234 = [[NSDateFormatter alloc] init];
        [dateFormatter1234 setDateFormat:@"yyyy-MM-dd"];
        NSString *todaydate = [dateFormatter1234 stringFromDate:dayView.date];
        //NSLog(@"todaydate==%@",todaydate);
        
        
        NSMutableArray * chatonly_arr  = [NSMutableArray new];
        for (NSDictionary * e_dict in arr_data)
        {
            
            //NSLog(@"filter dict==%@",e_dict);
            NSString *str = [NSString stringWithFormat:@"%@",[e_dict objectForKey:@"start"]];
            //convert string into date
            NSDateFormatter *format_date = [[NSDateFormatter alloc] init];
            [format_date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *dte = [format_date dateFromString:str];
            //convert date into string
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *eventdate = [dateFormatter stringFromDate:dte];
            //NSLog(@"eventdate==%@",eventdate);
            
            
            if([todaydate isEqualToString:eventdate])
            {
                if (![chatonly_arr containsObject:e_dict])
                {
                    [chatonly_arr addObject:e_dict];
                }
                str_startdate = [[chatonly_arr valueForKeyPath:@"start"]objectAtIndex:0];
                str_title = [[chatonly_arr valueForKeyPath:@"title"]objectAtIndex:0];
                str_enddate = [[chatonly_arr valueForKeyPath:@"end"]objectAtIndex:0];
               // NSLog(@"chatonly===========%@",chatonly_arr);
                //NSLog(@"str_startdate===========%@",str_startdate);
                [self CreateCommentPopover];
            }
        }
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blackColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
    }
    
    // Another day of the current month
    else
    {
        dayView.circleView.hidden = NO;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
        dayView.circleView.backgroundColor = [UIColor clearColor];
        dayView.circleView.layer.borderColor = [[UIColor colorWithRed: 153.0/255.0 green:187.0/255.0 blue:81./255.0 alpha:1.0]CGColor];
        dayView.circleView.layer.borderWidth = 1.f;
        
    }
    ///////////////i m working
    
    if([self haveTaskForDay:dayView.date])
    {
        dayView.dotView.hidden = NO;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        
        
        if ([str_color isEqualToString:@"Blue"])
        {
            dayView.circleView.backgroundColor = [UIColor blueColor];
        }
        else if ([str_color isEqualToString:@"Green"])
        {
            dayView.circleView.backgroundColor = [UIColor greenColor];
            dayView.textLabel.textColor = [UIColor darkGrayColor];
        }
        else
        {
            dayView.circleView.backgroundColor = [UIColor redColor];
        }
        
        dayView.circleView.layer.borderColor = [[UIColor whiteColor]CGColor];
        dayView.circleView.layer.borderWidth = 1.f;
        [dayView setUserInteractionEnabled:YES];
    }
    
    else
    {
        [dayView setUserInteractionEnabled:NO];
        dayView.dotView.hidden = YES;
    }
}
- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";
    }
    menuItemView.text = [dateFormatter stringFromDate:date];
    [menuItemView setTextColor:[UIColor whiteColor]];
    [menuItemView setFont:[UIFont systemFontOfSize:14]];
    
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString * calendarDate1 = [formatter stringFromDate:_dateSelected];
    
    NSDateFormatter * formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy-MM-dd"];
    NSString * calendarDate2 = [formatter1 stringFromDate:[NSDate date]];
    
   if([calendarDate1 isEqualToString:calendarDate2])
   {
       NSDateFormatter *dateFormatter1234 = [[NSDateFormatter alloc] init];
       [dateFormatter1234 setDateFormat:@"yyyy-MM-dd"];
       NSString *todaydate = [dateFormatter1234 stringFromDate:dayView.date];
       NSLog(@"todaydate==%@",todaydate);
       
       
       NSMutableArray * chatonly_arr  = [NSMutableArray new];
       for (NSDictionary * e_dict in arr_data)
       {
           
           NSLog(@"filter dict==%@",e_dict);
           NSString *str = [NSString stringWithFormat:@"%@",[e_dict objectForKey:@"start"]];
           //convert string into date
           NSDateFormatter *format_date = [[NSDateFormatter alloc] init];
           [format_date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
           NSDate *dte = [format_date dateFromString:str];
           //convert date into string
           NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
           [dateFormatter setDateFormat:@"yyyy-MM-dd"];
           NSString *eventdate = [dateFormatter stringFromDate:dte];
           NSLog(@"eventdate==%@",eventdate);
           
           
           if([todaydate isEqualToString:eventdate])
           {
               if (![chatonly_arr containsObject:e_dict])
               {
                   [chatonly_arr addObject:e_dict];
               }
               str_startdate = [[chatonly_arr valueForKeyPath:@"start"]objectAtIndex:0];
               str_title = [[chatonly_arr valueForKeyPath:@"title"]objectAtIndex:0];
               str_enddate = [[chatonly_arr valueForKeyPath:@"end"]objectAtIndex:0];
               NSLog(@"chatonly===========%@",chatonly_arr);
               NSLog(@"str_startdate===========%@",str_startdate);
               [self CreateCommentPopover];
           }
       }
       dayView.circleView.hidden = NO;
       dayView.circleView.backgroundColor = [UIColor blackColor];
       dayView.dotView.backgroundColor = [UIColor whiteColor];
   }
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^
     {
         dayView.circleView.transform = CGAffineTransformIdentity;
         
         [_calendarManager reload];
         
     } completion:nil];
    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(_calendarManager.settings.weekModeEnabled){
        return;
        
    }
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date])
    {
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending)
        {
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
}
-(void)CreateCommentPopover
{
    ARSPopover *popoverController = [ARSPopover new];
    popoverController.sourceRect =  CGRectMake(point.x-10,point.y-5,170,95);//for X , Y,  Width ,Height
    popoverController.contentSize = CGSizeMake(170,95);//for Height and Width
    [popoverController setBackgroundColor:[UIColor whiteColor]];
    //popoverController.arrowDirection = UIPopoverArrowDirectionUp;
    
    [self presentViewController:popoverController animated:YES completion:^{
        [popoverController insertContentIntoPopover:^(ARSPopover *popover, CGSize popoverPresentedSize, CGFloat popoverArrowHeight)
         {
              NSString * endstring = [NSString stringWithFormat:@"%@ To %@",str_startdate,str_enddate];
             UILabel*lbl_startdate=[[UILabel alloc]initWithFrame:CGRectMake(10,8,170,22)];
             [lbl_startdate setTextColor:[UIColor lightGrayColor]];
             [lbl_startdate setText:str_startdate];
             [lbl_startdate setFont:[UIFont systemFontOfSize:13]];
             [lbl_startdate setBackgroundColor:[UIColor clearColor]];
             
             UILabel*lbl_title=[[UILabel alloc]initWithFrame:CGRectMake(10,30, 170,22)];
             [lbl_title setTextColor:[UIColor lightGrayColor]];
             [lbl_title setText:str_title];
             [lbl_title setFont:[UIFont systemFontOfSize:14]];
             [lbl_title setBackgroundColor:[UIColor clearColor]];
             
             UILabel*lbl_start_end_date=[[UILabel alloc]initWithFrame:CGRectMake(10,52, 170,35)];
             [lbl_start_end_date setTextColor:[UIColor lightGrayColor]];
             [lbl_start_end_date setText:endstring];
             lbl_start_end_date.numberOfLines = 2;
             [lbl_start_end_date setFont:[UIFont systemFontOfSize:12]];
             [lbl_start_end_date setBackgroundColor:[UIColor clearColor]];
             
             [popover.view addSubview:lbl_title];
             [popover.view addSubview:lbl_startdate];
             [popover.view addSubview:lbl_start_end_date];
             
            
         }];
    }];
    
}


#pragma mark - Date selection

- (BOOL)isInDatesSelected:(NSDate *)date
{
    for(NSDate *dateSelected in _datesSelected)
    {
        if([_calendarManager.dateHelper date:dateSelected isTheSameDayThan:date]){
            
            return YES;
        }
    }
    
    return NO;
}

- (void)selectDates
{
    NSDate * startDate = [_datesSelected firstObject];
    NSDate * endDate = [_datesSelected lastObject];
    
    if([_calendarManager.dateHelper date:startDate isEqualOrAfter:endDate]){
        NSDate *nextDate = endDate;
        while ([nextDate compare:startDate] == NSOrderedAscending) {
            [_datesSelected addObject:nextDate];
            nextDate = [_calendarManager.dateHelper addToDate:nextDate days:1];
        }
    }
    else {
        NSDate *nextDate = startDate;
        while ([nextDate compare:endDate] == NSOrderedAscending) {
            [_datesSelected addObject:nextDate];
            nextDate = [_calendarManager.dateHelper addToDate:nextDate days:1];
        }
    }
}

#pragma mark - Fake data

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL) haveTaskForDay :(NSDate *)date
{
    
    if (arr_data.count == 0 || arr_data == nil)
    {
        return false;
    }
    
    NSDateFormatter *format_date = [[NSDateFormatter alloc] init];
    [format_date setDateFormat:@"yyyy-MM-dd"];
    NSString *dtestr = [format_date stringFromDate:date];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"start contains[c] %@",dtestr];

    
    NSArray  *filteredArray = [arr_data filteredArrayUsingPredicate:predicate];
    
    
    
    
    if (filteredArray.count == 0 || filteredArray == nil)
    {
        return false;
    }
    
    str_color = [NSString stringWithFormat:@"%@",[[filteredArray objectAtIndex:0 ]objectForKey:@"color"]];
    
    
    return true;

    
    
}

- (void)createRandomEvents
{
    _eventsByDate = [NSMutableDictionary new];
    
    for(int i = 0; i < 30; ++i)
    {
        
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
        
        if(!_eventsByDate[key])
        {
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
}

@end

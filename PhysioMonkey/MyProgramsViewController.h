//
//  MyProgramsViewController.h
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MyProgramsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate>

{
    NSArray * arr_name;
    NSArray * arr_image;
    NSArray * arr_phone;
    NSArray * arr_email;
    NSArray * arr_Requestsent;
    NSArray * arr_id;
    NSArray * main_arr;
    NSArray *Filterarr_name;
    NSArray * Filterarr_image;
    NSArray *  Filterarr_phone;
    NSArray *  Filterarr_email;
    NSArray *  Filterarr_id;
    NSArray * filtered;
    
    BOOL isFilter;
    NSMutableDictionary * Request_data;
    NSString * str_checkstatus;
    
}
//IBOUTLET

@property (nonatomic,retain) IBOutlet UISearchBar *search_bar;
@property (nonatomic,retain) IBOutlet UITableView * tbl_MyPrograms;
@property (nonatomic,strong) WebService *webObj;
@property (nonatomic,strong) CommonMethodClass*CommonMethodObj;

@end

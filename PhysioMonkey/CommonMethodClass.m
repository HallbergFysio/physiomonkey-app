//
//  CommonMethodClass.m
//  samayLa
//
//  Created by prateek on 30/11/16.
//  Copyright © 2016 alleviatetech. All rights reserved.
//

#import "CommonMethodClass.h"
#import <QuartzCore/QuartzCore.h>
@interface CommonMethodClass ()

@end

@implementation CommonMethodClass

- (void)viewDidLoad
{
    [super viewDidLoad];
 
}

-(void)buttonbordercolor:(UIButton*)btn
{
    btn.layer.cornerRadius=2;
    btn.layer.borderWidth = 1.0f;
    btn.clipsToBounds=YES;
    btn.layer.borderColor = [[UIColor colorWithRed:60.0f/255.0f green:154.0f/255.0f blue:116.0f/255.0f alpha:1.0] CGColor];
}

-(void)imageRounded:(UIButton*)btn
{
    btn.layer.cornerRadius=2;
    btn.layer.borderWidth = 1.0f;
    btn.clipsToBounds=YES;
    btn.layer.borderColor = [[UIColor clearColor]CGColor];
}
-(void)imageRoundedforview:(UIView*)vew
{
    vew.layer.cornerRadius=2;
    vew.clipsToBounds=YES;
    vew.layer.borderWidth = 1.0f;
    vew.layer.borderColor = [[UIColor colorWithRed:43.0f/255.0f green:116.0f/255.0f blue:190.0f/255.0f alpha:1.0] CGColor];
}

-(void)imageRoundedfordifferentborder:(UIButton*)btun
{
    btun.layer.cornerRadius=5;
    btun.clipsToBounds=YES;
    btun.layer.borderWidth = 1.2f;
    btun.layer.borderColor = [[UIColor colorWithRed:97.0f/255.0f green:150.0f/255.0f blue:204.0f/255.0f alpha:1.0] CGColor];
}
-(void)myprogrambuttonrounded:(UIButton*)btun
{
    btun.layer.cornerRadius=2;
    btun.clipsToBounds=YES;
    btun.layer.borderWidth = 1.2f;
    btun.layer.borderColor = [[UIColor clearColor] CGColor];
}
-(void)imageRoundedfordifferentborder2:(UIView*)btun
{
    btun.layer.cornerRadius=0;
    btun.clipsToBounds=YES;
    btun.layer.borderWidth = 1.2f;
    btun.layer.borderColor = [[UIColor clearColor]CGColor];
}
-(void)textfieldRounded:(UITextField*)txt
{
    txt.layer.cornerRadius=5;
    txt.clipsToBounds=YES;
}
-(void)imageroundedforimage:(UIImageView*)img
{
    img.layer.cornerRadius = 45;
    img.layer.masksToBounds = YES;
    img.layer.borderWidth = 1.5f;
    img.clipsToBounds = YES;
    img.layer.borderColor = [[UIColor  colorWithRed:114.0f/255.0f green:246.0f/255.0f blue:147.0f/255.0f alpha:1.0] CGColor];

}
-(void)imageround40:(UIImageView*)img
{
    img.layer.cornerRadius = 40;
    img.layer.masksToBounds = YES;
    img.layer.borderWidth = 1.5f;
    img.clipsToBounds = YES;
    img.layer.borderColor = [[UIColor clearColor] CGColor];
    
}
-(void)showAlertView:(NSString *)aTitle msg:(NSString *)message atag:(NSInteger)alerttag cncle:(NSString*)cancle othr:(NSString*)other othr2:(NSString *)other2 delegate:(id)delgate
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:aTitle message:message delegate:delgate cancelButtonTitle:cancle  otherButtonTitles:other,other2,nil];
    [alert show];
}
@end

//
//  HomeController.h
//  PhysioMonkey
//
//  Created by prateek on 03/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCBarView.h"
#import "CCButtonMenu.h"

@interface HomeController : UIViewController <UIGestureRecognizerDelegate>
{
    SWRevealViewController * menuSRVC;
}

//IBOUTLET
@property (nonatomic,retain)IBOutlet UIButton *  btn_myprofessional;
@property (nonatomic,retain)IBOutlet UIButton *  btn_callandar;


//IBACTION
-(IBAction)profilebuttonclicked:(id)sender;
-(IBAction)Logoutbuttonclicked:(id)sender;
-(IBAction)AddProfessionalButtonclicked:(id)sender;
-(IBAction)MYProgramsButtonclicked:(id)sender;
-(IBAction)RecentProgramButtonclicked:(id)sender;
-(IBAction)MyProfessionalButtonClicked:(id)sender;
-(IBAction)offerButtonClicked:(id)sender;
-(IBAction)callenderbuttonclicked:(id)sender;
@end

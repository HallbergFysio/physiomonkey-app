//
//  OpenProgramController.h
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface OpenProgramController : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate,SDWebImageManagerDelegate>
{
    NSArray * arr_main;
    NSArray * arr_id;
    NSArray * arr_name;
    NSArray * arr_image;
}

//IBOUTLET

@property (nonatomic,retain) IBOutlet UITableView * tbl_opProgram;
@property (nonatomic,retain) NSDictionary * arr_dict;
@property (nonatomic,retain) WebService *webobj;
@end

//
//  AddCommentController.m
//  PhysioMonkey
//
//  Created by prateek on 03/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "AddCommentController.h"

@interface AddCommentController ()
@end
@implementation AddCommentController


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tbl_view setHidden:YES];
    self.navigationItem.hidesBackButton=YES;
    strptid = [_dic_comment objectForKey:@"pt_id"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    
    if ([_str_classname isEqualToString:@"edit"])
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
        NSDate *dte = [dateFormat dateFromString:[_dic_comment objectForKey:@"end"]];
        
        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:dte];
        
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        [dateFormat2 setTimeZone:[NSTimeZone localTimeZone]];
        NSDate *dte2 = [dateFormat2 dateFromString:[_dic_comment objectForKey:@"start"]];
        
        NSDateComponents *components3 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:dte2];
        
        NSInteger monthNumber1 = [components3 month];
        NSInteger monthNumber2 = [components2 month];
        
        NSString * dateString = [NSString stringWithFormat: @"%ld", (long)monthNumber1];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM"];
        NSDate* myDate = [dateFormatter dateFromString:dateString];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMMM"];
        NSString *stringFromDate = [formatter stringFromDate:myDate];
        NSString *newmonthString = [stringFromDate substringToIndex:3];
        NSLog(@"1%@", newmonthString);
        
        NSString * dateString2 = [NSString stringWithFormat: @"%ld", (long)monthNumber2];
        NSDateFormatter* dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"MM"];
        NSDate* myDate2 = [dateFormatter2 dateFromString:dateString2];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"MMMM"];
        NSString *stringFromDate2 = [formatter2 stringFromDate:myDate2];
        NSString *newmonthString2 = [stringFromDate2 substringToIndex:3];
        NSLog(@"2%@", newmonthString2);
        
        str_typeid = [_dic_comment objectForKey:@"type_id"];
        
        _txt_details.text  = [_dic_comment objectForKey:@"details"];;
        _txt_title.text    =  [_dic_comment objectForKey:@"title"];
        _txt_type.text =     [_dic_comment objectForKey:@"type"];
        
        _txt_endminute.text = [NSString stringWithFormat:@"%ld",(long)[components2 minute]];
        _txt_endyear.text = [NSString stringWithFormat:@"%ld",(long)[components2 year]];
        _txt_endhours.text = [NSString stringWithFormat:@"%ld",(long)[components2 hour]];
        _txt_endmonthname.text = newmonthString2;
        _txt_enddays.text = [NSString stringWithFormat:@"%ld",(long)[components2 day]];
        _txt_endampm.text = [NSString stringWithFormat:@"%@",@"PM"];
        
        _txt_startminute.text = [NSString stringWithFormat:@"%ld",(long)[components3 minute]];
        _txt_Startyear.text = [NSString stringWithFormat:@"%ld",(long)[components3 year]];
        _txt_starthour.text = [NSString stringWithFormat:@"%ld",(long)[components3 hour]];
        _txt_Startmonthname.text = newmonthString;
        _txt_Startdays.text = [NSString stringWithFormat:@"%ld",(long)[components3 day]];
        _txt_startampm.text = [NSString stringWithFormat:@"%@",@"PM"];
    }
    
    else
    {
        NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:[NSDate date]];
        
        NSInteger day = [components1 day];
        NSInteger year = [components1 year];
        NSInteger hours = [components1 hour];
        NSInteger minute = [components1 minute];
        
        NSDateFormatter *calMonth = [[NSDateFormatter alloc] init];
        [calMonth setDateFormat:@"MMMM"];
        NSDate *date = [NSDate date];
        NSString * cal_month = [NSString stringWithFormat:@"%@",[calMonth stringFromDate:date]];
        NSString *newmonthString = [cal_month substringToIndex:3];
        
        
        _txt_Startyear.text = [NSString stringWithFormat:@"%ld",(long)year];
        _txt_endyear.text = [NSString stringWithFormat:@"%ld",(long)year];
        
        _txt_Startmonthname.text = [NSString stringWithFormat:@"%@",newmonthString];
        _txt_endmonthname.text = [NSString stringWithFormat:@"%@",newmonthString];
        
        _txt_Startdays.text = [NSString stringWithFormat:@"%ld",(long)day];
        _txt_enddays.text = [NSString stringWithFormat:@"%ld",(long)day];
        
        _txt_starthour.text = [NSString stringWithFormat:@"%ld",(long)hours];
        _txt_endhours.text = [NSString stringWithFormat:@"%ld",(long)hours];
        
        _txt_startminute.text = [NSString stringWithFormat:@"%ld",(long)minute];
        _txt_endminute.text = [NSString stringWithFormat:@"%ld",(long)minute];
        
        _txt_startampm.text = [NSString stringWithFormat:@"%@",@"PM"];
        _txt_endampm.text = [NSString stringWithFormat:@"%@",@"PM"];
    }
    
    
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    self.tbl_view.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.webobj TextfieldBorderColor:_txt_type];
    [self.webobj TextfieldBorderColor:_txt_title];
    [self.webobj TextfieldBorderColor:_txt_details];
    [self.webobj TextfieldBorderColor:_txt_professional];
    [self.webobj TextfieldBorderColor:_txt_Startmonthname];
    [self.webobj TextfieldBorderColor:_txt_Startdays];
    [self.webobj TextfieldBorderColor:_txt_Startyear];
    [self.webobj TextfieldBorderColor:_txt_endmonthname];
    [self.webobj TextfieldBorderColor:_txt_enddays];
    [self.webobj TextfieldBorderColor:_txt_endyear];
    [self.webobj TextfieldBorderColor:_txt_starthour];
    [self.webobj TextfieldBorderColor:_txt_startminute];
    [self.webobj TextfieldBorderColor:_txt_startampm];
    [self.webobj TextfieldBorderColor:_txt_endampm];
    [self.webobj TextfieldBorderColor:_txt_endminute];
    [self.webobj TextfieldBorderColor:_txt_endhours];
    
    [self webservicecalled];
    
    NSMutableArray * typearr = [[NSMutableArray alloc]init];
    UIBarButtonItem * typespace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * typecancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * typedone = [[UIBarButtonItem alloc]init];
    UIToolbar *typetoolbar=[[UIToolbar alloc]init];
    UIPickerView* typepicker = [[UIPickerView alloc]init];
    [self createpickerview:typepicker atag:1 txt:_txt_type toolbar:typetoolbar barcancel:typecancel bardone:typedone bartag:1 space:typespace arr:
     typearr];
    
    NSMutableArray * startmontharr = [[NSMutableArray alloc]init];
    UIBarButtonItem * startmonthspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startmonthcancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startmonthdone = [[UIBarButtonItem alloc]init];
    UIToolbar *startmonthtoolbar=[[UIToolbar alloc]init];
    UIPickerView* startmonthpicker = [[UIPickerView alloc]init];
    [self createpickerview:startmonthpicker atag:2 txt:_txt_Startmonthname toolbar:startmonthtoolbar barcancel:startmonthcancel bardone:startmonthdone bartag:2 space:startmonthspace arr:startmontharr];
    
    NSMutableArray *  endmontharr = [[NSMutableArray alloc]init];
    UIBarButtonItem * endmonthspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endmonthcancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endmonthdone = [[UIBarButtonItem alloc]init];
    UIToolbar *endmonthtoolbar=[[UIToolbar alloc]init];
    UIPickerView* endmonthpicker = [[UIPickerView alloc]init];
    [self createpickerview:endmonthpicker atag:3 txt:_txt_endmonthname toolbar:endmonthtoolbar barcancel:endmonthcancel bardone:endmonthdone bartag:3 space:endmonthspace arr:endmontharr];
    
    NSMutableArray *  endhourarr = [[NSMutableArray alloc]init];
    UIBarButtonItem * endhourspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endhourcancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endhourdone = [[UIBarButtonItem alloc]init];
    UIToolbar *endhourtoolbar=[[UIToolbar alloc]init];
    UIPickerView* endhourpicker = [[UIPickerView alloc]init];
    [self createpickerview:endhourpicker atag:5 txt:_txt_endhours toolbar:endhourtoolbar barcancel:endhourcancel bardone:endhourdone bartag:5 space:endhourspace arr:endhourarr];
    
    NSMutableArray *  starthourarr = [[NSMutableArray alloc]init];
    UIBarButtonItem * starthourspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * starthourcancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * starthourdone = [[UIBarButtonItem alloc]init];
    UIToolbar *starthourtoolbar=[[UIToolbar alloc]init];
    UIPickerView* starthourpicker = [[UIPickerView alloc]init];
    [self createpickerview:starthourpicker atag:4 txt:_txt_starthour toolbar:starthourtoolbar barcancel:starthourcancel bardone:starthourdone bartag:4 space:starthourspace arr:starthourarr];
    
    NSMutableArray *  startdayarr = [[NSMutableArray alloc]init];
    UIBarButtonItem * startdayspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startdaycancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startdaydone = [[UIBarButtonItem alloc]init];
    UIToolbar *startdaytoolbar=[[UIToolbar alloc]init];
    UIPickerView* startdaypicker = [[UIPickerView alloc]init];
    [self createpickerview:startdaypicker atag:6 txt:_txt_Startdays toolbar:startdaytoolbar barcancel:startdaycancel bardone:startdaydone bartag:6 space:startdayspace arr:startdayarr];
    
    NSMutableArray *  enddayarr = [[NSMutableArray alloc]init];
    UIBarButtonItem * enddayspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * enddaycancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * enddaydone = [[UIBarButtonItem alloc]init];
    UIToolbar *enddaytoolbar=[[UIToolbar alloc]init];
    UIPickerView* enddaypicker = [[UIPickerView alloc]init];
    [self createpickerview:enddaypicker atag:7 txt:_txt_enddays toolbar:enddaytoolbar barcancel:enddaycancel bardone:enddaydone bartag:7 space:enddayspace arr:enddayarr];
    
    NSMutableArray *  startAmpmarr = [[NSMutableArray alloc]init];
    UIBarButtonItem * startampmspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startampmcancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startampmdone = [[UIBarButtonItem alloc]init];
    UIToolbar *startampmtoolbar=[[UIToolbar alloc]init];
    UIPickerView* startampmpicker = [[UIPickerView alloc]init];
    [self createpickerview:startampmpicker atag:8 txt:_txt_startampm toolbar:startampmtoolbar barcancel:startampmcancel bardone:startampmdone bartag:8 space:startampmspace arr:startAmpmarr];
    
    NSMutableArray *  endampmarr = [[NSMutableArray alloc]init];
    UIBarButtonItem * endampmspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endampmcancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endampmdone = [[UIBarButtonItem alloc]init];
    UIToolbar *endampmtoolbar=[[UIToolbar alloc]init];
    UIPickerView* endampmpicker = [[UIPickerView alloc]init];
    [self createpickerview:endampmpicker atag:9 txt:_txt_endampm toolbar:endampmtoolbar barcancel:endampmcancel bardone:endampmdone bartag:9 space:endampmspace arr:endampmarr];
    
    NSMutableArray *  startyeararr = [[NSMutableArray alloc]init];
    UIBarButtonItem * startyearspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startyearcancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startyeardone = [[UIBarButtonItem alloc]init];
    UIToolbar *startyeartoolbar=[[UIToolbar alloc]init];
    UIPickerView* startyearicker = [[UIPickerView alloc]init];
    [self createpickerview:startyearicker atag:10 txt:_txt_Startyear toolbar:startyeartoolbar barcancel:startyearcancel bardone:startyeardone bartag:10 space:startyearspace arr:startyeararr];
    
    NSMutableArray *  endyeararr = [[NSMutableArray alloc]init];
    UIBarButtonItem * endyearspace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endyearcancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endyeardone = [[UIBarButtonItem alloc]init];
    UIToolbar *endyeartoolbar=[[UIToolbar alloc]init];
    UIPickerView* endyearpicker = [[UIPickerView alloc]init];
    [self createpickerview:endyearpicker atag:11 txt:_txt_endyear toolbar:endyeartoolbar barcancel:endyearcancel bardone:endyeardone bartag:11 space:endyearspace arr:endyeararr];
    
    NSMutableArray *  startminutearr = [[NSMutableArray alloc]init];
    UIBarButtonItem * startminutespace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startminutecancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * startminutedone = [[UIBarButtonItem alloc]init];
    UIToolbar * startminutetoolbar=[[UIToolbar alloc]init];
    UIPickerView* startminutepicker = [[UIPickerView alloc]init];
    [self createpickerview:startminutepicker atag:12 txt:_txt_startminute toolbar:startminutetoolbar barcancel:startminutecancel bardone:startminutedone bartag:12 space:startminutespace arr:startminutearr];
    
    NSMutableArray *  endminutearr = [[NSMutableArray alloc]init];
    UIBarButtonItem * endminutespace = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endminutecancel = [[UIBarButtonItem alloc]init];
    UIBarButtonItem * endminutedone = [[UIBarButtonItem alloc]init];
    UIToolbar *endminutetoolbar=[[UIToolbar alloc]init];
    UIPickerView* endminutepicker = [[UIPickerView alloc]init];
    [self createpickerview:endminutepicker atag:13 txt:_txt_endminute toolbar:endminutetoolbar barcancel:endminutecancel bardone:endminutedone bartag:13 space:endminutespace arr:endminutearr];
    
    arr_year =[NSMutableArray arrayWithObjects:@"2017",@"2018",@"2019",@"2020",@"2021",@"2022",@"2023",@"2024",nil];
    
    arr_minute =[NSMutableArray arrayWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31",@"32",@"33",@"34",@"35",@"36",@"37",@"38",@"39",@"40",@"41",@"42",@"43",@"44",@"45",@"46",@"47",@"48",@"49",@"50",@"51",@"52",@"53",@"54",@"55",@"56",@"57",@"58",@"59",nil];
    
    [self.txt_professional addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    arr_monthname =[NSMutableArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"Jun",@"July",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec",nil];
    
    arr_hours =[NSMutableArray arrayWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",nil];
    
    arr_days =[NSMutableArray arrayWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31",nil];
    
    arrampm =[NSMutableArray arrayWithObjects:@"AM",@"PM",nil];
    
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (IS_IPHONE_4_OR_LESS)
    {
        self.scroll_view.contentSize = CGSizeMake(0, 500);
    }
    if (IS_IPHONE_5)
    {
        self.scroll_view.contentSize = CGSizeMake(0, 500);
    }
    
    if (IS_IPHONE_6)
    {
        self.scroll_view.contentSize = CGSizeMake(0, 500);
    }
}

#pragma mark -
#pragma mark: Navigation Baar Menu
-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    
}
#pragma mark -
#pragma mark:Common Method

-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)dismissKeyboard
{
    [_txt_title resignFirstResponder];
    [_txt_details resignFirstResponder];
    [_txt_professional resignFirstResponder];
}

-(void)createpickerview:(UIPickerView*)pick atag:(NSInteger)tag txt:(UITextField*)txtfield toolbar:(UIToolbar*)bartool barcancel:(UIBarButtonItem*)cancelbarbutton bardone:(UIBarButtonItem*)donebarbutton bartag:(NSInteger)barbuttontag space:(UIBarButtonItem*)flexspace arr:(NSMutableArray*)arrbaritems

{
    pick = [[UIPickerView alloc]initWithFrame:
            CGRectMake(10, 100, self.view.frame.size.width, 200)];
    pick.dataSource = self;
    pick.delegate = self;
    [pick setTag:tag];
    pick.showsSelectionIndicator = YES;
    [pick setBackgroundColor:[UIColor colorWithRed:149.0f/255.0f green:197.0f/255.0f blue:117.0f/255.0f alpha:1.0]];
    [pick setTintColor:[UIColor whiteColor]];
    txtfield.inputView = pick;
    bartool=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [bartool sizeToFit];
    cancelbarbutton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(onClickingCancelBtn:)];
    [cancelbarbutton setTag:barbuttontag];
    [cancelbarbutton setTintColor:[UIColor whiteColor]];
    arrbaritems = [[NSMutableArray alloc] init];
    flexspace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [arrbaritems addObject:cancelbarbutton];
    [arrbaritems addObject:flexspace];
    [bartool setBarTintColor:[UIColor colorWithRed:69.0f/255.0f green:28.0f/255.0f blue:113.0f/255.0f alpha:1.0]];
    donebarbutton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(onClickingDoneBtn:)];
    [donebarbutton setTag:barbuttontag];
    [donebarbutton setTintColor:[UIColor whiteColor]];
    [arrbaritems addObject:donebarbutton];
    [bartool setItems:arrbaritems animated:YES];
    [txtfield setInputAccessoryView:bartool];
}


-(void)updateSearchArray:(NSString *)searchText
{
    if(searchText.length==0)
    {
        isFilter=NO;
        [self.tbl_view setHidden:YES];
    }
    else
    {
        
        isFilter=YES;
        searchArray=[[NSMutableArray alloc]init];
        NSMutableArray *Privateonly_arr = [NSMutableArray new];
        
        for (NSDictionary *dict1 in arr_ptdata)
        {
            NSString *status = [NSString stringWithFormat:@"%@",dict1[@"name"]];
            NSRange stringRange=[status rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(stringRange.location !=NSNotFound)
            {
                [self.tbl_view setHidden:NO];
                [searchArray addObject:status];
                [Privateonly_arr addObject:dict1];
                arridpt = [Privateonly_arr valueForKeyPath:@"id"];
                
            }
            [self.tbl_view reloadData];
        }
    }
}
-(void)onClickingDoneBtn:(UIButton*)sender
{
    //type
    if (sender.tag==1)
    {
        if ([str_checktype isEqualToString:@"paintype"])
        {
            
            [myPickerView removeFromSuperview];
            [_txt_type resignFirstResponder];
        }
        else
        {
            [self.txt_type setText:[arr_name objectAtIndex:0]];
            str_typeid = [arr_typeid objectAtIndex:0];
            [myPickerView removeFromSuperview];
            [_txt_type resignFirstResponder];
        }
        
    }
    
    //startmonth
    else if (sender.tag==2)
    {
        if ([str_checkstartmonth isEqualToString:@"startmonth"])
        {
            
            [picker_startmonthname removeFromSuperview];
            [_txt_Startmonthname resignFirstResponder];
        }
        else
        {
            [self.txt_Startmonthname setText:[arr_monthname objectAtIndex:0]];
            [picker_startmonthname removeFromSuperview];
            [_txt_Startmonthname resignFirstResponder];
        }
    }
    
    //end month
    else if (sender.tag==3)
    {
        if ([str_checkendmonth isEqualToString:@"endmonth"])
        {
            
            [picker_endmonthname removeFromSuperview];
            [_txt_endmonthname resignFirstResponder];
        }
        else
        {
            [self.txt_endmonthname setText:[arr_monthname objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_endmonthname resignFirstResponder];
        }
        
    }
    //Start Hours
    else if (sender.tag==4)
    {
        if ([str_checkstarthour isEqualToString:@"starthour"])
        {
            
            [picker_endmonthname removeFromSuperview];
            [_txt_starthour resignFirstResponder];
        }
        else
        {
            [self.txt_starthour setText:[arr_hours objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_starthour resignFirstResponder];
        }
        
    }
    
    //end Hours
    else if (sender.tag==5)
    {
        if ([str_checkendhour isEqualToString:@"endhour"])
        {
            
            [picker_endmonthname removeFromSuperview];
            [_txt_endhours resignFirstResponder];
        }
        else
        {
            [self.txt_endhours setText:[arr_hours objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_endhours resignFirstResponder];
        }
        
    }
    
    //startDays
    else if (sender.tag==6)
    {
        if ([str_checkstartdays isEqualToString:@"startdays"])
        {
            [picker_endmonthname removeFromSuperview];
            [_txt_Startdays resignFirstResponder];
        }
        else
        {
            [self.txt_Startdays setText:[arr_days objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_Startdays resignFirstResponder];
        }
    }
    
    //EndDays
    else if (sender.tag==7)
    {
        if ([str_checkenddays isEqualToString:@"enddays"])
        {
            
            [picker_endmonthname removeFromSuperview];
            [_txt_enddays resignFirstResponder];
        }
        else
        {
            [self.txt_enddays setText:[arr_days objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_enddays resignFirstResponder];
        }
    }
    
    //startampm
    else if (sender.tag==8)
    {
        if ([str_checkstartampm isEqualToString:@"startampm"])
        {
            
            [picker_endmonthname removeFromSuperview];
            [_txt_startampm resignFirstResponder];
        }
        else
        {
            [self.txt_startampm setText:[arrampm objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_startampm resignFirstResponder];
        }
    }
    
    //endampm
    else if (sender.tag==9)
    {
        if ([str_checkendampm isEqualToString:@"endampm"])
        {
            
            [picker_endmonthname removeFromSuperview];
            [_txt_endampm resignFirstResponder];
        }
        else
        {
            [self.txt_endampm setText:[arrampm objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_endampm resignFirstResponder];
        }
        
    }
    
    //Startyear
    else if (sender.tag==10)
    {
        if ([str_checkstartyear isEqualToString:@"startyear"])
        {
            
            [picker_startmonthname removeFromSuperview];
            [_txt_Startyear resignFirstResponder];
        }
        else
        {
            [self.txt_Startyear setText:[arr_year objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_Startyear resignFirstResponder];
        }
    }
    
    //EndYear
    else if (sender.tag==11)
    {
        if ([str_checkendyear isEqualToString:@"endyear"])
        {
            
            [picker_startmonthname removeFromSuperview];
            [_txt_endyear resignFirstResponder];
        }
        else
        {
            [self.txt_endyear setText:[arr_year objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_endyear resignFirstResponder];
        }
    }
    
    //startMinute
    else if (sender.tag==12)
    {
        if ([str_checkstartminute isEqualToString:@"startminute"])
        {
            
            [picker_startmonthname removeFromSuperview];
            [_txt_startminute resignFirstResponder];
        }
        else
        {
            [self.txt_startminute setText:[arr_minute objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_startminute resignFirstResponder];
        }
    }
    
    //EndMinute
    else if (sender.tag==13)
    {
        if ([str_checkendminute isEqualToString:@"endminute"])
        {
            
            [picker_startmonthname removeFromSuperview];
            [_txt_endminute resignFirstResponder];
        }
        else
        {
            [self.txt_endminute setText:[arr_minute objectAtIndex:0]];
            [picker_endmonthname removeFromSuperview];
            [_txt_endminute resignFirstResponder];
        }
    }
    
}
-(void)onClickingCancelBtn:(UIButton*)sender
{
    if (sender.tag==1)
    {
        [myPickerView removeFromSuperview];
        [_txt_type resignFirstResponder];
    }
    else if (sender.tag==2)
    {
        [picker_startmonthname removeFromSuperview];
        [_txt_Startmonthname resignFirstResponder];
    }
    else if (sender.tag==3)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_endmonthname resignFirstResponder];
    }
    else if (sender.tag==4)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_starthour resignFirstResponder];
    }
    else if (sender.tag==5)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_endhours resignFirstResponder];
    }
    else if (sender.tag==6)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_Startdays resignFirstResponder];
    }
    else if (sender.tag==7)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_enddays resignFirstResponder];
    }
    else if (sender.tag==8)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_startampm resignFirstResponder];
    }
    else if (sender.tag==9)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_endampm resignFirstResponder];
    }
    else if (sender.tag==10)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_Startyear resignFirstResponder];
    }
    else if (sender.tag==11)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_endyear resignFirstResponder];
    }
    else if (sender.tag==12)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_startminute resignFirstResponder];
    }
    else if (sender.tag==13)
    {
        [picker_endmonthname removeFromSuperview];
        [_txt_endminute resignFirstResponder];
    }
}

#pragma mark- Picker View Delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==1)
    {
        return  [arr_name count];
    }
    else if (pickerView.tag==3)
    {
        return  [arr_monthname count];
    }
    else if (pickerView.tag==4)
    {
        return  [arr_hours count];
    }
    else if (pickerView.tag==5)
    {
        return  [arr_hours count];
    }
    else if (pickerView.tag==6)
    {
        return  [arr_days count];
    }
    else if (pickerView.tag==7)
    {
        return  [arr_days count];
    }
    else if (pickerView.tag==8)
    {
        return  [arrampm count];
    }
    else if (pickerView.tag==9)
    {
        return  [arrampm count];
    }
    else if (pickerView.tag==10)
    {
        return  [arr_year count];
    }
    else if (pickerView.tag==11)
    {
        return  [arr_year count];
    }
    else if (pickerView.tag==12)
    {
        return  [arr_minute count];
    }
    else if (pickerView.tag==13)
    {
        return  [arr_minute count];
    }
    else
    {
        return  [arr_monthname count];
    }
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag==1)
    {
        str_checktype = @"paintype";
        [self.txt_type setText:[arr_name objectAtIndex:row]];
        str_typeid = [arr_typeid objectAtIndex:row];
    }
    else if (pickerView.tag==3)
    {
        str_checkendmonth = @"endmonth";
        [self.txt_endmonthname setText:[arr_monthname objectAtIndex:row]];
    }
    else if (pickerView.tag==4)
    {
        str_checkstarthour = @"starthour";
        [self.txt_starthour setText:[arr_hours objectAtIndex:row]];
    }
    else if (pickerView.tag==5)
    {
        str_checkendhour = @"endhour";
        [self.txt_endhours setText:[arr_hours objectAtIndex:row]];
    }
    else if (pickerView.tag==6)
    {
        str_checkstartdays = @"startdays";
        [self.txt_Startdays setText:[arr_days objectAtIndex:row]];
    }
    else if (pickerView.tag==7)
    {
        str_checkenddays = @"enddays";
        [self.txt_enddays setText:[arr_days objectAtIndex:row]];
    }
    else if (pickerView.tag==8)
    {
        str_checkstartampm = @"startampm";
        [self.txt_startampm setText:[arrampm objectAtIndex:row]];
    }
    else if (pickerView.tag==9)
    {
        str_checkendampm = @"endampm";
        [self.txt_endampm setText:[arrampm objectAtIndex:row]];
    }
    else if (pickerView.tag==10)
    {
        str_checkstartyear = @"startyear";
        [self.txt_Startyear setText:[arr_year objectAtIndex:row]];
    }
    else if (pickerView.tag==11)
    {
        str_checkendyear = @"endyear";
        [self.txt_endyear setText:[arr_year objectAtIndex:row]];
    }
    else if (pickerView.tag==12)
    {
        str_checkstartminute = @"startminute";
        [self.txt_startminute setText:[arr_minute objectAtIndex:row]];
    }
    else if (pickerView.tag==13)
    {
        str_checkendminute = @"endminute";
        [self.txt_endminute setText:[arr_minute objectAtIndex:row]];
    }
    
    else
    {
        str_checkstartmonth = @"startmonth";
        [self.txt_Startmonthname setText:[arr_monthname objectAtIndex:row]];
    }
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag==1)
    {
        return [arr_name objectAtIndex:row];
    }
    else if (pickerView.tag==3)
    {
        return [arr_monthname objectAtIndex:row];
    }
    else if (pickerView.tag==4)
    {
        return [arr_hours objectAtIndex:row];
    }
    else if (pickerView.tag==5)
    {
        return [arr_hours objectAtIndex:row];
    }
    else if (pickerView.tag==6)
    {
        return [arr_days objectAtIndex:row];
    }
    else if (pickerView.tag==7)
    {
        return [arr_days objectAtIndex:row];
    }
    else if (pickerView.tag==8)
    {
        return [arrampm objectAtIndex:row];
    }
    else if (pickerView.tag==9)
    {
        return [arrampm objectAtIndex:row];
    }
    else if (pickerView.tag==10)
    {
        return [arr_year objectAtIndex:row];
    }
    else if (pickerView.tag==11)
    {
        return [arr_year objectAtIndex:row];
    }
    else if (pickerView.tag==12)
    {
        return [arr_minute objectAtIndex:row];
    }
    else if (pickerView.tag==13)
    {
        return [arr_minute objectAtIndex:row];
    }
    else
    {
        return [arr_monthname objectAtIndex:row];
    }
    
}
#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)webservicecalled
{
    //[SVProgressHUD show];
    str_mtdtype = @"type";
    [self.webobj GETAPI:nil methodname:@"v1/typeoptions"];
}
-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    if ([str_mtdtype isEqualToString:@"type"])
    {
        [self.webobj postapi:nil methodname:@"v1/myprofessional"];
        NSArray * arr_data = [dict objectForKey:@"data"];
        arr_name = [arr_data valueForKeyPath:@"name"];
        arr_typeid = [arr_data valueForKeyPath:@"id"];
        str_mtdtype = @"";
    }
    else if ([str_mtdtype isEqualToString:@"save"])
    {
        NSString * status = [dict valueForKeyPath:@"message"];
        if ([status isEqualToString:@"Comment added."])
        {
            [self.navigationController popViewControllerAnimated:YES];
            [self.webobj showAlertView:nil msg:@"Comment saved!" atag:0 cncle:@"OK"  othr:nil delegate:nil];
            
        }
        else
        {
            [self.webobj showAlertView:nil msg:@"Failed!" atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
    }
    else
    {
        arr_ptdata = [dict objectForKey:@"data"];
        ptnamearr = [arr_ptdata valueForKeyPath:@"name"];
        ptidarr = [arr_ptdata valueForKeyPath:@"id"];
        
        
        if ([_str_classname isEqualToString:@"edit"])
        {
            NSMutableArray *chatonly_arr = [NSMutableArray new];
            
            for (NSDictionary *dict1 in arr_ptdata)
            {
                NSString *status = [NSString stringWithFormat:@"%@",dict1[@"id"]];
                
                
                if ([status isEqualToString:strptid])
                {
                    if (![chatonly_arr containsObject:dict1])
                    {
                        [chatonly_arr addObject:dict1];
                    }
                }
            }
            
            NSLog(@"chatonly===========%@",chatonly_arr);
            
            NSArray * ptnaarr = [chatonly_arr objectAtIndex:0];
            _txt_professional.text = [ptnaarr valueForKey:@"name"];
        }
    }
}

#pragma mark -
#pragma mark:TextField Delegate Method
-(void)textFieldDidChange:(UITextField *)textField
{
    if (textField==_txt_professional)
    {
        searchTextString=textField.text;
        [self updateSearchArray:searchTextString];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL) validateAlphabets: (NSString *)alpha
{
    NSString *abnRegex = @"[A-Za-z]+"; // check for one or more occurrence of string you can also use * instead + for ignoring null value
    NSPredicate *abnTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", abnRegex];
    BOOL isValid = [abnTest evaluateWithObject:alpha];
    return isValid;
}

#pragma mark - IBAction

-(IBAction)savebuttonclicked:(id)sender
{
    if ([_txt_type.text isEqualToString:@""])
    {
        [self.webobj showAlertView:@"Alert!" msg:@"Please Select Comment Type!" atag:0 cncle:@"Thanks!" othr:nil delegate:nil];
    }
    else if ([_txt_professional.text isEqualToString:@""])
    {
        [self.webobj showAlertView:@"Alert!" msg:@"Please Select Professional!" atag:0 cncle:@"Thanks!" othr:nil delegate:nil];
    }
    else if ([_txt_title.text isEqualToString:@""])
    {
        [self.webobj showAlertView:@"Alert!" msg:@"Please Enter Comment Title!" atag:0 cncle:@"Thanks!" othr:nil delegate:nil];
    }
    else if ([_txt_details.text isEqualToString:@""])
    {
        [self.webobj showAlertView:@"Alert!" msg:@"Please Enter Comment Details!" atag:0 cncle:@"Thanks!" othr:nil delegate:nil];
    }
    else
    {
        [SVProgressHUD show];
        str_mtdtype = @"save";
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM"];
        NSDate *aDate = [formatter dateFromString:_txt_Startmonthname.text];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:aDate];
        NSLog(@"Month: %li", (long)[components month]);
        NSString * str_startmonth = [NSString stringWithFormat:@"%ld",(long)[components month]];
        
        NSDateFormatter* formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"MM"];
        NSDate *aDate2 = [formatter dateFromString:_txt_endmonthname.text];
        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:aDate2];
        NSLog(@"Month: %li", (long)[components2 month]);
        NSString * str_endmonthname = [NSString stringWithFormat:@"%ld",(long)[components2 month]];
        
        NSString * str_endtimedate  = [NSString stringWithFormat:@"%@:%@",_txt_endhours.text,_txt_endminute.text];
        NSString * str_starttimedate  = [NSString stringWithFormat:@"%@:%@",_txt_starthour.text,_txt_startminute.text];
        
        NSDateFormatter *startdateFormatter = [[NSDateFormatter alloc] init] ;
        [startdateFormatter setDateFormat:@"hh:mm"];// here give the format which you get in TimeStart
        NSDate *startdate = [startdateFormatter dateFromString: str_starttimedate];
        startdateFormatter = [[NSDateFormatter alloc] init] ;
        [startdateFormatter setDateFormat:@"HH:mm"];
        NSString *startconvertedString = [startdateFormatter stringFromDate:startdate];
        
        NSDateFormatter *enddateFormatter = [[NSDateFormatter alloc] init] ;
        [enddateFormatter setDateFormat:@"hh:mm"];// here give the format which you get in TimeStart
        NSDate *enddate = [enddateFormatter dateFromString: str_endtimedate];
        enddateFormatter = [[NSDateFormatter alloc] init] ;
        [enddateFormatter setDateFormat:@"HH:mm"];
        NSString *endconvertedString = [enddateFormatter stringFromDate:enddate];
        
        NSLog(@"start Converted String : %@",startconvertedString);
        NSLog(@"end Converted String : %@",endconvertedString);
        
        
        if ([startconvertedString length]==0)
        {
            [SVProgressHUD dismiss];
            [self.webobj showAlertView:nil msg:@"Please Select Start Hours" atag:0 cncle:@"OK"  othr:nil delegate:nil];
        }
        else if ([endconvertedString length]==0)
        {
            [SVProgressHUD dismiss];
            [self.webobj showAlertView:nil msg:@" Please Select End Hours" atag:0 cncle:@"OK"  othr:nil delegate:nil];
        }
        else
        {
            NSString * str_strtdate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@",_txt_Startyear.text,str_startmonth,_txt_Startdays.text,startconvertedString,@"00"];
            
            NSString * str_enddate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@",_txt_endyear.text,str_endmonthname,_txt_enddays.text,endconvertedString,@"00"];
            
            NSLog(@"startdate==%@",str_strtdate);
            NSLog(@"enddate==%@",str_enddate);
            
            
            Request_data  = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_typeid,@"type_id",strptid,@"pt_id",_txt_title.text,@"title",_txt_details.text,@"comment",str_strtdate,@"start_date",str_enddate,@"end_date", nil];
            
            [self.webobj postapi:Request_data methodname:@"v1/addcalendarcomment"];
        }
    }
}


-(IBAction)viewcalenderclicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)previousbuttonclicked:(id)sender
{
    PreviousCommentController * ADDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PreviousCommentController"];
    [self.navigationController pushViewController:ADDVC animated:NO];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [searchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"addcomment";
    callendercell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[callendercell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    if(isFilter)
    {
        cell.lbl_Commenttitle.text=[searchArray objectAtIndex:indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _txt_professional.text = [searchArray objectAtIndex:indexPath.row];
    strptid  = [arridpt objectAtIndex:indexPath.row];
    [self.tbl_view setHidden:YES];
}
@end

//
//  PreviousCommentController.m
//  PhysioMonkey
//
//  Created by prateek on 03/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "PreviousCommentController.h"

@interface PreviousCommentController ()
@end
@implementation PreviousCommentController


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tbl_view.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.navigationItem.hidesBackButton=YES;
    
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    
}


#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)webservicecalled
{
    [self.webobj GETAPI:nil methodname:@"v1/previouscomments"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    NSLog(@"responsejsondata=%@",dict);
    arr_main = [dict valueForKey:@"data"];
    arr_type = [arr_main valueForKeyPath:@"type"];
    arr_start = [arr_main valueForKeyPath:@"start"];
    arr_end = [arr_main valueForKeyPath:@"end"];
    arr_title = [arr_main valueForKeyPath:@"title"];
    arr_id = [arr_main valueForKeyPath:@"id"];
    arr_ptid = [arr_main valueForKeyPath:@"pt_id"];
    arr_typeid = [arr_main valueForKeyPath:@"type_id"];
    [self.tbl_view reloadData];
    
}
#pragma mark - IBAction

-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)viewcalenderclicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)editbuttonclicked:(UIButton*)sender
{
    AddCommentController * ADDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCommentController"];
    ADDVC.dic_comment = arr_main[sender.tag];
    ADDVC.str_classname = @"edit";
    [self.navigationController pushViewController:ADDVC animated:NO];
    
}
-(IBAction)newcommentbuttonclicked:(id)sender
{
    AddCommentController * ADDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCommentController"];
    
    [self.navigationController pushViewController:ADDVC animated:NO];
}
#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_id count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"callendercell";
    callendercell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[callendercell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    cell.lbl_end.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    cell.lbl_end.text = [arr_end objectAtIndex:indexPath.row];
    cell.lbl_start.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    cell.lbl_start.text = [arr_start objectAtIndex:indexPath.row];
    cell.lbl_type.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    cell.lbl_type.text = [arr_type objectAtIndex:indexPath.row];
    cell.lbl_title.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    cell.lbl_title.text = [arr_title objectAtIndex:indexPath.row];
    
    cell.btn_action.tag = indexPath.row;
    [cell.btn_action addTarget:self action:@selector(editbuttonclicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *CellIdentifier = @"callenderheader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    return headerView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

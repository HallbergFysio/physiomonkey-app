//
//  CCBarView.h
//  Covantech chap chap taxi app
//
//  Created by Manish on 06/06/16.
//  Copyright © 2016 vishnu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCBarView : UIView

@property (nonatomic , strong) IBOutlet UIButton *btnMenu;
- (IBAction)btnMenu_didPressed:(id)sender;

@end

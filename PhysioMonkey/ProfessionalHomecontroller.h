//
//  ProfessionalHomecontroller.h
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfessionalHomecontroller : UIViewController <UIGestureRecognizerDelegate>

//IBOUTLET
@property (nonatomic,retain)IBOutlet UIButton *  btn_Mylibrary;
@property (nonatomic,retain)IBOutlet UIButton *  btn_Subscription;
@property (nonatomic,retain)WebService * webobj;

//IBACTION
-(IBAction)MyProfilebuttonclicked:(id)sender;
-(IBAction)Logoutbuttonclicked:(id)sender;
-(IBAction)VerifyNewClientButtonclicked:(id)sender;
-(IBAction)RecentClientButtonclicked:(id)sender;
-(IBAction)ClientButtonclicked:(id)sender;
-(IBAction)SubsriptionButtonClicked:(id)sender;
-(IBAction)MyLibraryButtonClicked:(id)sender;
-(IBAction)offerButtonClicked:(id)sender;
@end

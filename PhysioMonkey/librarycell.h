//
//  librarycell.h
//  PhysioMonkey
//
//  Created by prateek on 17/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface librarycell : UITableViewCell


//IBOUTLET MYLIBRARY
@property (nonatomic,retain) IBOutlet UIImageView * img_library;
@property (nonatomic,retain) IBOutlet   UILabel * lbl_mylibrary;


//IBOUTLET PREVIOUSPROGRAM
@property (nonatomic,retain) IBOutlet UIImageView * img_PreviousProgram;
@property (nonatomic,retain) IBOutlet UILabel * lbl_PreviousProgram;

//IBOUTLET EXISTINGPROGRAM
@property (nonatomic,retain) IBOutlet UIImageView * img_ExistingProgram;
@property (nonatomic,retain) IBOutlet UILabel * lbl_ExistingProgram;

//IBOUTLET CREATE PROGRAM
@property (nonatomic,retain) IBOutlet UIImageView * img_createProgram;
@property (nonatomic,retain) IBOutlet UITextView * lbl_createProgram;
@property (nonatomic,retain) IBOutlet UIButton * btn_camera;
@property (nonatomic,retain) IBOutlet UIButton * btn_video;
@property (nonatomic,retain) IBOutlet UIButton * btn_imageclick;
@property (nonatomic,retain) IBOutlet UIButton * btn_deleteclick;

//IBOUTLET
@property (nonatomic,retain)IBOutlet UIButton * btn_edit;
@property (nonatomic,retain)IBOutlet UIButton * btn_delete;
@property (nonatomic,retain)IBOutlet UIButton * btn_assign;
@property (nonatomic,retain)IBOutlet UIButton * btn_pdf;
@property (nonatomic,retain)IBOutlet UILabel * lbl_date;
@property (nonatomic,retain)IBOutlet UILabel * lbl_detail;
@property (nonatomic,retain)IBOutlet UILabel * lbl_assignlbl;


//IBOUTLET editprevious
@property (nonatomic,retain)IBOutlet UIButton * btn_editpreviousedit;
@property (nonatomic,retain)IBOutlet UIButton * btn_editpreviouspdf;
@property (nonatomic,retain)IBOutlet UIButton * btn_editpreviousdelete;
@property (nonatomic,retain)IBOutlet UILabel * lbl_editprevioustitle;
@property (nonatomic,retain)IBOutlet UILabel * lbl_editpreviousdate;
@end

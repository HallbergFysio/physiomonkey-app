//
//  RecentProgramController.m
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "RecentProgramController.h"

@interface RecentProgramController ()

@end

@implementation RecentProgramController


#pragma mark -
#pragma mark:ViewLoadMethod
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.CommonMethodObj =[[CommonMethodClass alloc]init];
    
    self.tbl_RecentPrograms.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    
    
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [SVProgressHUD show];
    _search_bar.text=@"";
    [self webservicecalled];
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod

-(void)webservicecalled
{
    [self.webobj postapi:nil methodname:@"v1/recentprograms"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    NSString * str_msg = [dict valueForKeyPath:@"message"];
    
    if ([str_msg isEqualToString:@"Record not found"])
    {
        [self.webobj showAlertView:@"Alert!" msg:@"Record not found" atag:0 cncle:@"OK" othr:nil delegate:nil];
        [self.tbl_RecentPrograms setHidden:YES];
    }
    else
    {
        main_arr = [dict valueForKeyPath:@"data"];
        arr_title = [main_arr valueForKeyPath:@"title"];
        arr_createddate = [main_arr valueForKeyPath:@"created"];
        arr_cat_name= [main_arr valueForKeyPath:@"cat_name"];
        arr_description= [main_arr valueForKeyPath:@"description"];
        arr_detail = [main_arr valueForKeyPath:@"details"];
        arr_id = [main_arr valueForKeyPath:@"id"];
        
        arr_ptdetail = [dict valueForKeyPath:@"pt_detail"];
        dictclient = [[NSMutableDictionary alloc]init];
        [dictclient setObject:[main_arr valueForKeyPath:@"pt_id"] forKey:@"id"];
        [dictclient setObject:[main_arr valueForKeyPath:@"pt_name"] forKey:@"name"];
        [self.tbl_RecentPrograms reloadData];
    }
}

#pragma mark - IBAction

-(void)openprogramclicked:(UIButton *)sender
{
    NSArray * d = main_arr[sender.tag];
    programcontroller * programVC = [self.storyboard instantiateViewControllerWithIdentifier:@"programcontroller"];
    programVC.dictclient = d;
    programVC.classname = @"recentprogram";
    
    [self.navigationController pushViewController:programVC animated:NO];
}

-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"clientrecenthome" sender:self];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_id count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"recentcell";
    RecentProgramCell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[RecentProgramCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    
    cell.lbl_title.text = [arr_title objectAtIndex:indexPath.row];
    cell.lbl_description.text = [arr_description objectAtIndex:indexPath.row];
    cell.lbl_startdate.text = [arr_createddate objectAtIndex:indexPath.row];
    cell.lbl_cat_name.text = [arr_cat_name objectAtIndex:indexPath.row];
    
    if (IS_IPHONE_4_OR_LESS)
    {
        cell.scroll_view.contentSize = CGSizeMake(1000, 105);
    }
    if (IS_IPHONE_5)
    {
        cell.scroll_view.contentSize = CGSizeMake(1000, 105);
    }
    
    if (IS_IPHONE_6)
    {
        cell.scroll_view.contentSize = CGSizeMake(1000, 105);
    }
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in cell.scroll_view.subviews)
    {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    cell.scroll_view.contentSize = contentRect.size;
    
    NSArray * arr_dict = [arr_detail objectAtIndex:indexPath.row];
    NSLog(@"arr_dict==%@",arr_dict);
    NSArray * filearray = [arr_dict valueForKeyPath:@"file"];
    NSLog(@"filearray==%@",filearray);
    
    for (int i = 0; i < [filearray count]; i++)
    {
        CGFloat xorigin = i * 148;
        UIImageView * imag_view = [[UIImageView alloc]init];
        [imag_view setFrame:CGRectMake(xorigin,8,140,89)];
        [imag_view sd_setImageWithURL:[NSURL URLWithString:[filearray objectAtIndex:i]]
                     placeholderImage:[UIImage imageNamed:@"video_dafault_img.jpg"]];
        [cell.scroll_view addSubview:imag_view];
    }
    
    cell.lbl_description.editable = NO;
    cell.lbl_description.dataDetectorTypes = UIDataDetectorTypeLink;
    cell.lbl_description.delegate = self;
    
    cell.btn_OpenProgram.tag = indexPath.row;
    [cell.btn_OpenProgram addTarget:self action:@selector(openprogramclicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.tbl_RecentPrograms deselectRowAtIndexPath:indexPath animated:NO];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

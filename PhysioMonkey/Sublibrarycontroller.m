//
//  Sublibrarycontroller.m
//  PhysioMonkey
//
//  Created by prateek on 09/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "Sublibrarycontroller.h"

@interface Sublibrarycontroller ()
@end
@implementation Sublibrarycontroller


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    [SVProgressHUD show];
     [_lbl_line setHidden:YES];
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    [self webservicecalled];
    self.tbl_view.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.webobj TextfieldBorderColor:_txt_clientname];
    
    [self.webobj buttonBorderclearColor:self.btn_close];
    [self.webobj buttonBorderclearColor:self.btn_assignprogram];
    
    [self.assignview setHidden:YES];
    [self.tbl_view setHidden:NO];
    [self.tblassign_view setHidden:YES];
    self.navigationItem.hidesBackButton=YES;
    [self.txt_clientname addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
}

#pragma mark -
#pragma mark: Navigation Baar Menu
-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}
-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}
#pragma mark -
#pragma mark:HTTPWebserviceMethod

-(void)webservicecalledSavePdf
{
    //[SVProgressHUD show];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:pdf_id,@"program_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/programaspdf"];
}
-(void)DeleteWebServiceCalled
{
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_programidd,@"program_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/deleteprogram"];
}
-(void)webservicecalled
{
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_str_cat_idd,@"cat_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/useexistingprograms"];
}
-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    if ([common_str isEqualToString:@"delete"])
    {
        NSString * delete_status = [dict valueForKeyPath:@"message"];
        if ([delete_status isEqualToString:@"Program deleted successfully"])
        {
            common_str= @"";
            [self.webobj showAlertView:@"Program Delete Successfully!" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
            [self webservicecalled];
        }
        else
        {
            [self.webobj showAlertView:@"Program Delete Failed!" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
            
        }
    }
    
    else if ([common_str isEqualToString:@"assign"])
    {
        common_str = @"";
        arr_mainassign = [dict valueForKeyPath:@"data"];
    }
    else if ([common_str isEqualToString:@"assignprogram"])
    {
        NSString * status = [dict valueForKeyPath:@"message"];
        if ([status isEqualToString:@"Program assigned successfully."])
        {
            [self.webobj showAlertView:@"Program Assign Successfully" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        else if ([status isEqualToString:@"Program already assign."])
        {
            [self.webobj showAlertView:@"Program already assign." msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        else
        {
            [self.webobj showAlertView:@"Program Assign Failed!" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        common_str = @"";
    }
    else if ([common_str isEqualToString:@"pdfsave"])
    {
        
        NSDate *currentTime = [NSDate date];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@" yyyy-MM-d H:m:s"];
        NSString *resultString = [dateFormatter stringFromDate: currentTime];
        NSLog(@"resultString=%@",resultString);

        //NSString * data_status = [dict valueForKeyPath:@"message"];
        NSString *data = [dict valueForKeyPath:@"data"];
        NSString *pdf = [data valueForKeyPath:@"program_pdf"];
        
        NSData *dataPdf = [NSData dataWithContentsOfURL:[NSURL URLWithString:pdf]];
        //Get path directory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //Create PDF_Documents directory
        documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Physiomonkey"];
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        
        NSString *filePath = [NSString stringWithFormat:@"%@/%@ (%@)", documentsDirectory, @"Physiomonkey program Pdf" ,  resultString];
        
        [dataPdf writeToFile:filePath atomically:YES];
       
        NSLog(@"filePath===%@",filePath);
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                          
                           [self.webobj showAlertView:@"Program" msg:@"Pdf Saved Successfull!" atag:0 cncle:@"OK" othr:nil delegate:nil];
                       });
        common_str= @"";
        

    }
        
    else
    {
        NSString * data_status = [dict valueForKeyPath:@"message"];
        if ([data_status isEqualToString:@"Record not found."])
        {
            [_lbl_line setHidden:YES];
             [self.webobj showAlertView:nil msg:@"No programs has been created here yet" atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        else
        {
            [_lbl_line setHidden:NO];
            _lbl_programtitle.text = _str_cat_name;
            arr_main = [dict valueForKeyPath:@"data"];
            arr_detials = [arr_main valueForKeyPath:@"details"];
            arr_title = [arr_main valueForKeyPath:@"title"];
            arr_id = [arr_main valueForKeyPath:@"id"];
            arr_createdate = [arr_main valueForKeyPath:@"created"];
            [self.tbl_view reloadData];
            
        }
    }
    
    
}


#pragma mark -
#pragma mark: Textfield Delegate Method
-(void)textFieldDidChange:(UITextField *)textField
{
    if (textField==_txt_clientname)
    {
        [self.btn_close setHidden:YES];
        [self.btn_assignprogram setHidden:YES];
        searchTextString=textField.text;
        [self updateSearchArray:searchTextString];
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

#pragma mark -
#pragma mark:  UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.tbl_view)
    {
        return [arr_id count];
    }
    else
    {
        return [searchArray count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"library";
    librarycell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[librarycell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    if (tableView==self.tbl_view)
    {
        cell.lbl_date.text = [arr_createdate objectAtIndex:indexPath.row];
        cell.lbl_detail.text = [arr_title objectAtIndex:indexPath.row];
        
        [self.webobj buttonBorderColor:cell.btn_edit];
        [self.webobj buttonBorderColor:cell.btn_assign];
        [self.webobj buttonBorderColor:cell.btn_delete];
        [self.webobj buttonBorderColor:cell.btn_pdf];
        
        cell.btn_delete.tag=indexPath.row;
        cell.btn_assign.tag=indexPath.row;
        cell.btn_edit.tag=indexPath.row;
        cell.btn_pdf.tag=indexPath.row;
        
        [cell.btn_edit addTarget:self action:@selector(editbuttonclicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_delete addTarget:self action:@selector(deletebuttonclicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_assign addTarget:self action:@selector(assignbuttonclicked:) forControlEvents:UIControlEventTouchUpInside];
         [cell.btn_pdf addTarget:self action:@selector(pdfbuttonclicked:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else
    {
        cell.lbl_assignlbl.text =[searchArray objectAtIndex:indexPath.row];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==self.tblassign_view)
    {
        [self.tbl_view setAllowsSelection:YES];
        _txt_clientname.text = [searchArray objectAtIndex:indexPath.row];
        strptid  = [arridpt objectAtIndex:indexPath.row];
        NSLog(@"strptid==%@",strptid);
        [self.tblassign_view setHidden:YES];
        [self.btn_assignprogram setHidden:NO];
        [self.btn_close setHidden:NO];
        
    }
    else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self.tbl_view setAllowsSelection:NO];
    }
   
}
#pragma mark -
#pragma mark: IBACTION

-(IBAction)pdfbuttonclicked:(UIButton*)sender
{
    common_str  = @"pdfsave";
    NSDictionary * edit_dic = arr_main[sender.tag];
    pdf_id = [edit_dic valueForKey:@"id"];
    [self webservicecalledSavePdf];
    
}
-(IBAction)assignclosebuttonclick:(id)sender
{
    [self.assignview setHidden:YES];
    [self.tbl_view setHidden:NO];
}
-(IBAction)assignprogrambuttonclick:(id)sender
{
    [SVProgressHUD show];
    common_str = @"assignprogram";
    Request_data  = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_prograamidd,@"program_id",strptid,@"client_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/assignprogram"];
}
-(IBAction)editbuttonclicked:(UIButton*)sender
{
    CreateProgramController * exiobj = [self.storyboard instantiateViewControllerWithIdentifier:@"CREATE"];
    NSDictionary * edit_dic = arr_main[sender.tag];
    exiobj.str_clasname = @"sublibrary";
    exiobj.str_subclasname = @"newlibrary";
    exiobj.dic_sublib = edit_dic;
    [self.navigationController pushViewController:exiobj animated:NO];
}
-(IBAction)deletebuttonclicked:(UIButton*)sender
{
    [SVProgressHUD show];
    common_str = @"delete";
    NSDictionary * delete_dic = arr_main[sender.tag];
    str_programidd = [delete_dic valueForKeyPath:@"id"];
    NSLog(@"%@",str_programidd);
    [self DeleteWebServiceCalled];
}
-(IBAction)assignbuttonclicked:(UIButton*)sender
{
    NSDictionary * delete_dic = arr_main[sender.tag];
    str_prograamidd = [delete_dic valueForKeyPath:@"id"];
    common_str = @"assign";
    [self.webobj postapi:nil methodname:@"v1/myclients"];
    [self.assignview setHidden:NO];
    [self.tbl_view setHidden:YES];
}
-(void)updateSearchArray:(NSString *)searchText
{
    if(searchText.length==0)
    {
        isFilter=NO;
        [self.btn_assignprogram setHidden:NO];
        [self.btn_close setHidden:NO];
        [self.tblassign_view setHidden:YES];
    }
    else
    {
        
        isFilter=YES;
        searchArray=[[NSMutableArray alloc]init];
        NSMutableArray *Privateonly_arr = [NSMutableArray new];
        
        for (NSDictionary *dict1 in arr_mainassign)
        {
            NSString *status = [NSString stringWithFormat:@"%@",dict1[@"name"]];
            NSString *str_dob = [NSString stringWithFormat:@"%@",dict1[@"dob"]];
            NSString * final_str = [NSString stringWithFormat:@"%@  (%@)",status,str_dob];
            NSRange stringRange=[status rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(stringRange.location !=NSNotFound)
            {
                [self.tblassign_view setHidden:NO];
                [searchArray addObject:final_str];
                [Privateonly_arr addObject:dict1];
                arridpt = [Privateonly_arr valueForKeyPath:@"id"];
                
            }
            [self.tblassign_view reloadData];
        }
        
        
    }
}

@end

//
//  LoginViewController.m
//  PhysioMonkey
//
//  Created by prateek on 02/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "LoginViewController.h"
#import "SignupController.h"

@interface LoginViewController ()
@end

@implementation LoginViewController
@synthesize email_txt,password_txt;


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
    obj = [NSUserDefaults standardUserDefaults];
}

#pragma mark -
#pragma mark: UIButton Action

-(IBAction)Backbutton_clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)Loginbutton_clicked:(id)sender
{
    check_status = @"login";
    if ([email_txt.text isEqual:@""])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter E-Mail id!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if (![self validateEmailWithString:email_txt.text])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter Valid E-Mail id!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([password_txt.text isEqual:@""])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter Password!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else
    {
        [SVProgressHUD show];
        Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:email_txt.text,@"email",password_txt.text,@"password", nil];
        [self.webObj postDictionary:Request_data methodname:@"v1/userlogin" view:self.view];
    }
    
}

-(IBAction)Signupbutton_clicked:(id)sender
{
    SignupController * professional_obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SignupVC"];
    [self.navigationController pushViewController:professional_obj animated:NO];
}

-(IBAction)forgotpasswordclicked:(id)sender
{
    forgotpasswordcontroller * forgot_obj = [self.storyboard instantiateViewControllerWithIdentifier:@"forgotpasswordcontroller"];
    [self presentViewController:forgot_obj animated:NO completion:nil];
}
#pragma mark -
#pragma mark AlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex==1)
        {
            textfield=[alertView textFieldAtIndex:0];
            
            if ([self validateEmailWithString:textfield.text])
            {
                [SVProgressHUD show];
                Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:textfield.text,@"email", nil];
                [self.webObj postDictionary:Request_data methodname:@"v1/forgotpassword" view:self.view];
            }
            else if ([textfield.text isEqualToString:@""])
            {
                [self.webObj showAlertView:@"Forgot Password!" msg:@"Please enter your email address!" atag:1 cncle:@"OK" othr:@"YES" delegate:self];
            }
            else
            {
                [self.webObj showAlertView:@"Forgot Password!" msg:@"Please check that your email address is correct and try again!" atag:0 cncle:@"OK" othr:nil delegate:nil];
            }
        }
    }
}
#pragma mark -
#pragma mark:Common Method
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    NSLog(@"dict====%@",dict);
    str_status = [dict valueForKeyPath:@"message"];
    if ([check_status isEqualToString:@"login"])
    {
        NSArray * data = [dict valueForKeyPath:@"data"];
        
        NSString * role_str = [data valueForKeyPath:@"role"];
        NSString * str_id = [data valueForKeyPath:@"id"];
        NSString * str_name = [data valueForKeyPath:@"name"];
        NSString * str_image = [data valueForKeyPath:@"image"];
        NSString * str_email = [data valueForKeyPath:@"email"];
        
        
        
        [obj setObject:str_id forKey:@"LoginUserID"];
        [obj synchronize];
        [obj setObject:str_name forKey:@"LoginUserName"];
        [obj synchronize];
        [obj setObject:str_image forKey:@"LoginUserImage"];
        [obj synchronize];
        [obj setObject:str_email forKey:@"LoginUserEmail"];
        [obj synchronize];
        [obj setObject:password_txt.text forKey:@"LoginUserpassword"];
        [obj synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"login" forKey:@"logedIn"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:role_str forKey:@"role"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([str_status isEqualToString:@"Login successfully"])
        {
            if ([role_str isEqualToString:@"3"])
            {
                [self performSegueWithIdentifier:@"clientlogin" sender:self];
            }
            if ([role_str isEqualToString:@"2"])
            {
                [self performSegueWithIdentifier:@"ptlogin" sender:self];
            }
            
        }
        else if ([str_status isEqualToString:@"Invalid Email or Password"])
        {
            [self.webObj showAlertView:@"LOGIN!" msg:@"Invalid Email or Password!" atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        else
        {
            [self.webObj showAlertView:@"LOGIN!" msg:@"Failed!" atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        
    }
    if ([check_status isEqualToString:@"forgot"])
    {
        if([str_status isEqualToString:@"Email address does not exist"])
        {
            [self.webObj showAlertView:@"Forgot Password!" msg:@"Email address does not exist!" atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        else if ([str_status isEqualToString:@"New Password sent to your registered mail address"])
        {
            [self.webObj showAlertView:@"Forgot Password!" msg:@"New Password sent to your registered mail address!" atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        else
        {
            [self.webObj showAlertView:@"Forgot Password!" msg:@"Failed!" atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        
        
    }
}

#pragma mark -
#pragma mark:UITextfieldDelegateMethod
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end

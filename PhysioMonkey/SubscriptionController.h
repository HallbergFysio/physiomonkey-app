//
//  SubscriptionController.h
//  PhysioMonkey
//
//  Created by prateek on 10/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonMethodClass.h"
#import "WebService.h"
#import "PayPalMobile.h"


@interface SubscriptionController : UIViewController <UIScrollViewDelegate,UIScrollViewAccessibilityDelegate,
UITableViewDelegate,UITableViewDataSource,WebDataDelegate ,PayPalPaymentDelegate ,PayPalFuturePaymentDelegate ,PayPalProfileSharingDelegate>
{
    NSArray * arr_TransactionNo;
    NSArray * arr_Amount;
    NSArray * arr_PaymentType;
    NSArray * arr_ExpiryDate;
    NSArray * arr_PlanType;
    NSArray * arr_PaymentDate;
    NSArray * arr_id;
    NSArray * arr_main;
    NSArray * arr_amount;
    NSArray * arr_description;
    NSArray * arr_duration_days;
    NSArray * arr_name;
    NSArray * arr_planlistid;
    NSArray * arr_historyAmount;
    
    NSString * str_pdf;
    NSString * payment_iddd;
    NSString * receipt_value;
    NSString * pay_subamount;
    NSString * payment_paypalamount;
    NSString * str_transactionid;
    
    NSInteger ind_ex;
    NSInteger plannumber;
    
    NSMutableDictionary * receipt_dict;
    NSMutableDictionary *Request_data;
}

//IBOUTLET
@property(nonatomic, strong, readwrite) IBOutlet UIView *successView;
@property(nonatomic, strong, readwrite) IBOutlet UIView *paymentdetailView;

@property (nonatomic,strong) NSString *environment;
@property (nonatomic,strong) NSString *resultText;

@property (nonatomic,retain)IBOutlet UIButton * btn_AdvanceBuyNow;
@property (nonatomic,retain)IBOutlet UIButton * btn_RegularBuyNow;
@property (nonatomic,retain)IBOutlet UIButton * btn_BasicBuyNow;
@property (nonatomic,retain)IBOutlet UIButton * btn_buynow;
@property (nonatomic,strong)IBOutlet UIButton *btn_dismiss;

@property (nonatomic,retain)IBOutlet UILabel * lbl_amount;
@property (nonatomic,retain)IBOutlet UILabel * lbl_plan;
@property (nonatomic,retain)IBOutlet UILabel * lbl_Action;
@property (nonatomic,retain)IBOutlet UILabel * lbl_paymentdate;
@property (nonatomic,retain)IBOutlet UILabel * lbl_expirationdate;
@property (nonatomic,retain)IBOutlet UILabel * lbl_transactionnumber;
@property (nonatomic,retain)IBOutlet UILabel * lbl_plantype1;
@property (nonatomic,retain)IBOutlet UILabel * lbl_planduration1;
@property (nonatomic,retain)IBOutlet UILabel * lbl_plandiscription1;
@property (nonatomic,retain)IBOutlet UILabel * lbl_planamount1;
@property (nonatomic,retain)IBOutlet UILabel * lbl_plantype2;
@property (nonatomic,retain)IBOutlet UILabel * lbl_planduration2;
@property (nonatomic,retain)IBOutlet UILabel * lbl_plandiscription2;
@property (nonatomic,retain)IBOutlet UILabel * lbl_planamount2;
@property (nonatomic,retain)IBOutlet UILabel * lbl_plantype3;
@property (nonatomic,retain)IBOutlet UILabel * lbl_planduration3;
@property (nonatomic,retain)IBOutlet UILabel * lbl_plandiscription3;
@property (nonatomic,retain)IBOutlet UILabel * lbl_planamount3;

@property (nonatomic,retain)IBOutlet UIScrollView * scroll_view;
@property (nonatomic,retain)IBOutlet UITableView * tbl_subsription;

@property (nonatomic,retain)WebService * webobj;
@property (nonatomic,retain)CommonMethodClass * COMMONOBJ;
@property (nonatomic,strong)PayPalConfiguration *payPalConfig;

//IBACTION
-(IBAction)buynowclicked:(id)sender;
-(IBAction)ReceiptAllclicked:(id)sender;
@end

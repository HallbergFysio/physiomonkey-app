//
//  AddProfessionalCell.h
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProfessionalCell : UITableViewCell

//IBOUTLET
@property (nonatomic,retain)IBOutlet UILabel * lbl_username;
@property (nonatomic,retain)IBOutlet UILabel * lbl_userhospitalname;
@property (nonatomic,retain)IBOutlet UIImageView * img_user;
@property (nonatomic,retain)IBOutlet UIImageView * img_cliniclogo;
@property (nonatomic,retain)IBOutlet UIButton * btn_Requestsent;
@property (nonatomic,retain)IBOutlet UIButton * btn_RequestSend;
@property (nonatomic,retain)IBOutlet UIButton * btn_Cancel;


@end

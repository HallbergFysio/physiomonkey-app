//
//  MyProfileviewcontroller.m
//  PhysioMonkey
//
//  Created by prateek on 06/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "MyProfileviewcontroller.h"

#import "SVProgressHUD.h"
#import "SWRevealViewController.h"
@interface MyProfileviewcontroller ()

@end

@implementation MyProfileviewcontroller
@synthesize imagepick;

#pragma mark -
#pragma mark:ViewLoadMethod
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
    
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    [self webservicecalled];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.webObj imageround1:_img_userimage];
    
    if (IS_IPHONE_4_OR_LESS)
    {
        self.scroll_view.contentSize = CGSizeMake(0,300);
    }
    
    if (IS_IPHONE_5)
    {
        self.scroll_view.contentSize = CGSizeMake(0,300);
    }
    
    if (IS_IPHONE_6)
    {
        _heightConstraint.constant = 100;
//        _scroll_view.contentSize = CGSizeMake(_scroll_view.frame.size.width, 100);
    }
}
#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}

-(void)displaybackbutton
{
    
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    
}

#pragma mark - IBAction
-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"clientprofilehome" sender:self];
}

-(void)dismissKeyboard
{
    [_txt_dob resignFirstResponder];
    [_txt_Name resignFirstResponder];
    [_txt_email resignFirstResponder];
    [_txt_phone resignFirstResponder];
    [_txt_Address resignFirstResponder];
}
-(void)ImageButtonclicked:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet]; // 1
    UIAlertAction *Cancel_click = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       NSLog(@"You pressed button Cancel_click");
                                   }
                                   ];
    UIAlertAction *Takephoto_clicked = [UIAlertAction actionWithTitle:@"Take photo"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                        {
                                            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                                            {
                                                [self.webObj showAlertView:@"Error" msg:@"Device has no camera" atag:0 cncle:@"OK" othr:nil delegate:nil];
                                            }
                                            else
                                            {
                                                imagepick = [[UIImagePickerController alloc] init];
                                                imagepick.delegate = self;
                                                imagepick.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                [self presentViewController:imagepick animated:YES completion:NULL];
                                            }

                                        }];
    
    UIAlertAction *Selectphoto_clicked = [UIAlertAction actionWithTitle:@"Select photo"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                          {
                                              NSLog(@"You pressed button Selectphoto_clicked");
                                              imagepick = [[UIImagePickerController alloc] init];
                                              imagepick.delegate = self;
                                              imagepick.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                              [self presentViewController:imagepick animated:YES completion:NULL];
                                          }];
    
    [alert addAction:Cancel_click];
    [alert addAction:Takephoto_clicked];
    [alert addAction:Selectphoto_clicked];
    
    [self presentViewController:alert animated:NO completion:nil];
}
-(IBAction)editbuttonclicked:(UIButton*)sender
{
    str_common = @"edit";
    
    
    if ([sender.titleLabel.text isEqualToString:@"Save"])
    {
        Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_txt_Name.text,@"name",_txt_dob.text,@"dob",_txt_phone.text,@"phone",_txt_Address.text,@"address", nil];
        
        NSData *imageData = UIImageJPEGRepresentation(self.img_userimage.image,1.0);
        
        [WebService UpdateProfileMultiPartParameters:Request_data strMethod_url:@"v1/profileupdate" imageDataDic:imageData withCompletion:^(NSDictionary *resposnce)
         {
             NSLog(@"%@",resposnce);
             
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSString * str = [resposnce valueForKeyPath:@"message"];
                                if ([str isEqualToString:@"User updated successfully."])
                                {
                                    [self.webObj showAlertView:@"Update Successfull" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
                                    [self webservicecalled];
                                    [_btn_Edit setTitle:@"Edit" forState:UIControlStateNormal];
                                    [self UserInteraction:NO];
                                }
                            else if ([str isEqualToString:@"Required field(s) phone is missing or empty"])
                                {
                                   [self.webObj showAlertView:@"phone number is missing or empty" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
                                }
                                
                            });
             
         }
            failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                                                
                            });}];
    }
    
    else
    {
        
        [self UserInteraction:YES];
        [_txt_dob becomeFirstResponder];
        [_btn_Edit setTitle:@"Save" forState:UIControlStateNormal];
    }
    
}

-(void)UserInteraction:(BOOL)bol
{
    [_txt_Name setUserInteractionEnabled:bol];
    [_txt_dob setUserInteractionEnabled:bol];
    [_txt_email setUserInteractionEnabled:bol];
    [_txt_phone setUserInteractionEnabled:bol];
    [_txt_Address setUserInteractionEnabled:bol];
    [_btn_userimage setUserInteractionEnabled:bol];
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod

-(void)webservicecalled
{
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    [SVProgressHUD show];
    if ([_str_recentclientid length]==0)
    {
        [_btn_Edit setHidden:NO];
        Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[obj valueForKey:@"LoginUserID"],@"userid", nil];
    }
    else
    {
        [_btn_Edit setHidden:YES];
        Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_str_recentclientid,@"userid", nil];
    }
    
    [self.webObj postDictionary:Request_data methodname:@"v1/userdetailbyid" view:self.view];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    NSLog(@"response string===%@",dict);
    
    NSArray * main_arr = [dict valueForKeyPath:@"data"];
    str_phone = [main_arr valueForKeyPath:@"phone"];
    str_address = [main_arr valueForKeyPath:@"address"];
    str_image = [main_arr valueForKeyPath:@"image"];
    
    self.txt_Name.text = [NSString stringWithFormat:@"%@",[main_arr valueForKeyPath:@"name"]];
    self.txt_dob.text = [NSString stringWithFormat:@"%@",[main_arr valueForKeyPath:@"dob"]];
    self.txt_email.text = [NSString stringWithFormat:@"%@",[main_arr valueForKeyPath:@"email"]];
    
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    [obj setObject:str_image forKey:@"LoginUserImage"];
    [obj synchronize];
    
    [_img_userimage sd_setImageWithURL:[NSURL URLWithString:str_image]
                      placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
    
    if ([str_phone isEqualToString:@""])
    {
        self.txt_phone.text = @"";
    }
    else
    {
        self.txt_phone.text = str_phone;
    }
    if ([str_address isEqualToString:@""])
    {
        self.txt_Address.text = @"";
    }
    else
    {
        self.txt_Address.text = str_address;
        [_txt_Address sizeToFit];
    }
    
}

#pragma mark -
#pragma mark:ImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [SVProgressHUD dismiss];
    chosenImage = info[UIImagePickerControllerOriginalImage];
    self.img_userimage.image = chosenImage;
    [SVProgressHUD dismiss];
    [imagepick dismissViewControllerAnimated:NO completion:nil];
}

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    navigationController.navigationBar.titleTextAttributes =@{NSForegroundColorAttributeName: [UIColor whiteColor]};
    [imagepick.navigationBar setTintColor:[UIColor whiteColor]];
    imagepick.navigationBar.barTintColor=[UIColor colorWithRed:105.0/255.0 green:198.0/255.0 blue:135.0/255.0 alpha:1];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark -
#pragma mark:Textfield Delegate Method

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    return true;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound )
    {
        return YES;
    }
    if (textView==_txt_Address)
    {
        [textView resignFirstResponder];
    }
    return NO;
}
@end

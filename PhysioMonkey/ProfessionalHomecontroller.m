//
//  ProfessionalHomecontroller.m
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ProfessionalHomecontroller.h"
@interface ProfessionalHomecontroller ()
@end
@implementation ProfessionalHomecontroller

#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self AddConstraints];
    [self DisplayLogoImageinTitleview];
    
    SWRevealViewController *revealController = [self revealViewController];
    UITapGestureRecognizer *tap = [revealController tapGestureRecognizer];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}
-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
    
     UIButton * btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20, 25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
}
#pragma mark -
#pragma mark: UIButton Action

-(IBAction)MyProfilebuttonclicked:(id)sender
{
    [self performSegueWithIdentifier:@"pt_profile" sender:self];
}
-(IBAction)ClientButtonclicked:(id)sender
{
    [self performSegueWithIdentifier:@"pt_client" sender:self];
}
-(IBAction)VerifyNewClientButtonclicked:(id)sender
{
    [self performSegueWithIdentifier:@"pt_verifyclient" sender:self];
}
-(IBAction)RecentClientButtonclicked:(id)sender
{
    [self performSegueWithIdentifier:@"pt_resentclient" sender:self];
}
-(IBAction)MyLibraryButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"pt_library" sender:self];
}
-(IBAction)SubsriptionButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"pt_sub" sender:self];
}
-(IBAction)offerButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"profhomeoffer1" sender:self];
}
-(IBAction)Logoutbuttonclicked:(id)sender
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"logedIn"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"role"];
    
    ViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"main_vc"];
    [self.navigationController pushViewController:vc animated:NO];
}

-(void)AddConstraints
{
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    NSLog(@"height==%f",height);
    if (height==736.000000)
    {
        NSLayoutConstraint * constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_Mylibrary attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:155.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_Mylibrary attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:222.0f];
        
        [self.view addConstraint:constraint_myprofessional];
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_Mylibrary attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:105.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_Mylibrary attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:115.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        NSLayoutConstraint * constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_Subscription
                                                                                 attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:155.0f];
        [self.view addConstraint:constraint_callandar];
        
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_Subscription attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:472.0f];
        
        [self.view addConstraint:constraint_callandar];
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_Subscription attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:105.0f];
        [self.view addConstraint:constraint_callandar];
        
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_Subscription attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:115.0f];
        [self.view addConstraint:constraint_callandar];
    }
    
    
    if (height==667.000000)
    {
        NSLayoutConstraint * constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_Mylibrary attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:135.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_Mylibrary attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:207.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_Mylibrary attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:105.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        constraint_myprofessional = [NSLayoutConstraint constraintWithItem:_btn_Mylibrary attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:115.0f];
        [self.view addConstraint:constraint_myprofessional];
        
        NSLayoutConstraint * constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_Subscription attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:135.0f];
        [self.view addConstraint:constraint_callandar];
        
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_Subscription attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:420.0f];
        [self.view addConstraint:constraint_callandar];
        
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_Subscription attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:105.0f];
        [self.view addConstraint:constraint_callandar];
        
        constraint_callandar = [NSLayoutConstraint constraintWithItem:_btn_Subscription attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:115.0f];
        [self.view addConstraint:constraint_callandar];
    }
}
@end

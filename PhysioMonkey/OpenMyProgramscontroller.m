//
//  OpenMyProgramscontroller.m
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "OpenMyProgramscontroller.h"
@interface OpenMyProgramscontroller ()

@end

@implementation OpenMyProgramscontroller


#pragma mark -
#pragma mark:ViewLoadMethod
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self hidedata];
    str_catopenmyprogram_id = [_dict_myprogram valueForKeyPath:@"id"];
    str_cptid_id = [_dict_ptdetail valueForKeyPath:@"id"];
    self.tbl_view.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.web_obj=[[WebService alloc]init];
    self.web_obj.delegate=self;
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[SVProgressHUD show];
    [self method];
}
#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView1.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView1.frame = titleView.bounds;
    [titleView addSubview:imageView1];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)webservicecalled
{
    //[SVProgressHUD show];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_cptid_id,@"pt_id",str_catopenmyprogram_id,@"cat_id", nil];
    [self.web_obj postapi:Request_data  methodname:@"v1/programslist"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    
    NSString * str_status = [dict valueForKeyPath:@"message"];
    if ([str_status isEqualToString:@"Record not found"])
    {
        [self hidedata];
        [self.web_obj showAlertView:@"Record not found." msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else
    {
        [self showdata];
        arr_main = [dict valueForKeyPath:@"data"];
        arr_title = [arr_main valueForKeyPath:@"title"];
        arr_description= [arr_main valueForKeyPath:@"description"];
        arr_detail = [arr_main valueForKeyPath:@"details"];
        // NSArray * arr_details = [arr_detail objectAtIndex:0];
        arr_id = [arr_main valueForKeyPath:@"id"];
        arr_createddate = [arr_main valueForKeyPath:@"created"];
        arr_ptdetail = [dict valueForKeyPath:@"pt_detail"];
        str_ptname = [_dict_ptdetail valueForKeyPath:@"name"];
        str_ptphone = [_dict_ptdetail valueForKeyPath:@"phone"];
        str_ptimage = [_dict_ptdetail valueForKeyPath:@"image"];
        str_ptemail = [_dict_ptdetail valueForKeyPath:@"email"];
        str_ptcliniclogo = [arr_ptdetail valueForKeyPath:@"clinic_logo"];
        str_ptid = [_dict_ptdetail valueForKeyPath:@"id"];
        _lbl_ptname.text = str_ptname;
        _lbl_ptemail.text = str_ptemail;
        _lbl_ptphone.text = str_ptphone;
        self.lbl_cat_name.text = _str_catname;
        [self.tbl_view reloadData];
        [_img_ptimage sd_setImageWithURL:[NSURL URLWithString:str_ptimage]
                        placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
        
        if (![str_ptcliniclogo isEqual:[NSNull null]])
        {
            [_img_ptlogo sd_setImageWithURL:[NSURL URLWithString:str_ptcliniclogo]
                           placeholderImage:[UIImage imageNamed:@""]];
        }
        
        [self.web_obj imageround2:_img_ptimage];
    }
}

#pragma mark - IBAction
-(void)method
{
    //[SVProgressHUD show];
}
-(void)hidedata
{
    [_lbl_line1 setHidden:YES];
    [_lbl_line2 setHidden:YES];
    [_lbl_ptname setHidden:YES];
    [_lbl_ptemail setHidden:YES];
    [_lbl_cat_name setHidden:YES];
    [_lbl_ptphone setHidden:YES];
    [_img_ptlogo setHidden:YES];
    [_img_ptemail setHidden:YES];
    [_img_ptphone setHidden:YES];
    [_img_ptname setHidden:YES];
    [_img_ptimage setHidden:YES];
}
-(void)showdata
{
    [_lbl_line1 setHidden:NO];
    [_lbl_line2 setHidden:NO];
    [_lbl_ptname setHidden:NO];
    [_lbl_ptemail setHidden:NO];
    [_lbl_cat_name setHidden:NO];
    [_lbl_ptphone setHidden:NO];
    [_img_ptlogo setHidden:NO];
    [_img_ptemail setHidden:NO];
    [_img_ptphone setHidden:NO];
    [_img_ptname setHidden:NO];
    [_img_ptimage setHidden:NO];
}
-(void)backbuttonclick
{
    [self .navigationController popViewControllerAnimated:NO];
}
-(IBAction)openprogramclicked:(UIButton*)sender
{
    programcontroller * programVC = [self.storyboard instantiateViewControllerWithIdentifier:@"programcontroller"];
    programVC.dictclient = _dict_ptdetail;
    programVC.str_logo = str_ptcliniclogo;
    programVC.program_id = arr_id [sender.tag];
    [self.navigationController pushViewController:programVC animated:NO];
}


#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_id count] ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"mycell";
    MyProgramsCell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[MyProgramsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    cell.scroll_view.delegate = self;
    cell.lbl_myprogram_title.text = [arr_title objectAtIndex:indexPath.row];
    cell.txt_myprogram_discription.text = [arr_description objectAtIndex:indexPath.row];
    cell.lbl_myprogramcreateddate.text = [arr_createddate objectAtIndex:indexPath.row];
    NSArray * arr_dict = [arr_detail objectAtIndex:indexPath.row];
    NSLog(@"arr_dict==%@",arr_dict);
    filearray = [arr_dict valueForKeyPath:@"file"];
    NSLog(@"filearray==%@",filearray);
    cell.txt_myprogram_discription.contentInset = UIEdgeInsetsMake(-7.0,0.0,0,0.0);
    
    cell.btn_myprogramopen.tag= indexPath.row;
    [cell.btn_myprogramopen addTarget:self action:@selector(openprogramclicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (IS_IPHONE_4_OR_LESS)
    {
        cell.scroll_view.contentSize = CGSizeMake(1000, 105);
    }
    if (IS_IPHONE_5)
    {
        cell.scroll_view.contentSize = CGSizeMake(1000, 105);
    }
    
    if (IS_IPHONE_6)
    {
        cell.scroll_view.contentSize = CGSizeMake(1000, 105);
    }
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in cell.scroll_view.subviews)
    {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    
    cell.scroll_view.contentSize = contentRect.size;
    
    if ([filearray count]==0)
    {
        //_heightConstraint.constant = 0;
    }
    else
    {
        for (int i = 0; i < [filearray count]; i++)
        {
            CGFloat xorigin = i * 148;
            UIImageView * imag_view = [[UIImageView alloc]init];
            [imag_view setFrame:CGRectMake(xorigin,8,140,89)];
            [imag_view sd_setImageWithURL:[NSURL URLWithString:[filearray objectAtIndex:i]]
                         placeholderImage:[UIImage imageNamed:@"video_dafault_img.jpg"]];
            [cell.scroll_view addSubview:imag_view];
        }
    }
   
    cell.txt_myprogram_discription.editable = NO;
    cell.txt_myprogram_discription.dataDetectorTypes = UIDataDetectorTypeLink;
    cell.txt_myprogram_discription.delegate = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([filearray count]==0)
    {
        return 80;
    }
    else
    {
      return 195;
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange NS_AVAILABLE_IOS(7_0)
{
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

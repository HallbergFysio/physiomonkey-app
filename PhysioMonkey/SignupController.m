//
//  SignupController.m
//  PhysioMonkey
//
//  Created by prateek on 02/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "SignupController.h"

@interface SignupController ()

@end

@implementation SignupController
@synthesize Check_btn,Back_btn,creatingaccount_lbl,txt_name,txt_email,txt_password,txt_personal_id,txt_conform_password,Login_btn;


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    creatingaccount_lbl.numberOfLines = 1;
    [creatingaccount_lbl setMinimumScaleFactor:10.0];
    creatingaccount_lbl.adjustsFontSizeToFitWidth = YES;
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
    [self createDatePicker];
    [Check_btn setBackgroundImage:[UIImage imageNamed:@"unchekedsign1.png"] forState:UIControlStateNormal];
    
    self.view_subview.hidden=YES;
    self.view_image.hidden=YES;
    self.view_Back_btn.hidden=YES;
    self.view_logoimage.hidden=YES;
    
}

#pragma mark -
#pragma mark: UIButton Action
-(IBAction)Backbutton_clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
-(IBAction)viewbackbutton_clicked:(id)sender
{
    self.view_subview.hidden=YES;
}
-(IBAction)Termsofservicebutton_clicked:(id)sender
{
  [self termsANDprivacyWebServicecalled]; 
}

-(IBAction)Privacypolicybutton_clicked:(id)sender
{
    [self termsANDprivacyWebServicecalled];
}

-(void)termsANDprivacyWebServicecalled
{
    [self.webObj postapi:nil methodname:@"v1/get_terms_and_condition"];
}
-(void)createDatePicker
{
    datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode=UIDatePickerModeDate;
    [datePicker setBackgroundColor:[UIColor colorWithRed:77.0f/255.0f green:154.0f/255.0f blue:79.0f/255.0f alpha:1.0]];
    [txt_personal_id setInputView:datePicker];
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setBarTintColor:[UIColor colorWithRed:69.0f/255.0f green:28.0f/255.0f blue:113.0f/255.0f alpha:1.0]];
    
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onClickingDoneBtn)];
    [doneBtn setTag:1];
    [doneBtn setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *cancelBtn=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(onClickingCancelBtn)];
    [cancelBtn setTag:1];
    [cancelBtn setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,space,doneBtn, nil]];
    [txt_personal_id setInputAccessoryView:toolBar];
}

-(IBAction)checkbutton_clicked:(UIButton*)sender
{
    if (sender.selected == NO)
    {
        [Check_btn setBackgroundImage:[UIImage imageNamed:@"chekedsign1.png"] forState:UIControlStateNormal];
        str_check = @"checkclicked";
        sender.selected = YES;
    }
    else
    {
        str_check = @"";
        [Check_btn setBackgroundImage:[UIImage imageNamed:@"unchekedsign1.png"] forState:UIControlStateNormal];
        sender.selected = NO;
    }
    
}

-(void)onClickingDoneBtn
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    txt_personal_id.text=[NSString stringWithFormat:@"%@",[dateFormat stringFromDate:datePicker.date]];
    [self.txt_personal_id resignFirstResponder];
}

-(void)onClickingCancelBtn
{
    [RequiredpickerView removeFromSuperview];
    [txt_personal_id resignFirstResponder];
}
-(IBAction)Signup_clicked:(id)sender
{
    if ([txt_name.text isEqual:@""])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter Name!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([txt_email.text isEqual:@""])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter E-Mail id!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if (![self validateEmailWithString:txt_email.text])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter Valid E-Mail id!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([txt_personal_id.text isEqual:@""])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter DOB!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([txt_password.text isEqual:@""])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter Password!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([txt_conform_password.text isEqual:@""])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter Conform Password!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if (![txt_password.text isEqualToString:txt_conform_password.text])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter Same Password and Conform Password!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([str_check isEqualToString:@""])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Accept on Terms and Condition!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([str_check length]==0)
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Accept Terms and Condition!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else
    {
        NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
        if ([[obj valueForKey:@"clientprovalue"]isEqualToString:@"1"])
        {
            Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:txt_name.text,@"name",txt_email.text,@"email",txt_personal_id.text, @"dob",txt_password.text,@"password",txt_conform_password.text,@"conf_password",@"3",@"role", nil];
        }
        if ([[obj valueForKey:@"clientprovalue"]isEqualToString:@"2"])
        {
            Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:txt_name.text,@"name",txt_email.text,@"email",txt_personal_id.text, @"dob",txt_password.text,@"password",txt_conform_password.text,@"conf_password",@"2",@"role", nil];
        }
        [SVProgressHUD show];
        [self.webObj postDictionary:Request_data methodname:@"v1/userregistration" view:self.view];
    }
    
    
}
#pragma mark -
#pragma mark:Common Method
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    NSLog(@"response string===%@",dict);
    NSString * status = [dict valueForKeyPath:@"message"];
    if ([status isEqualToString:@"User successfully registered"])
    {
        [self.navigationController popViewControllerAnimated:NO];
        [self.webObj showAlertView:@"Registration!" msg:@"Successfull!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([status isEqualToString:@"Terms And Condition"])
    {
        
        
        self.view_subview.hidden=NO;
        self.view_image.hidden=NO;
        self.view_Back_btn.hidden=NO;
        self.view_logoimage.hidden=NO;

        NSArray * arr = [dict valueForKey:@"data"];
        
        [_web_view loadHTMLString:[[arr valueForKey:@"description"] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"] baseURL:nil];
        
        
        
    }
    else
    {
        [self.webObj showAlertView:@"Registration!" msg:@"Failed!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
}

#pragma mark -
#pragma mark:UITextfieldDelegateMethod
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}
-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    
    if ([[UIScreen mainScreen] bounds].size.height==568.000000)
    {
        
        if ([textField tag]==4)
        {
            const long movementDistance =0* [textField tag]; // tweak as needed
            const float movementDuration = 0.2f; // tweak as needed
            long movement = (up ? movementDistance : -movementDistance);
            [UIView beginAnimations:@"animateTextField" context: nil];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration: movementDuration];
            self.view.frame = CGRectOffset(self.view.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        if ([textField tag]==5)
        {
            const long movementDistance =-10* [textField tag]; // tweak as needed
            const float movementDuration = 0.2f; // tweak as needed
            long movement = (up ? movementDistance : -movementDistance);
            [UIView beginAnimations:@"animateTextField" context: nil];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration: movementDuration];
            self.view.frame = CGRectOffset(self.view.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        
    }
    
}


@end

//
//  AddCommentController.h
//  PhysioMonkey
//
//  Created by prateek on 03/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCommentController : UIViewController <UIScrollViewDelegate,UIScrollViewAccessibilityDelegate,WebDataDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIPickerViewAccessibilityDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIPickerView * myPickerView;
    UIPickerView * picker_startmonthname;
    UIPickerView * picker_endmonthname;
    
    NSArray * arr_name;
    NSArray * arr_typeid;
    NSArray * arridpt;
    NSArray *  ptnamearr;
    NSArray *  ptidarr;
    NSArray * arr_ptdata;
    NSArray * ptname;
    NSArray * ptid;
    NSArray * arrOfColor;
    
    NSString *strptid;
    NSString * pt_str;
    NSString * str_mtdtype;
    NSString * str_typeid;
    NSString *searchTextString;
    NSString * str_startmonthname;
    NSString * str_checkstartmonth;
    NSString * str_checkendmonth;
    NSString * str_checkenddays;
    NSString * str_checkstartdays;
    NSString * str_checkendyear;
    NSString * str_checkstartyear;
    NSString * str_checktype;
    NSString * str_checkstartampm;
    NSString * str_checkendampm;
    
    NSString * str_checkstarthour;
    NSString * str_checkendhour;
    
    NSString * str_checkstartminute;
    NSString * str_checkendminute;
    
    
    NSMutableArray *searchArray;
    NSMutableArray * arr_days ;
    NSMutableArray * arr_hours;
    NSMutableArray * arr_monthname;
    NSMutableArray *barItems_endmonth;
    NSMutableArray * arrampm;
    NSMutableArray * arr_year;
    NSMutableArray * arr_minute;
    
    UIBarButtonItem *flexSpace_bar;
    UIBarButtonItem *btnCancel_endmonth;
    
    NSMutableDictionary *Request_data;
    BOOL isFilter;
    UIToolbar *toolbar_endmonth;
}

//IBOUTLET
@property (nonatomic,retain)IBOutlet UIButton * btn_viewcalender;
@property (nonatomic,retain)IBOutlet UIButton * btn_previouscomment;

@property (nonatomic,retain)IBOutlet UITextField * txt_type;
@property (nonatomic,retain)IBOutlet UITextField * txt_title;
@property (nonatomic,retain)IBOutlet UITextField * txt_details;
@property (nonatomic,retain)IBOutlet UITextField * txt_professional;

@property (nonatomic,retain)IBOutlet UITextField * txt_Startmonthname;
@property (nonatomic,retain)IBOutlet UITextField * txt_Startdays;
@property (nonatomic,retain)IBOutlet UITextField * txt_Startyear;

@property (nonatomic,retain)IBOutlet UITextField * txt_endmonthname;
@property (nonatomic,retain)IBOutlet UITextField * txt_enddays;
@property (nonatomic,retain)IBOutlet UITextField * txt_endyear;

@property (nonatomic,retain)IBOutlet UITextField * txt_starthour;
@property (nonatomic,retain)IBOutlet UITextField * txt_startminute;
@property (nonatomic,retain)IBOutlet UITextField * txt_startampm;

@property (nonatomic,retain)IBOutlet UITextField * txt_endhours;
@property (nonatomic,retain)IBOutlet UITextField * txt_endminute;
@property (nonatomic,retain)IBOutlet UITextField * txt_endampm;

@property (nonatomic,retain)IBOutlet UIScrollView * scroll_view;
@property (nonatomic,retain)IBOutlet UITableView * tbl_view;

@property (nonatomic,retain)WebService * webobj;

@property (nonatomic,retain)NSDictionary * dic_comment;
@property (nonatomic,retain)NSString * str_classname;

//IBACTION
-(IBAction)viewcalenderclicked:(id)sender;
-(IBAction)previousbuttonclicked:(id)sender;
-(IBAction)savebuttonclicked:(id)sender;
@end

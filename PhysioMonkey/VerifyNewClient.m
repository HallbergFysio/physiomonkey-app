//
//  VerifyNewClient.m
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "VerifyNewClient.h"

@interface VerifyNewClient ()

@end

@implementation VerifyNewClient


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.CommonMethodObj =[[CommonMethodClass alloc]init];
     self.tbl_verifyclient.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                    action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [SVProgressHUD show];
    [self method];
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark -
#pragma mark: UIButton Action
-(void)dismissKeyboard
{
    [_SearchBar resignFirstResponder];
}

-(void)method
{
    [SVProgressHUD show];
}

-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"profverify" sender:self];
}

-(void)verifyclicked:(UIButton *)sender
{
    [SVProgressHUD show];
    NSLog(@"RequestSend_clicked");
    str_checkstatus = @"verify_clicked";
    NSDictionary *commentData = main_arr[sender.tag];
    NSLog(@"all data %@",commentData);
    NSString * str_pt_id = [commentData valueForKeyPath:@"id"];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_pt_id,@"client_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/requestaccept"];
}
-(void)verifyCancelclicked:(UIButton *)sender
{
    [SVProgressHUD show];
    NSLog(@"RequestSend_clicked");
    str_checkstatus = @"cancel_clicked";
    NSDictionary *commentData = main_arr[sender.tag];
    NSLog(@"all data %@",commentData);
    NSString * str_pt_id = [commentData valueForKeyPath:@"id"];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_pt_id,@"client_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/cancelreqbypt"];
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)webservicecalled
{
    [SVProgressHUD show];
    [self.webobj postapi:nil methodname:@"v1/verifynewclientslist"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    str_status = [dict valueForKeyPath:@"message"];
    
    if ([str_checkstatus isEqualToString:@"verify_clicked"])
    {
        if ([str_status isEqualToString:@"Client verified."])
        {
            str_checkstatus = @"";
            [self webservicecalled];
        }
    }
    else if ([str_checkstatus isEqualToString:@"cancel_clicked"])
    {
        if ([str_status isEqualToString:@"Request canceled successful"])
        {
            str_checkstatus = @"";
            [self webservicecalled];
        }
    }
    
    else
    {
        if ([str_status isEqualToString:@"Record not found"])
        {
            [self.tbl_verifyclient setHidden:YES];
            [self.webobj showAlertView:nil msg:@"No new client at the moment" atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        else
        {
             main_arr = [dict valueForKeyPath:@"data"];
            arr_phone = [main_arr valueForKeyPath:@"phone"];
            arr_name = [main_arr valueForKeyPath:@"name"];
            arr_email = [main_arr valueForKeyPath:@"email"];
            arr_image = [main_arr valueForKeyPath:@"image"];
            arr_id = [main_arr valueForKeyPath:@"id"];
            [self.tbl_verifyclient reloadData];
        }
    }
}


#pragma mark -
#pragma mark: UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_id count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"verifyclientcell";
    clientcell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[clientcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    cell.lbl_Verifyclientname.text = [arr_name objectAtIndex:indexPath.row];
    cell.lbl_Verifyclientemail.text = [arr_email objectAtIndex:indexPath.row];
    cell.lbl_Verifyphone.text = [arr_phone objectAtIndex:indexPath.row];
    
    if (![[arr_image objectAtIndex:indexPath.row]isEqual:[NSNull null]])
    {
        [cell.Verifyimg_user sd_setImageWithURL:[NSURL URLWithString:[arr_image objectAtIndex:indexPath.row]]
                               placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
    }
    
    [self.webobj imageround2:cell.Verifyimg_user];
    [self.CommonMethodObj imageRounded:cell.btn_Verify];
    [self.CommonMethodObj imageRounded:cell.btn_VerifyCancel];
    
    cell.btn_Verify.tag = indexPath.row;
    [cell.btn_Verify addTarget:self action:@selector(verifyclicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.btn_VerifyCancel.tag = indexPath.row;
    [cell.btn_VerifyCancel addTarget:self action:@selector(verifyCancelclicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark: search bar method
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

@end

//
//  callendercell.h
//  PhysioMonkey
//
//  Created by prateek on 03/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface callendercell : UITableViewCell

@property (nonatomic,retain)IBOutlet UILabel * lbl_title;
@property (nonatomic,retain)IBOutlet UILabel * lbl_start;
@property (nonatomic,retain)IBOutlet UILabel * lbl_end;
@property (nonatomic,retain)IBOutlet UILabel * lbl_type;
@property (nonatomic,retain)IBOutlet UIButton * btn_action;

//IBOUTLET addcomment

@property (nonatomic,retain)IBOutlet UILabel * lbl_Commenttitle;
@end

//
//  MyProfessionalController.h
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfessionalController : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate , UITextViewDelegate>
{
   
    NSArray * arr_name;
    NSArray * arr_image;
    NSArray * arr_phone;
    NSArray * arr_email;
    NSArray * arr_Requestsent;
    NSArray * arr_id;
    NSArray * main_arr;
    NSArray *filtered;
    NSArray *Filterarr_name;
    NSArray * Filterarr_image;
    NSArray *  Filterarr_phone;
    NSArray *  Filterarr_email;
    NSArray *  Filterarr_id;
    
    NSMutableDictionary * Request_data;
    BOOL isFilter;
    NSString * str_checkstatus;
}
//IBOUTLET
@property(nonatomic,strong)IBOutlet UISearchBar *search_bar;
@property(nonatomic,retain)IBOutlet UITableView * tbl_MyProfessional;
@property(nonatomic,strong)CommonMethodClass*CommonMethodObj;
@property(nonatomic,strong)WebService *webObj;
@end

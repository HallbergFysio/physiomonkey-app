//
//  RecentProgramCell.h
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentProgramCell : UITableViewCell <UITextViewDelegate>

//IBOUTLET
@property (nonatomic,retain)IBOutlet UILabel * lbl_cat_name;
@property (nonatomic,retain)IBOutlet UILabel * lbl_title;
@property (nonatomic,retain)IBOutlet UILabel * lbl_startdate;
@property (nonatomic,retain)IBOutlet UITextView * lbl_description;
@property (nonatomic,retain) IBOutlet UIScrollView * scroll_view;
@property (nonatomic,retain)IBOutlet UIButton * btn_OpenProgram;


@end

//
//  ViewController.h
//  PhysioMonkey
//
//  Created by prateek on 02/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    NSUserDefaults * obj;
    NSString * clientprovalue;
}

//IBOUTLET
@property (nonatomic,retain)IBOutlet UIImageView * Back_img;
@property (nonatomic,retain)IBOutlet UIImageView * logo_img;

@property (nonatomic,retain)IBOutlet UIButton * Btn_professional;
@property (nonatomic,retain)IBOutlet UIButton * Btn_client;

//IBACTION
-(IBAction)ProfessionalButtonClicked:(id)sender;
-(IBAction)ClientButtonClicked:(id)sender;


@end


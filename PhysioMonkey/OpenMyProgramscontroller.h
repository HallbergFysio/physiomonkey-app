//
//  OpenMyProgramscontroller.h
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface OpenMyProgramscontroller : UIViewController <WebDataDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate , UITextViewDelegate>
{
    NSArray * arr_main;
    NSArray * arr_id;
    NSArray * arr_image;
    NSArray * arr_title;
    NSArray * arr_detail;
    NSArray * arr_description;
    NSArray * arr_createddate;
    NSArray * arr_ptdetail;
    NSArray * filearray;
    
    NSString * str_ptid;
    NSString * str_catopenmyprogram_id ;
    NSString * str_cptid_id ;
    NSString * str_ptname;
    NSString * str_ptphone;
    NSString * str_ptimage;
    NSString * str_ptemail;
    NSString * str_ptcliniclogo;
    
    NSMutableDictionary * Request_data;
    UIImageView *imageView;
}

//IBOUTLET


@property (nonatomic,retain)IBOutlet UILabel * lbl_line1;
@property (nonatomic,retain)IBOutlet UILabel * lbl_line2;
@property (nonatomic,retain)IBOutlet UILabel * lbl_cat_name;
@property (nonatomic,retain)IBOutlet UILabel * lbl_ptname;
@property (nonatomic,retain)IBOutlet UILabel * lbl_ptphone;
@property (nonatomic,retain)IBOutlet UILabel * lbl_ptemail;

@property (nonatomic,retain)IBOutlet UIImageView * img_ptimage;
@property (nonatomic,retain)IBOutlet UIImageView * img_ptlogo;
@property (nonatomic,retain)IBOutlet UIImageView * img_ptemail;
@property (nonatomic,retain)IBOutlet UIImageView * img_ptphone;
@property (nonatomic,retain)IBOutlet UIImageView * img_ptname;

@property (nonatomic,retain)IBOutlet UITableView * tbl_view;
//@property (nonatomic,retain)IBOutlet NSLayoutConstraint * heightConstraint;
@property (nonatomic,retain)NSArray * dict_myprogram;
@property (nonatomic,retain)NSDictionary * dict_ptdetail;
@property (nonatomic,retain)WebService * web_obj;
@property (nonatomic,retain)NSString * str_catname;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightconstraints;

//IBACTION
-(IBAction)openprogramclicked:(id)sender;

@end

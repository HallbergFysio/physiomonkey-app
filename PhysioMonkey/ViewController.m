//
//  ViewController.m
//  PhysioMonkey
//
//  Created by prateek on 02/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"
#import "SignupController.h"
#import <malloc/malloc.h>

@interface ViewController ()

@end

@implementation ViewController
@synthesize Back_img,logo_img,Btn_client,Btn_professional;

#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    obj = [NSUserDefaults standardUserDefaults];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

#pragma mark -
#pragma mark: UIButton Action

-(IBAction)ClientButtonClicked:(id)sender
{
    clientprovalue=@"1";
    [obj setObject:clientprovalue forKey:@"clientprovalue"];
    [obj synchronize];
    LoginViewController * Client_obj = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:Client_obj animated:NO];
}

-(IBAction)ProfessionalButtonClicked:(id)sender
{
    clientprovalue=@"2";
    [obj setObject:clientprovalue forKey:@"clientprovalue"];
    [obj synchronize];
    LoginViewController * Client_obj = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:Client_obj animated:NO];
}
@end

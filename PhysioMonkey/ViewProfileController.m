//
//  ViewProfileController.m
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ViewProfileController.h"
@interface ViewProfileController ()
@end
@implementation ViewProfileController


#pragma mark -
#pragma mark:ViewLoadMethod
- (void)viewDidLoad
{
    [super viewDidLoad];
    str_id = [_arr_viewprofile valueForKeyPath:@"id"];
    
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
    [self.webObj imageround1:_img_user];
    
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}



#pragma mark -
#pragma mark:HTTPWebserviceMethod

-(void)webservicecalled
{
    [SVProgressHUD show];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_id,@"userid", nil];
    [self.webObj postDictionary:Request_data methodname:@"v1/userdetailbyid" view:self.view];
}
-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    
    NSArray * main_arr = [dict valueForKeyPath:@"data"];
    str_phone = [main_arr valueForKeyPath:@"phone"];
    str_address = [main_arr valueForKeyPath:@"address"];
    str_image = [main_arr valueForKeyPath:@"image"];
    str_name = [main_arr valueForKeyPath:@"name"];
    str_email = [main_arr valueForKeyPath:@"email"];
    str_website = [main_arr valueForKeyPath:@"website"];
    str_aboutme = [main_arr valueForKeyPath:@"about_me"];
    str_clinicname = [main_arr valueForKeyPath:@"clinic_name"];
    str_logoimg = [main_arr valueForKeyPath:@"clinic_logo"];
    str_twitter = [main_arr valueForKeyPath:@"twitter"];
    str_linkedin = [main_arr valueForKeyPath:@"linkedin"];
    str_facebook = [main_arr valueForKeyPath:@"facebook"];
    
    self.lbl_name.text = str_name;
    self.lbl_phone.text = str_phone;
    self.lbl_address.text = str_address;
    [_lbl_address sizeToFit];
    self.lbl_email.text = str_email;
    self.lbl_clinicname.text = str_clinicname;
    self.lbl_aboutme.text = str_aboutme;
    self.lbl_website.text = str_website;
    
    [_img_user sd_setImageWithURL:[NSURL URLWithString:str_image]
                 placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
    

    [_img_clinic_logo sd_setImageWithURL:[NSURL URLWithString:str_logoimg]
                        placeholderImage:[UIImage imageNamed:@"cliniclogo"]];
    
     self.lbl_website.editable = NO;
     self.lbl_website.dataDetectorTypes = UIDataDetectorTypeLink;
     self.lbl_website.delegate = self;
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange NS_AVAILABLE_IOS(7_0)
{
    return YES;
}

#pragma mark - IBAction

-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)facebookBtnclicked:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str_facebook] options:@{} completionHandler:nil];
}

-(IBAction)twitterBtnclicked:(id)sender
{
   
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str_twitter] options:@{} completionHandler:nil];
}

-(IBAction)LinkedinBtnclicked:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str_linkedin] options:@{} completionHandler:nil];
}
@end

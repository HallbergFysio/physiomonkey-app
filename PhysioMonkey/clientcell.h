//
//  clientcell.h
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface clientcell : UITableViewCell


//IBOUTLET CLIENTCONTROLLER
@property (nonatomic,retain)IBOutlet UILabel * lbl_clientname;
@property (nonatomic,retain)IBOutlet UILabel * lbl_clientnumber;
@property (nonatomic,retain)IBOutlet UILabel * lbl_clientdob;
@property (nonatomic,retain)IBOutlet UILabel * lbl_clientemail;
@property (nonatomic,retain)IBOutlet UIImageView * img_user;
@property (nonatomic,retain)IBOutlet UIButton * btn_Calender;
@property (nonatomic,retain)IBOutlet UIButton * btn_CreateProgram;
@property (nonatomic,retain)IBOutlet UIButton * btn_ViewPreviousProgram;
@property (nonatomic,retain)IBOutlet UIButton * btn_UseExistingProgram;
@property (nonatomic,retain)IBOutlet UIButton * btn_ViewProfile;




//IBOUTLET RECENTCLIENT
@property (nonatomic,retain)IBOutlet UILabel * lbl_Recentclientname;
@property (nonatomic,retain)IBOutlet UILabel * lbl_Recentclientnumber;
@property (nonatomic,retain)IBOutlet UILabel * lbl_Recentclientdob;
@property (nonatomic,retain)IBOutlet UILabel * lbl_Recentclientemail;
@property (nonatomic,retain)IBOutlet UIImageView * Recentimg_user;
@property (nonatomic,retain)IBOutlet UIButton * btn_RecentCalender;
@property (nonatomic,retain)IBOutlet UIButton * btn_RecentCreateProgram;
@property (nonatomic,retain)IBOutlet UIButton * btn_RecentViewPreviousProgram;
@property (nonatomic,retain)IBOutlet UIButton * btn_RecentUseExistingProgram;
@property (nonatomic,retain)IBOutlet UIButton * btn_RecentViewProfile;




//IBOUTLET VERIFYCLIENT
@property (nonatomic,retain)IBOutlet UILabel * lbl_Verifyclientname;
@property (nonatomic,retain)IBOutlet UILabel * lbl_Verifyphone;
@property (nonatomic,retain)IBOutlet UILabel * lbl_Verifyclientdob;
@property (nonatomic,retain)IBOutlet UILabel * lbl_Verifyclientemail;
@property (nonatomic,retain)IBOutlet UIImageView * Verifyimg_user;
@property (nonatomic,retain)IBOutlet UIButton * btn_Verify;
@property (nonatomic,retain)IBOutlet UIButton * btn_VerifyCancel;


@end

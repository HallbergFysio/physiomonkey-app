//
//  offercontrollercell.h
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface offercontrollercell : UITableViewCell <UITextViewDelegate>

//IBOUTLET
@property (nonatomic,retain)IBOutlet UILabel * lbl_offertye;
@property (nonatomic,retain)IBOutlet UITextView * lbl_offerpercent;
@property (nonatomic,retain)IBOutlet UIImageView * img_offer;

//IBOUTLET PROFESSIONAL
@property (nonatomic,retain)IBOutlet UILabel * lbl_Profoffertye;
@property (nonatomic,retain)IBOutlet UITextView * lbl_Profofferpercent;
@property (nonatomic,retain)IBOutlet UIImageView * img_Profoffer;
@end

//
//  ptviewprofile.m
//  PhysioMonkey
//
//  Created by prateek on 4/6/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ptviewprofile.h"
#import "SWRevealViewController.h"

@interface ptviewprofile ()
@end
@implementation ptviewprofile
@synthesize imagepick;

#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    [self webservicecalled];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SVProgressHUD show];
    [self webservicecalled];
    [self.webObj imageround1:_img_userimage];
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}
-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark: HTTPWebserviceMethod
-(void)webservicecalled
{
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    [SVProgressHUD show];
    if ([_str_recentclientid length]==0)
    {
        
        Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[obj valueForKey:@"LoginUserID"],@"userid", nil];
    }
    else
    {
        Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_str_recentclientid,@"userid", nil];
    }
    
    [self.webObj postDictionary:Request_data methodname:@"v1/clientProfileViewByPt" view:self.view];
}


-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    NSLog(@"response string===%@",dict);
    
    NSArray * main_arr = [dict valueForKeyPath:@"data"];
    str_phone = [main_arr valueForKeyPath:@"phone"];
    str_address = [main_arr valueForKeyPath:@"address"];
    str_image = [main_arr valueForKeyPath:@"image"];
    
    self.txt_Name.text = [NSString stringWithFormat:@"%@",[main_arr valueForKeyPath:@"name"]];
    self.txt_dob.text = [NSString stringWithFormat:@"%@",[main_arr valueForKeyPath:@"dob"]];
    self.txt_email.text = [NSString stringWithFormat:@"%@",[main_arr valueForKeyPath:@"email"]];
    
    
    [_img_userimage sd_setImageWithURL:[NSURL URLWithString:str_image]
                      placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
    
    
    
    
    
    if ([str_phone isEqualToString:@""])
    {
        self.txt_phone.text = @"";
    }
    else
    {
        self.txt_phone.text = str_phone;
    }
    if ([str_address isEqualToString:@""])
    {
        self.txt_Address.text = @"";
    }
    else
    {
        self.txt_Address.text = str_address;
        [self.txt_Address sizeToFit];
    }
    
}

@end

//
//  LeftController.h
//  PhysioMonkey
//
//  Created by prateek on 03/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftController : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate>
{
    NSArray * arr_leftmenuItem_title;
    NSArray * arr_leftmenuItem_img;
}
@property (nonatomic,retain)IBOutlet UITableView * table_view;
@property (nonatomic,retain)IBOutlet UILabel * lbl_username;
@property (nonatomic,retain)IBOutlet UIImageView * img_userimage;
@property (nonatomic,retain)WebService * webobj;

@end

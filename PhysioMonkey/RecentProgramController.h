//
//  RecentProgramController.h
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentProgramController : UIViewController <UITableViewDataSource,UITableViewDelegate,WebDataDelegate,UISearchBarDelegate,UITextViewDelegate>
{
    NSArray * arr_title;
    NSArray * arr_description;
    NSArray * arr_detail;
    NSArray * arr_createddate;
    NSArray * arr_cat_name;
    NSArray * arr_ptdetail;
    NSArray * arr_id;
    NSArray * main_arr;
    
    NSString * str_checkstatus;
    NSMutableDictionary * Request_data;
    NSMutableDictionary * dictclient;
}

//IBOUTLET
@property (strong,nonatomic)IBOutlet UISearchBar *search_bar;
@property (nonatomic,retain)IBOutlet UITableView * tbl_RecentPrograms;
@property (nonatomic,strong)CommonMethodClass*CommonMethodObj;
@property (nonatomic,retain)WebService * webobj;
@end

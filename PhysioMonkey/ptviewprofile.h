//
//  ptviewprofile.h
//  PhysioMonkey
//
//  Created by prateek on 4/6/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface ptviewprofile : UIViewController <WebDataDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>
{
    NSMutableDictionary *Request_data;
    NSString * str_phone;
    NSString * str_address;
    NSString * str_image;
    NSString * str_common;
    UIImage  * chosenImage;
}

//IBOUTLET

@property (nonatomic,retain)IBOutlet UIButton * btn_userimage;
@property (nonatomic,retain)IBOutlet UIButton * btn_sidemenu;
@property (nonatomic,retain)IBOutlet UIButton * btn_Edit;

@property (nonatomic,retain)IBOutlet UITextField * txt_dob;
@property (nonatomic,retain)IBOutlet UITextField * txt_phone;
@property (nonatomic,retain)IBOutlet UITextField * txt_email;
@property (nonatomic,retain)IBOutlet UITextField * txt_Name;

@property (nonatomic,retain)IBOutlet UITextView * txt_Address;
@property (nonatomic,retain)IBOutlet UIImageView * img_userimage;

@property (nonatomic,retain)UIImagePickerController *imagepick;
@property (nonatomic,retain)NSString * str_recentclientid;
@property (nonatomic,strong) WebService *webObj;



@end

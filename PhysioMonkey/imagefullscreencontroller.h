//
//  imagefullscreencontroller.h
//  PhysioMonkey
//
//  Created by prateek on 02/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface imagefullscreencontroller : UIViewController

//IBOUTLET

@property (nonatomic,retain)IBOutlet UIImageView * img_view;
@property (nonatomic,retain)IBOutlet UILabel * lbl_duration;
@property (nonatomic,retain)IBOutlet UISlider * view_progress;
@property (nonatomic,retain)IBOutlet UIView * player_view;
@property (nonatomic,retain)IBOutlet UIButton * back_btn;
@property (nonatomic,retain)NSString * image_str;


//IBACTION

-(IBAction)backbuttonclicked:(id)sender;
@end

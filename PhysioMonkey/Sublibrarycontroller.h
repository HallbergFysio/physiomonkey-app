//
//  Sublibrarycontroller.h
//  PhysioMonkey
//
//  Created by prateek on 09/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Sublibrarycontroller : UIViewController <UITableViewDelegate , UITableViewDataSource , WebDataDelegate , UITextFieldDelegate>
{
    NSMutableDictionary *Request_data;
    NSMutableArray *searchArray;
    
    NSArray * arr_main;
    NSArray * arr_detials;
    NSArray * arr_title;
    NSArray * arr_createdate;
    NSArray * arr_id;
    NSArray * arridpt;
    NSArray * arr_mainassign;
    
    NSString * common_str;
    NSString * str_programidd;
    NSString *strptid;
    NSString *searchTextString;
    NSString * str_prograamidd;
    NSString *Str_savepdf;
    NSString * pdf_id;
    
    BOOL isFilter;
}

//IBOUTLET
@property (nonatomic,retain)IBOutlet UITableView * tbl_view;
@property (nonatomic,retain)IBOutlet UITableView * tblassign_view;

@property (nonatomic,retain)IBOutlet UIButton * btn_close;
@property (nonatomic,retain)IBOutlet UIButton * btn_assignprogram;

@property (nonatomic,retain)IBOutlet UILabel *  lbl_programtitle;
@property (nonatomic,retain)IBOutlet UILabel *  lbl_line;

@property (nonatomic,retain)IBOutlet UIView * assignview;
@property (nonatomic,retain)IBOutlet UITextField * txt_clientname;

@property (nonatomic,retain)WebService *webobj;
@property (nonatomic,retain)NSString * str_cat_idd;
@property (nonatomic,retain)NSString * str_cat_name;

//IBACTION
-(IBAction)assignclosebuttonclick:(id)sender;
-(IBAction)assignprogrambuttonclick:(id)sender;
@end

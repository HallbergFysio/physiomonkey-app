//
//  SubscriptionController.m
//  PhysioMonkey
//
//  Created by prateek on 10/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "SubscriptionController.h"
#define kPayPalEnvironment PayPalEnvironmentNoNetwork

@interface SubscriptionController ()
@end
@implementation SubscriptionController


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.paymentdetailView setHidden:YES];
    
    self.tbl_subsription.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _COMMONOBJ = [[CommonMethodClass alloc]init];
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    
    [self.COMMONOBJ buttonbordercolor:_btn_AdvanceBuyNow];
    [self.COMMONOBJ buttonbordercolor:_btn_RegularBuyNow];
    [self.COMMONOBJ buttonbordercolor:_btn_BasicBuyNow];
    
    [self webservicecalled];
    [self configurationPaypal];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (IS_IPHONE_4_OR_LESS)
    {
        self.scroll_view.contentSize = CGSizeMake(0, 500);
    }
    if (IS_IPHONE_5)
    {
        self.scroll_view.contentSize = CGSizeMake(0, 500);
    }
    
    if (IS_IPHONE_6)
    {
        self.scroll_view.contentSize = CGSizeMake(0, 500);
    }
    
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    
}
#pragma mark -
#pragma mark:HTTPWebserviceMethod

-(void)webservicecalled
{
    [self.webobj postapi:nil methodname:@"v1/plans"];
}

-(void)paymenttransferwebservice
{
    NSLog(@"%ld",(long)plannumber);
    NSString *Str_planid = [NSString stringWithFormat: @"%ld", (long)plannumber];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_transactionid,@"transaction_id",Str_planid,@"plan_id",@"success",@"status",pay_subamount,@"amount",@"USD",@"cc",@"paypal",@"payment_type", nil];
    [self.webobj postapi:Request_data methodname:@"v1/paymentorder"];
}

-(void)receiptwebservicecalled
{
    if ([receipt_value isEqualToString:@"receiptall"])
    {
        receipt_dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"",@"id", nil];
    }
    else
    {
        receipt_dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:payment_iddd,@"id", nil];
    }
    [self.webobj postapi:receipt_dict methodname:@"v1/paymentreceipt"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    NSLog(@"responsejsondata=%@",dict);
    NSString * status = [dict valueForKeyPath:@"message"];
    if ([status isEqualToString:@"plan List"])
    {
        [self.webobj postapi:nil methodname:@"v1/paymenthistory"];
        arr_main = [dict valueForKey:@"data"];
        arr_amount = [arr_main valueForKeyPath:@"amount"];
        arr_description = [arr_main valueForKeyPath:@"description"];
        arr_duration_days = [arr_main valueForKeyPath:@"duration_days"];
        arr_name = [arr_main valueForKeyPath:@"name"];
        arr_planlistid = [arr_main valueForKeyPath:@"id"];
        
        _lbl_plantype1.text =[arr_name objectAtIndex:0];
        _lbl_plantype2.text =[arr_name objectAtIndex:1];
        _lbl_plantype3.text =[arr_name objectAtIndex:2];
        
        _lbl_planamount1.text =[NSString stringWithFormat:@"$%@",[arr_amount objectAtIndex:0]];
        _lbl_planamount2.text =[NSString stringWithFormat:@"$%@",[arr_amount objectAtIndex:1]];
        _lbl_planamount3.text =[NSString stringWithFormat:@"$%@",[arr_amount objectAtIndex:2]];
        
        _lbl_planduration1.text = [NSString stringWithFormat:@"%@ Days",[arr_duration_days objectAtIndex:0]];
        _lbl_planduration2.text = [NSString stringWithFormat:@"%@ Days",[arr_duration_days objectAtIndex:1]];
        _lbl_planduration3.text = [NSString stringWithFormat:@"%@ Days",[arr_duration_days objectAtIndex:2]];
        
        _lbl_plandiscription1.text =[arr_description objectAtIndex:0];
        _lbl_plandiscription2.text =[arr_description objectAtIndex:1];
        _lbl_plandiscription3.text =[arr_description objectAtIndex:2];
        
    }
    else if ([status isEqualToString:@"Payment History."])
    {
        arr_main = [dict valueForKey:@"data"];
        arr_historyAmount = [arr_main valueForKeyPath:@"amount"];
        arr_PlanType = [arr_main valueForKeyPath:@"plan"];
        arr_TransactionNo = [arr_main valueForKeyPath:@"transactionNo"];
        arr_PaymentType = [arr_main valueForKeyPath:@"paymentType"];
        arr_PaymentDate = [arr_main valueForKeyPath:@"paymentDate"];
        arr_ExpiryDate = [arr_main valueForKeyPath:@"expire_date"];
        arr_id = [arr_main valueForKeyPath:@"id"];
        [self.tbl_subsription reloadData];
    }
    else if ([status isEqualToString:@"Payment Receipt."])
    {
        NSArray * arr_pdf = [dict valueForKey:@"data"];
        str_pdf = [arr_pdf valueForKeyPath:@"receipt_pdf"];
        [self downloadpdf];
        NSLog(@"str==%@",str_pdf);
        receipt_value = @"";
        
    }
    else if ([status isEqualToString:@"Payment Successful."])
    {
        [self.webobj showAlertView:@"Payment Successfull" msg:nil atag:0 cncle:@"OK" othr:nil delegate:0];
        [self.webobj postapi:nil methodname:@"v1/paymenthistory"];
    }
}
#pragma mark -
#pragma mark: UIButton Action
-(void)receiptclicked:(UIButton*)sender
{
    ind_ex++;
    //ind_ex = sender.tag;
    [SVProgressHUD show];
    NSArray * arr = arr_main[sender.tag];
    payment_iddd= [arr valueForKeyPath:@"id"];
    NSLog(@"arr==%@",arr);
    [self receiptwebservicecalled];
}

-(void)downloadpdf
{
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@" yyyy-MM-d H:m:s"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    NSLog(@"resultString=%@",resultString);
    
    NSData *dataPdf = [NSData dataWithContentsOfURL:[NSURL URLWithString:str_pdf]];
    
    //Get path directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Physiomonkey"];
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSString *filePath = [NSString stringWithFormat:@"%@/%@ (%@)", documentsDirectory, @"Physiomonkey payment receipt", resultString];
    
    [dataPdf writeToFile:filePath atomically:YES];
    
    NSLog(@"filePath===%@",filePath);
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [SVProgressHUD dismiss];
                       [dataPdf writeToFile:filePath atomically:YES];
                       [self.webobj showAlertView:nil msg:@"Receipt saved" atag:0 cncle:@"OK" othr:nil delegate:nil];
                   });
    
}



-(IBAction)dismissclicked:(id)sender
{
    [self.paymentdetailView setHidden:YES];
}

-(IBAction)ReceiptAllclicked:(id)sender
{
    ind_ex++;
    receipt_value = @"receiptall";
    [SVProgressHUD show];
    [self receiptwebservicecalled];
}

-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"profsubscriptionhome" sender:self];
}

-(void)configurationPaypal
{
    self.environment = PayPalEnvironmentSandbox;
    //self.environment = PayPalEnvironmentProduction;
    [PayPalMobile preconnectWithEnvironment:_environment];
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = YES;
    self.successView.hidden = YES;
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
}

-(IBAction)buynowclicked:(UIButton*)sender
{
    plannumber = [sender tag];
    NSInteger value =  plannumber-1;
    pay_subamount = [arr_amount objectAtIndex:value];
    NSLog(@"%ld",(long)plannumber);
    NSLog(@"%ld",(long)pay_subamount);
    self.resultText = nil;
    
    self.resultText = nil;
    
    // Optional: include multiple items
    PayPalItem *item1 = [PayPalItem itemWithName:@"Old jeans with holes"
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:pay_subamount]
                                    withCurrency:@"USD"
                                         withSku:@"Hip-00037"];
    
    
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = @"USD";
    payment.shortDescription = @"PhysioMonkey";
    payment.items = items;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable)
    {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment configuration:self.payPalConfig delegate:self];
    [self presentViewController:paymentViewController animated:NO completion:nil];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_id count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"subscriptioncell";
    subscriptioncell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[subscriptioncell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    
    cell.lbl_Amount.text = [NSString stringWithFormat:@"$%@",[arr_historyAmount objectAtIndex:indexPath.row]];
    cell.lbl_PaymentDate.text = [arr_PaymentDate objectAtIndex:indexPath.row];
    cell.lbl_ExpirationDate.text = [arr_ExpiryDate objectAtIndex:indexPath.row];
    
    cell.btn_receipt.tag = indexPath.row;
    [cell.btn_receipt addTarget:self action:@selector(receiptclicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *CellIdentifier = @"SectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    return headerView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    NSLog(@"PayPal Payment Success!=%@",[completedPayment description]);
    self.resultText = [completedPayment description];
    [self showSuccess];
    [self sendCompletedPaymentToServer:completedPayment];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment
{
    payment_paypalamount = [NSString stringWithFormat:@"%@",completedPayment.amount];
    NSArray * arr_conf = [completedPayment valueForKey:@"confirmation"];
    NSArray * arr_response = [arr_conf valueForKey:@"response"];
    str_transactionid = [arr_response valueForKey:@"id"];
    NSLog(@"tranid==%@",str_transactionid);
    
    [self paymenttransferwebservice];
}


#pragma mark - Authorize Future Payments

- (IBAction)getUserAuthorizationForFuturePayments:(id)sender
{
    PayPalFuturePaymentViewController *futurePaymentViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfig delegate:self];
    [self presentViewController:futurePaymentViewController animated:NO completion:nil];
}


#pragma mark - PayPalFuturePaymentDelegate methods

- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization
{
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = [futurePaymentAuthorization description];
    [self showSuccess];
    [self sendFuturePaymentAuthorizationToServer:futurePaymentAuthorization];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController
{
    NSLog(@"PayPal Future Payment Authorization Canceled");
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)sendFuturePaymentAuthorizationToServer:(NSDictionary *)authorization
{
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}


#pragma mark - Authorize Profile Sharing

- (IBAction)getUserAuthorizationForProfileSharing:(id)sender
{
    NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
    
    PayPalProfileSharingViewController *profileSharingPaymentViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues configuration:self.payPalConfig delegate:self];
    [self presentViewController:profileSharingPaymentViewController animated:NO completion:nil];
}


#pragma mark - PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization
{
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    [self showSuccess];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
{
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization
{
    // Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}


#pragma mark - Helpers

- (void)showSuccess
{
    self.successView.hidden = NO;
    self.successView.alpha = 1.0f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:2.0];
    self.successView.alpha = 0.0f;
    [UIView commitAnimations];
}

#pragma mark - Flipside View Controller

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"pushSettings"])
    {
        [[segue destinationViewController] setDelegate:(id)self];
    }
}

@end

//
//  programcontroller.h
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface programcontroller : UIViewController <UITableViewDelegate,UITableViewDataSource,WebDataDelegate , UITextViewDelegate>
{
    NSArray * arr_main;
    NSArray * arr_id;
    NSArray * arr_pt_detail;
    NSArray * arr_name;
    NSArray * arr_image;
    NSArray * file_arr;
    NSArray * text_arr;
    NSArray * arr_crteateddate;
    NSArray * arr_discription;
    NSArray * arr_programdetail;
    NSArray * arr_file;
    NSArray * arr_text;
    
    NSString * pt_id ;
    NSString *Str_savepdf;
    
    BOOL isFilter;
    NSInteger ind_ex;
    NSMutableDictionary * Request_data;
}

//IBOUTLET

@property (nonatomic,retain)IBOutlet  UILabel * lbl_name;
@property (nonatomic,retain)IBOutlet  UILabel * lbl_email;
@property (nonatomic,retain)IBOutlet  UILabel * lbl_phone;
@property (nonatomic,retain)IBOutlet  UILabel * lbl_programcontrollername;
@property (nonatomic,retain)IBOutlet  UILabel * lbl_programcontrollernamedate;

@property (nonatomic,retain)IBOutlet  UITableView * tbl_program;

@property (nonatomic,retain)IBOutlet  UITextView * txt_programcontrollerdiscription;

@property (nonatomic,retain)IBOutlet  UIImageView  * img_ptimg;
@property (nonatomic,retain)IBOutlet  UIImageView  * img_logoimg;

@property (nonatomic,retain)NSString * classname;
@property (nonatomic,retain)NSString * program_id;
@property (nonatomic,retain)NSString * str_logo;

@property (nonatomic,retain)WebService *webobj;
@property (nonatomic,retain)NSArray * dictclient;


//Action
- (IBAction)Btn_savepdf:(id)sender;
@end

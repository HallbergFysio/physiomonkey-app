//
//  Leftviewcell.h
//  PhysioMonkey
//
//  Created by prateek on 03/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Leftviewcell : UITableViewCell

//IBOUTLET CLIENTLEFTCELL
@property (nonatomic,retain)IBOutlet UILabel * lbl_Leftcell;
@property (nonatomic,retain)IBOutlet UIImageView * img_Leftcell;

//IBOUTLET PROFESSIONALLEFTCELL
@property (nonatomic,retain)IBOutlet UILabel * lbl_PROLeftcell;
@property (nonatomic,retain)IBOutlet UIImageView * img_PROLeftcell;


@end

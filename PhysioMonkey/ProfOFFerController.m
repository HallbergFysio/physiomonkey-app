//
//  ProfOFFerController.m
//  PhysioMonkey
//
//  Created by prateek on 5/19/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ProfOFFerController.h"

@interface ProfOFFerController ()

@end

@implementation ProfOFFerController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tbl_Professionaloffer.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];


    
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
    
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [SVProgressHUD show];
}

#pragma mark -
#pragma mark Left Menu method

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"profofferhome" sender:self];
}

-(void)webservicecalled
{
    [self.webObj GETAPI:nil methodname:@"v1/offers"];
}
-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    NSArray * arr_main = [dict valueForKeyPath:@"data"];
    arr_name = [arr_main valueForKeyPath:@"name"];
    arr_image = [arr_main valueForKeyPath:@"image"];
    arr_link = [arr_main valueForKeyPath:@"link"];
    [self.tbl_Professionaloffer reloadData];
}
-(void)dismissKeyboard
{
    offercontrollercell* cell1 = [_tbl_Professionaloffer cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [cell1.lbl_Profoffertye resignFirstResponder];
    [cell1.lbl_Profofferpercent resignFirstResponder];
}


#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_name count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"OFFERCell";
    offercontrollercell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[offercontrollercell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    cell.lbl_Profoffertye.text = [arr_name objectAtIndex:indexPath.row];
    cell.lbl_Profofferpercent.text = [arr_link objectAtIndex:indexPath.row];
    [cell.img_Profoffer sd_setImageWithURL:[NSURL URLWithString:[arr_image objectAtIndex:indexPath.row]]
                      placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
    
    cell.lbl_Profofferpercent.editable = NO;
    cell.lbl_Profofferpercent.dataDetectorTypes = UIDataDetectorTypeLink;
    cell.lbl_Profofferpercent.delegate = self;
    return cell;
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange NS_AVAILABLE_IOS(7_0)
{
    return YES;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end

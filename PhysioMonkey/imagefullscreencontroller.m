//
//  imagefullscreencontroller.m
//  PhysioMonkey
//
//  Created by prateek on 02/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "imagefullscreencontroller.h"
#import "UIImageView+WebCache.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVPlayerItem.h>
@import AVKit;
@import AVFoundation;
@interface imagefullscreencontroller ()
{
    AVPlayerViewController *playerViewController;
}

@end

@implementation imagefullscreencontroller

#pragma mark -
#pragma mark:ViewLoadMethod

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    NSString * str_filetype = [_image_str pathExtension];
    NSLog(@"extension: %@", _image_str);
    
    if ([str_filetype isEqualToString:@"mp4"])
    {
        [_img_view setHidden:YES];
        
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        NSLog(@"height==%f",height);
        playerViewController = [[AVPlayerViewController alloc]init];
        AVURLAsset *asset = [AVURLAsset assetWithURL: [NSURL URLWithString:_image_str]];
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
        
        AVPlayer * player = [[AVPlayer alloc] initWithPlayerItem: item];
        playerViewController.player = player;
        
        if (height==736.000000)
        {
            [playerViewController.view setFrame:CGRectMake(0,0,414,735)];
        }
        else if (height==667.000000)
        {
            [playerViewController.view setFrame:CGRectMake(0,0,375,666)];
        }
        else if (height==568.000000)
        {
            [playerViewController.view setFrame:CGRectMake(0,0,320,567)];
        }
       
        playerViewController.showsPlaybackControls = YES;
        [self.view addSubview:playerViewController.view];
        
    }
    
    else
        
    {
        
        [_player_view setHidden:YES];
        [_img_view setHidden:NO];
        
        if (![self.image_str isEqual:[NSNull null]])
        {
            [_img_view setContentMode:UIViewContentModeScaleAspectFit];
            [_img_view sd_setImageWithURL:[NSURL URLWithString:self.image_str]
                         placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
            
        }
    }
}

#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}

-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark - IBAction
-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)backbuttonclicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end

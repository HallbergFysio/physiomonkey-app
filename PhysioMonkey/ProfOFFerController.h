//
//  ProfOFFerController.h
//  PhysioMonkey
//
//  Created by prateek on 5/19/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfOFFerController : UIViewController<UITableViewDelegate,UITableViewDataSource,WebDataDelegate,UITextViewDelegate>
{
    NSArray * arr_name;
    NSArray * arr_image;
    NSArray * arr_link;
    NSMutableDictionary * Request_data;
}

//IBOUTLET
@property(nonatomic,retain)IBOutlet UITableView * tbl_Professionaloffer;
@property (nonatomic,strong) WebService *webObj;

@end

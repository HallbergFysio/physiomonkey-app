//
//  previousprogramcontroller.m
//  PhysioMonkey
//
//  Created by prateek on 17/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "previousprogramcontroller.h"

@interface previousprogramcontroller ()
@end
@implementation previousprogramcontroller


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tbl_previousprogram.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    
    [self webservicecalled];
    [self displaybackbutton];
    [self DisplayLogoImageinTitleview];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[SVProgressHUD show];
    [self method];
}

#pragma mark -
#pragma mark: Navigation Baar Menu
-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
}

#pragma mark -
#pragma mark: IBAction

-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)method
{
    //[SVProgressHUD show];
}
#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)webservicecalled
{
    //[SVProgressHUD show];
    Post_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_str_client_id_previous,@"client_id", nil];
    [self.webobj postapi:Post_data methodname:@"v1/categoryOptionsOnClientScreen"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    arr_main = [dict valueForKeyPath:@"data"];
    arr_name = [arr_main valueForKeyPath:@"name"];
    arr_image = [arr_main valueForKeyPath:@"image"];
    arr_id = [arr_main valueForKeyPath:@"id"];
    [self.tbl_previousprogram reloadData];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_id count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"librarycell";
    librarycell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[librarycell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    cell.lbl_PreviousProgram.text = [arr_name objectAtIndex:indexPath.row];
    
    cell.img_PreviousProgram.contentMode = UIViewContentModeScaleAspectFill;
    cell.img_PreviousProgram.clipsToBounds=YES;
    
    
    [ cell.img_PreviousProgram sd_setImageWithURL:[NSURL URLWithString:[arr_image objectAtIndex:indexPath.row]]
                                 placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EditPreviousProgram * subobj = [self.storyboard instantiateViewControllerWithIdentifier:@"EditPreviousProgram"];
    subobj.str_client_id_previous22 = _str_client_id_previous;
    subobj.str_cat_idd = [arr_id objectAtIndex:indexPath.row];
    subobj.str_cat_name = [arr_name objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:subobj animated:NO];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

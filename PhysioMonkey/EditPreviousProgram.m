//
//  EditPreviousProgram.m
//  PhysioMonkey
//
//  Created by prateek on 11/03/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "EditPreviousProgram.h"
@interface EditPreviousProgram ()
@end
@implementation EditPreviousProgram


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_lbl_line setHidden:YES];
    
    self.tbl_view.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
}

#pragma mark -
#pragma mark: Navigation Baar Menu
-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;

}



#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)webservicecalled
{
    
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_str_client_id_previous22,@"client_id",_str_cat_idd,@"cat_id", nil];
    [self.webobj postapi:Request_data  methodname:@"v1/viewpreviousprograms"];
}
-(void)webservicecalledSavePdf
{
    //[SVProgressHUD show];
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:pdf_id,@"program_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/programaspdf"];
}
-(void)DeleteWebServiceCalled
{
    Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:str_programidd,@"program_id", nil];
    [self.webobj postapi:Request_data methodname:@"v1/deleteprogram"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    if ([common_str isEqualToString:@"delete"])
    {
        NSString * delete_status = [dict valueForKeyPath:@"message"];
        if ([delete_status isEqualToString:@"Program deleted successfully"])
        {
            common_str= @"";
            [self.webobj showAlertView:@"Program Delete Successfully!" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
            [self webservicecalled];
        }
        else
        {
            [self.webobj showAlertView:@"Program Delete Failed!" msg:nil atag:0 cncle:@"OK" othr:nil delegate:nil];
            
        }
    }
    else if ([common_str isEqualToString:@"pdfsave"])
    {
        
        NSDate *currentTime = [NSDate date];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@" yyyy-MM-d H:m:s"];
        NSString *resultString = [dateFormatter stringFromDate: currentTime];
        NSLog(@"resultString=%@",resultString);
        
        //NSString * data_status = [dict valueForKeyPath:@"message"];
        NSString *data = [dict valueForKeyPath:@"data"];
        NSString *pdf = [data valueForKeyPath:@"program_pdf"];
        
        NSData *dataPdf = [NSData dataWithContentsOfURL:[NSURL URLWithString:pdf]];
        //Get path directory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //Create PDF_Documents directory
        documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Physiomonkey"];
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        
        NSString *filePath = [NSString stringWithFormat:@"%@/%@ (%@)", documentsDirectory, @"Physiomonkey program Pdf" ,  resultString];
        
        [dataPdf writeToFile:filePath atomically:YES];
        
        NSLog(@"filePath===%@",filePath);
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [SVProgressHUD dismiss];
                           
                           [self.webobj showAlertView:@"Alert!" msg:@"Pdf Saved Successfull!" atag:0 cncle:@"OK" othr:nil delegate:nil];
                       });
        common_str= @"";
        
        
    }

    else
    {
        arr_main = [dict valueForKeyPath:@"data"];
        arr_detials = [arr_main valueForKeyPath:@"details"];
        arr_title = [arr_main valueForKeyPath:@"title"];
        arr_created = [arr_main valueForKeyPath:@"created"];
        arr_id = [arr_main valueForKeyPath:@"id"];
        _cat_name.text =  _str_cat_name;
        [_lbl_line setHidden:NO];
        [self.tbl_view reloadData];
    }
    
}

#pragma mark -
#pragma mark: UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_id count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"library";
    librarycell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[librarycell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    cell.lbl_editpreviousdate.text = [arr_created objectAtIndex:indexPath.row];
    cell.lbl_editprevioustitle.text = [arr_title objectAtIndex:indexPath.row];
    
    [self.webobj buttonBorderColor:cell.btn_editpreviousedit];
    [self.webobj buttonBorderColor:cell.btn_editpreviousdelete];
    [self.webobj buttonBorderColor:cell.btn_editpreviouspdf];
    
    cell.btn_editpreviousedit.tag=indexPath.row;
    cell.btn_editpreviousdelete.tag=indexPath.row;
    cell.btn_editpreviouspdf.tag=indexPath.row;
    
    [cell.btn_editpreviousedit addTarget:self action:@selector(editbuttonclicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_editpreviousdelete addTarget:self action:@selector(deletebuttonclicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_editpreviouspdf addTarget:self action:@selector(pdfbuttonclicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}


#pragma mark -
#pragma mark: IBAction

-(void)editbuttonclicked:(UIButton*)sender
{
    CreateProgramController * exiobj = [self.storyboard instantiateViewControllerWithIdentifier:@"CREATE"];
    NSDictionary * edit_dic = arr_main[sender.tag];
    exiobj.str_clasname = @"sublibrary";
    exiobj.str_subclasname = @"newlibrary";
    exiobj.dic_sublib = edit_dic;
    [self.navigationController pushViewController:exiobj animated:NO];
}

-(void)backbuttonclick
{
    [self.navigationController popViewControllerAnimated:NO];
}


-(void)pdfbuttonclicked:(UIButton*)sender
{
    common_str  = @"pdfsave";
    NSDictionary * edit_dic = arr_main[sender.tag];
    pdf_id = [edit_dic valueForKey:@"id"];
    [self webservicecalledSavePdf];
}

-(void)deletebuttonclicked:(UIButton*)sender
{
    [SVProgressHUD show];
    common_str = @"delete";
    NSDictionary * delete_dic = arr_main[sender.tag];
    str_programidd = [delete_dic valueForKeyPath:@"id"];
    NSLog(@"%@",str_programidd);
    [self DeleteWebServiceCalled];
}
@end

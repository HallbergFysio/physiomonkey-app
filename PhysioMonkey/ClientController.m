//
//  ClientController.m
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ClientController.h"

@interface ClientController ()
@end
@implementation ClientController


#pragma mark -
#pragma mark: viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.CommonMethodObj =[[CommonMethodClass alloc]init];
    self.tbl_client.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.webobj=[[WebService alloc]init];
    self.webobj.delegate=self;
    [self webservicecalled];
    [self DisplayLogoImageinTitleview];
    [self displaybackbutton];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _search_bar.text = @"";
    [self method];
}


#pragma mark -
#pragma mark: Navigation Baar Menu

-(void)dismissKeyboard
{
    [_search_bar resignFirstResponder];
}
-(void)DisplayLogoImageinTitleview
{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hd_logo3.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0,10,150,30)];
    imageView.frame = titleView.bounds;
    [titleView addSubview:imageView];
    self.navigationItem.titleView = titleView;
}
-(void)displaybackbutton
{
    UIImage* image3 = [UIImage imageNamed:@"back.png"];
    CGRect frameimg = CGRectMake(0,16,15,15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonclick)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnSideMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSideMenu.frame=CGRectMake(0, 20,25,25);
    [btnSideMenu setImage:[UIImage imageNamed:@"menunew.png"] forState:UIControlStateNormal];
    [btnSideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnSideMenu];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    
}
-(void)method
{
    // [SVProgressHUD show];
}


#pragma mark -
#pragma mark: HTTPWebserviceMethod
-(void)webservicecalled
{
    [SVProgressHUD show];
    [self.webobj postapi:nil methodname:@"v1/myclients"];
}

-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    str_status = [dict valueForKeyPath:@"message"];
    
    if ([str_checkstatus isEqualToString:@"verify_clicked"])
    {
        if ([str_status isEqualToString:@"Client verified."])
        {
            str_checkstatus = @"";
            [self webservicecalled];
        }
    }
    else if ([str_checkstatus isEqualToString:@"cancel_clicked"])
    {
        if ([str_status isEqualToString:@"Request canceled successful"])
        {
            str_checkstatus = @"";
            [self webservicecalled];
        }
    }
    
    else
    {
        if ([str_status isEqualToString:@"Record not found"])
        {
            [self.tbl_client setHidden:YES];
            [self.webobj showAlertView:nil msg:@"No new clients at the moment" atag:0 cncle:@"OK" othr:nil delegate:nil];
        }
        else
        {
            arr_main = [dict valueForKeyPath:@"data"];
            arr_phone = [arr_main valueForKeyPath:@"phone"];
            arr_name = [arr_main valueForKeyPath:@"name"];
            arr_email = [arr_main valueForKeyPath:@"email"];
            arr_image = [arr_main valueForKeyPath:@"image"];
            arr_dob = [arr_main valueForKeyPath:@"dob"];
            arr_id = [arr_main valueForKeyPath:@"id"];
            
            [self.tbl_client reloadData];
        }
        
    }
}

#pragma mark -
#pragma mark: UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isFilter)
    {
        return [filtered count];
    }
    else{
        return [arr_id count];
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strIdentifier = @"clientcell";
    clientcell * cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    if (cell==nil)
    {
        cell = [[clientcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    
    
    if (isFilter)
    {
        Filterarr_name =  [filtered valueForKeyPath:@"name"];
        Filterarr_image = [filtered valueForKeyPath:@"image"];
        Filterarr_phone = [filtered valueForKeyPath:@"phone"];
        Filterarr_email = [filtered valueForKeyPath:@"email"];
        Filterarr_id = [filtered valueForKeyPath:@"id"];
        Filterarr_dob = [filtered valueForKeyPath:@"dob"];
        
        cell.lbl_clientname.text = [Filterarr_name objectAtIndex:indexPath.row];
        cell.lbl_clientemail.text = [Filterarr_email objectAtIndex:indexPath.row];
        
        
        if ([[Filterarr_phone objectAtIndex:indexPath.row]isEqualToString:@""])
        {
            cell.lbl_clientnumber.text = @"";
        }
        else
        {
            cell.lbl_clientnumber.text = [Filterarr_phone objectAtIndex:indexPath.row];
        }
        
        
        NSString * str_dob  = [NSString stringWithFormat:@"%@",[Filterarr_dob objectAtIndex:indexPath.row]];
        cell.lbl_clientdob.text = str_dob;
        
        if ([[Filterarr_image objectAtIndex:indexPath.row]isEqual:[NSNull null]])
        {
            cell.img_user.image = [UIImage imageNamed:@"profile_img_infonew.png"];
        }
        else
        {
            [ cell.img_user sd_setImageWithURL:[NSURL URLWithString:[Filterarr_image objectAtIndex:indexPath.row]]
                              placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
            
        }
        
        [self.CommonMethodObj imageround40:cell.img_user];
        [self.CommonMethodObj imageRounded:cell.btn_Calender];
        [self.CommonMethodObj imageRounded:cell.btn_CreateProgram];
        [self.CommonMethodObj imageRounded:cell.btn_ViewPreviousProgram];
        [self.CommonMethodObj imageRounded:cell.btn_UseExistingProgram];
        [self.CommonMethodObj myprogrambuttonrounded:cell.btn_ViewProfile];
        
        
        cell.btn_Calender.tag=indexPath.row;
        [cell.btn_Calender addTarget:self action:@selector(calenderclicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_ViewProfile.tag=indexPath.row;
        [cell.btn_ViewProfile addTarget:self action:@selector(viewprofileclicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_CreateProgram.tag=indexPath.row;
        [cell.btn_CreateProgram addTarget:self action:@selector(createprogramclicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_UseExistingProgram.tag=indexPath.row;
        [cell.btn_UseExistingProgram addTarget:self action:@selector(useExistingprogramclicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_ViewPreviousProgram.tag=indexPath.row;
        [cell.btn_ViewPreviousProgram addTarget:self action:@selector(viewPreviousprogramclicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        
        cell.lbl_clientname.text = [arr_name objectAtIndex:indexPath.row];
        cell.lbl_clientemail.text = [arr_email objectAtIndex:indexPath.row];
        
        
        if ([[arr_phone objectAtIndex:indexPath.row]isEqualToString:@""])
        {
            cell.lbl_clientnumber.text = @"";
        }
        else
        {
            cell.lbl_clientnumber.text = [arr_phone objectAtIndex:indexPath.row];
        }
        
        
        NSString * str_dob  = [NSString stringWithFormat:@"%@",[arr_dob objectAtIndex:indexPath.row]];
        cell.lbl_clientdob.text = str_dob;
        
        if ([[arr_image objectAtIndex:indexPath.row]isEqual:[NSNull null]])
        {
            cell.img_user.image = [UIImage imageNamed:@"profile_img_infonew.png"];
        }
        else
        {
            [ cell.img_user sd_setImageWithURL:[NSURL URLWithString:[arr_image objectAtIndex:indexPath.row]]
                              placeholderImage:[UIImage imageNamed:@"profile_img_infonew.png"]];
            
        }
        
        [self.CommonMethodObj imageround40:cell.img_user];
        [self.CommonMethodObj imageRounded:cell.btn_Calender];
        [self.CommonMethodObj imageRounded:cell.btn_CreateProgram];
        [self.CommonMethodObj imageRounded:cell.btn_ViewPreviousProgram];
        [self.CommonMethodObj imageRounded:cell.btn_UseExistingProgram];
        [self.CommonMethodObj myprogrambuttonrounded:cell.btn_ViewProfile];
        
        
        
        cell.btn_Calender.tag=indexPath.row;
        [cell.btn_Calender addTarget:self action:@selector(calenderclicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_ViewProfile.tag=indexPath.row;
        [cell.btn_ViewProfile addTarget:self action:@selector(viewprofileclicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_CreateProgram.tag=indexPath.row;
        [cell.btn_CreateProgram addTarget:self action:@selector(createprogramclicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_UseExistingProgram.tag=indexPath.row;
        [cell.btn_UseExistingProgram addTarget:self action:@selector(useExistingprogramclicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_ViewPreviousProgram.tag=indexPath.row;
        [cell.btn_ViewPreviousProgram addTarget:self action:@selector(viewPreviousprogramclicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark: search bar method

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSString *filterstr = [NSString stringWithFormat:@"%@",_search_bar.text] ;
    NSString *substring = [NSString stringWithString:filterstr];
    NSLog(@"substring %@",substring);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@",substring];
    filtered =[[arr_main filteredArrayUsingPredicate:predicate] copy];
    NSLog(@"filtered %@",filtered);
    if (filtered.count == 0)
    {
        isFilter=NO;
        [self.tbl_client reloadData];
    }
    else
    {
        
        NSLog(@"filtered %@",filtered);
        isFilter=YES;
        [self.tbl_client reloadData];
    }
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_search_bar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_search_bar resignFirstResponder];
}


#pragma mark -
#pragma mark: UIButton Action

-(void)backbuttonclick
{
    [self performSegueWithIdentifier:@"profclient" sender:self];
}

-(void)calenderclicked:(UIButton*)sender
{
    ptcalandar * cal_obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ptcalandar"];
    if (isFilter)
    {
        client_viewprofile = filtered[sender.tag];
        
    }
    else
    {
        client_viewprofile = arr_main[sender.tag];
    }
    client_idd = [client_viewprofile objectForKey:@"id"];
    cal_obj.str_recentclientid = client_idd;
    [self.navigationController pushViewController:cal_obj animated:NO];
}
-(void)viewprofileclicked:(UIButton*)sender
{
    ptviewprofile * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ptviewprofile"];
    
    if (isFilter)
    {
        client_viewprofile = filtered[sender.tag];
        
    }
    else
    {
        client_viewprofile = arr_main[sender.tag];
    }
    client_idd = [client_viewprofile objectForKey:@"id"];
    vc.str_recentclientid = client_idd;
    [self.navigationController pushViewController:vc animated:NO];
}


-(void)createprogramclicked:(UIButton*)sender
{
    CreateProgramController * exiobj = [self.storyboard instantiateViewControllerWithIdentifier:@"CREATE"];
    if (isFilter)
    {
        client_clientpreprogdict = filtered[sender.tag];
        client_idd = [client_clientpreprogdict objectForKey:@"id"];
        
    }
    else
    {
        client_clientpreprogdict = arr_main[sender.tag];
        client_idd = [client_clientpreprogdict objectForKey:@"id"];
    }
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    [obj setObject:client_idd forKey:@"clientid_createprogram"];
    [obj synchronize];
    exiobj.str_idclientid = client_idd ;
    [self.navigationController pushViewController:exiobj animated:NO];
}


-(void)useExistingprogramclicked:(UIButton*)sender
{
    ExistingProgramController * exiobj = [self.storyboard instantiateViewControllerWithIdentifier:@"existing"];
    if (isFilter)
    {
        client_clientpreprogdict = filtered[sender.tag];
        client_idd = [client_clientpreprogdict objectForKey:@"id"];
        
    }
    else
    {
        client_clientpreprogdict = arr_main[sender.tag];
        client_idd = [client_clientpreprogdict objectForKey:@"id"];
    }
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    [obj setObject:client_idd forKey:@"clientid_createprogram"];
    [obj synchronize];
    client_idd = [client_clientpreprogdict objectForKey:@"id"];
    exiobj.str_client_id_previous = client_idd;
    [self.navigationController pushViewController:exiobj animated:NO];
}
-(void)viewPreviousprogramclicked:(UIButton*)sender
{
    previousprogramcontroller * probj = [self.storyboard instantiateViewControllerWithIdentifier:@"PREVIOUS"];
    if (isFilter)
    {
        client_clientpreprogdict = filtered[sender.tag];
        client_idd = [client_clientpreprogdict objectForKey:@"id"];
        
    }
    else
    {
        client_clientpreprogdict = arr_main[sender.tag];
        client_idd = [client_clientpreprogdict objectForKey:@"id"];
    }
    NSUserDefaults * obj = [NSUserDefaults standardUserDefaults];
    [obj setObject:client_idd forKey:@"clientid_createprogram"];
    [obj synchronize];
    client_idd = [client_clientpreprogdict objectForKey:@"id"];
    probj.str_client_id_previous = client_idd;
    [self.navigationController pushViewController:probj animated:NO];
}

@end

//
//  ViewProfileController.h
//  PhysioMonkey
//
//  Created by prateek on 08/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
@interface ViewProfileController : UIViewController < WebDataDelegate , UITextViewDelegate >
{
    NSString * str_id ;
    NSString * str_name;
    NSString * str_address;
    NSString * str_phone;
    NSString * str_image;
    NSString * str_email;
    NSString * str_clinicname;
    NSString * str_website;
    NSString * str_aboutme;
    NSString * str_logoimg;
    NSString * str_twitter;
    NSString * str_facebook;
    NSString * str_linkedin;
    NSMutableDictionary * Request_data;
}

//IBOUTLET

@property (nonatomic,retain)IBOutlet UILabel * lbl_name;
@property (nonatomic,retain)IBOutlet UILabel * lbl_email;
@property (nonatomic,retain)IBOutlet UILabel * lbl_phone;
@property (nonatomic,retain)IBOutlet UILabel * lbl_address;
@property (nonatomic,retain)IBOutlet UILabel * lbl_clinicname;
@property (nonatomic,retain)IBOutlet UILabel * lbl_aboutme;
@property (nonatomic,retain)IBOutlet UIImageView * img_user;
@property (nonatomic,retain)IBOutlet UIImageView * img_clinic_logo;

@property (nonatomic,retain)IBOutlet UITextView * lbl_website;
@property (nonatomic,retain)NSMutableArray * arr_viewprofile;
@property (nonatomic,strong)WebService *webObj;


//IBACTION
-(IBAction)facebookBtnclicked:(id)sender;
-(IBAction)twitterBtnclicked:(id)sender;
-(IBAction)LinkedinBtnclicked:(id)sender;
@end

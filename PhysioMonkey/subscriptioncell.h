//
//  subscriptioncell.h
//  PhysioMonkey
//
//  Created by prateek on 20/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface subscriptioncell : UITableViewCell

//IBOUTLET
@property (nonatomic,retain)IBOutlet UILabel * lbl_TransactionNo;
@property (nonatomic,retain)IBOutlet UILabel * lbl_Amount;
@property (nonatomic,retain)IBOutlet UILabel * lbl_PaymentType;
@property (nonatomic,retain)IBOutlet UILabel * lbl_PaymentDate;
@property (nonatomic,retain)IBOutlet UILabel * lbl_ExpirationDate;
@property (nonatomic,retain)IBOutlet UILabel * lbl_PlanType;
@property (nonatomic,retain)IBOutlet UILabel * lbl_Receipt;
@property (nonatomic,retain)IBOutlet UIButton * btn_receipt;

@end

//
//  SignupController.h
//  PhysioMonkey
//
//  Created by prateek on 02/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
@interface SignupController : UIViewController <UITextFieldDelegate,WebDataDelegate , UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSMutableDictionary *Request_data;
    NSMutableArray *arrbaritems;
    NSString * str_check;
    UIDatePicker *datePicker;
    UIPickerView *RequiredpickerView;
}


//IBOUTLET

@property (nonatomic,retain)IBOutlet UIButton * Back_btn;
@property (nonatomic,retain)IBOutlet UIButton * view_Back_btn;

@property (nonatomic,retain)IBOutlet UIView * view_subview;

@property (nonatomic,retain)IBOutlet UIImageView * view_image;
@property (nonatomic,retain)IBOutlet UIImageView * view_logoimage;

@property (nonatomic,retain)IBOutlet UIButton * Check_btn;
@property (nonatomic,retain)IBOutlet UIButton * Login_btn;
@property (nonatomic,retain)IBOutlet UITextField * txt_name;
@property (nonatomic,retain)IBOutlet UITextField * txt_email;
@property (nonatomic,retain)IBOutlet UITextField * txt_personal_id;
@property (nonatomic,retain)IBOutlet UITextField * txt_password;
@property (nonatomic,retain)IBOutlet UITextField * txt_conform_password;
@property (nonatomic,retain)IBOutlet UILabel * creatingaccount_lbl;
@property (nonatomic,retain) IBOutlet UIWebView * web_view;
@property (nonatomic,strong) WebService *webObj;

//IBACTION
-(IBAction)Backbutton_clicked:(id)sender;
-(IBAction)Signup_clicked:(id)sender;
-(IBAction)checkbutton_clicked:(id)sender;

-(IBAction)Termsofservicebutton_clicked:(id)sender;
-(IBAction)Privacypolicybutton_clicked:(id)sender;
-(IBAction)viewbackbutton_clicked:(id)sender;
@end

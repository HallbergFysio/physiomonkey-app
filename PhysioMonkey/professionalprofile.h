//
//  professionalprofile.h
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface professionalprofile : UIViewController <WebDataDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{
    
    NSString * str_Phone;
    NSString * str_Address;
    NSString * str_ClinicName;
    NSString * str_Email;
    NSString * str_WebsiteUrl;
    NSString * str_FacebookUrl;
    NSString * str_TwitterUrl;
    NSString * str_LinkedinUrl;
    NSString * str_DOB;
    NSString * str_AboutMe;
    NSString * str_image;
    NSString * str_common;
    NSString * str_cliniclogo;
    
    NSMutableDictionary *Request_data;
    UIImage * chosenImage;

}

//IBOUTLET

@property (nonatomic,retain)IBOutlet  UIButton * btn_userimage;
@property (nonatomic,retain)IBOutlet  UIButton * btn_logouserimage;
@property (nonatomic,retain)IBOutlet  UIButton * btn_Edit;

@property (nonatomic,retain) UIImagePickerController *imagepick;
@property (nonatomic,retain) UIImagePickerController *logoimagepick;

@property (nonatomic,retain)IBOutlet  UIScrollView * scroll_view;
@property (nonatomic,retain)IBOutlet  UIView * content_view;

@property (nonatomic,retain)IBOutlet  UIImageView * img_userimage;
@property (nonatomic,retain)IBOutlet  UIImageView * img_logoimage;

@property (nonatomic,retain)IBOutlet UITextField * txt_Name;
@property (nonatomic,retain)IBOutlet UITextField * txt_DOB;
@property (nonatomic,retain)IBOutlet UITextField * txt_Phone;
@property (nonatomic,retain)IBOutlet UITextField * txt_Email;
@property (nonatomic,retain)IBOutlet UITextField * txt_ClinicName;
@property (nonatomic,retain)IBOutlet UITextField * txt_Address;
@property (nonatomic,retain)IBOutlet UITextField * txt_Website;
@property (nonatomic,retain)IBOutlet UITextField * txt_Aboutme;
@property (nonatomic,retain)IBOutlet UITextField * txt_Facebookurl;
@property (nonatomic,retain)IBOutlet UITextField * txt_Twitterurl;
@property (nonatomic,retain)IBOutlet UITextField * txt_LinkedinUrl;

@property (nonatomic,strong) WebService *webObj;

//IBAction
-(IBAction)editbuttonclicked:(id)sender;
-(IBAction)ImageButtonclicked:(id)sender;

@end

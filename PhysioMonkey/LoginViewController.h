//
//  LoginViewController.h
//  PhysioMonkey
//
//  Created by prateek on 02/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoginViewController : UIViewController <UITextFieldDelegate,WebDataDelegate,UIAlertViewDelegate>

{
    NSMutableDictionary *Request_data;
    NSUserDefaults * obj;
    UITextField *textfield;
    NSString * check_status;
    NSString * str_status;
}

//IBOUTLET
@property (nonatomic,retain)IBOutlet UITextField * email_txt;
@property (nonatomic,retain)IBOutlet UITextField * password_txt;
@property (nonatomic,retain)IBOutlet UIButton * Back_btn;
@property (nonatomic,strong) WebService *webObj;

//IBACTION
-(IBAction)Backbutton_clicked:(id)sender;
-(IBAction)Loginbutton_clicked:(id)sender;
-(IBAction)Signupbutton_clicked:(id)sender;
-(IBAction)forgotpasswordclicked:(id)sender;


@end

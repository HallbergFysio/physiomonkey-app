//
//  RestClientController.h
//  PhysioMonkey
//
//  Created by prateek on 09/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonMethodClass.h"
#import "WebService.h"
@interface RestClientController : UIViewController  <UITableViewDelegate,UITableViewDataSource,WebDataDelegate>
{
    NSString *  str_status;
    NSString * str_checkstatus;
    NSArray * arr_main;
    NSArray * arr_phone;
    NSArray * arr_name;
    NSArray * arr_email;
    NSArray * arr_image;
    NSArray * arr_id;
    NSArray * arr_dob;
    NSDictionary * client_previosdict;
     NSDictionary * client_prevprogdict;
    NSDictionary * client_preprofiledict;
    BOOL isFilter;
    NSArray *Filterarr_name;
    NSArray * Filterarr_image;
    NSArray *  Filterarr_phone;
    NSArray *  Filterarr_email;
    NSArray *  Filterarr_id;
    NSArray * filtered;
    NSArray * Filterarr_dob;
    NSString * client_idd;
     NSDictionary * client_viewprofile;
}

//IBOUTLET

@property (strong, nonatomic) IBOutlet UISearchBar *search_bar;
@property (nonatomic,retain)IBOutlet UITableView * tbl_recentclient;
@property(nonatomic,strong)CommonMethodClass*CommonMethodObj;
@property (nonatomic,retain)WebService * webobj;

@end

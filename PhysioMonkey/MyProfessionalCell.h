//
//  MyProfessionalCell.h
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfessionalCell : UITableViewCell


//IBOUTLET MYPROFESSIONAL
@property (nonatomic,retain)IBOutlet UILabel * lbl_UserName;
@property (nonatomic,retain)IBOutlet UILabel * lbl_UserContactNumber;
@property (nonatomic,retain)IBOutlet UILabel * lbl_UserEmailID;

@property (nonatomic,retain)IBOutlet UIImageView * img_User;
@property (nonatomic,retain)IBOutlet UIImageView * img_Name;
@property (nonatomic,retain)IBOutlet UIImageView * img_Contactnumber;
@property (nonatomic,retain)IBOutlet UIImageView * img_EmailID;

@property (nonatomic,retain)IBOutlet UIButton * btn_ViewProfile;


//IBOUTLET OPENPROGRAM
@property (nonatomic,retain) IBOutlet UIImageView * img_OpenProgram;
@property (nonatomic,retain) IBOutlet   UILabel * lbl_OpenProgram;
@end

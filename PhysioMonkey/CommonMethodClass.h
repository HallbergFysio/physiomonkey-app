//
//  CommonMethodClass.h
//  samayLa
//
//  Created by prateek on 30/11/16.
//  Copyright © 2016 alleviatetech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonMethodClass : UIViewController

-(void)buttonbordercolor:(UIButton*)btn;
-(void)myprogrambuttonrounded:(UIButton*)btun;
-(void)imageRounded:(UIButton*)btn;
-(void)imageRoundedforview:(UIView*)vew;
-(void)imageRoundedfordifferentborder:(UIButton*)btun;
-(void)textfieldRounded:(UITextField*)txt;
-(void)imageround40:(UIImageView*)img;
-(void)imageroundedforimage:(UIImageView*)img;
-(void)imageRoundedfordifferentborder2:(UIView*)btun;
-(void)showAlertView:(NSString *)aTitle msg:(NSString *)message atag:(NSInteger)alerttag cncle:(NSString*)cancle othr:(NSString*)other othr2:(NSString*)other2 delegate:(id)delgate;

@end

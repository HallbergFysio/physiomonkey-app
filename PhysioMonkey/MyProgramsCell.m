//
//  MyProgramsCell.m
//  PhysioMonkey
//
//  Created by prateek on 07/02/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "MyProgramsCell.h"

@implementation MyProgramsCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.scroll_view.delegate=self;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

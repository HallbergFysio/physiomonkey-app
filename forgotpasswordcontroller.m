//
//  forgotpasswordcontroller.m
//  PhysioMonkey
//
//  Created by prateek on 3/29/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "forgotpasswordcontroller.h"

@interface forgotpasswordcontroller ()
@end
@implementation forgotpasswordcontroller


#pragma mark -
#pragma mark: viewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webObj=[[WebService alloc]init];
    self.webObj.delegate=self;
}

#pragma mark -
#pragma mark: UIButton Action

-(IBAction)Backbutton_clicked:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(IBAction)Resetpasswordclicked:(id)sender
{
    if ([_txt_email.text isEqual:@""]||[_txt_email.text length]==0)
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter E-Mail id!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if (![self validateEmailWithString:_txt_email.text])
    {
        [self.webObj showAlertView:@"Alert!" msg:@"Please Enter Valid E-Mail id!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else
    {
        [SVProgressHUD show];
        Request_data = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_txt_email.text,@"email", nil];
        [self.webObj postDictionary:Request_data methodname:@"v1/forgotpassword" view:self.view];
    }
}

#pragma mark -
#pragma mark:HTTPWebserviceMethod
-(void)returnJsonData:(NSDictionary *)dict
{
    [SVProgressHUD dismiss];
    str_status = [dict valueForKeyPath:@"message"];
    if([str_status isEqualToString:@"Email address does not exist"])
    {
        [self.webObj showAlertView:@"Forgot Password!" msg:@"Email address does not exist!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else if ([str_status isEqualToString:@"New Password sent to your registered mail address"])
    {
        [self.webObj showAlertView:@"Forgot Password!" msg:@"New Password sent to your registered mail address!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
    else
    {
        [self.webObj showAlertView:@"Forgot Password!" msg:@"Failed!" atag:0 cncle:@"OK" othr:nil delegate:nil];
    }
}

#pragma mark -
#pragma mark:Common Method
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
@end
